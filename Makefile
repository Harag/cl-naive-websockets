all: test documentation
.PHONY:: test tests documentation clean lisp-tests \
	javascript-tests \
	javascript-install-dependencies \
	javascript-unit-tests \
	javascript-integration-tests \
	javascript-simple-client-test

# default values:
ARTDIR = tests/artifacts/
DEPENDENCYDIR = $(abspath $(CURDIR)/..)/
THISDIR = $(abspath $(CURDIR))/
PORT = 33001

help:
	@printf 'make %-32s # %s\n' documentation     		   'Generate the documentation.'
	@printf 'make %-32s # %s\n' test              		   'Run all the tests.'
	@printf 'make %-32s # %s\n' tests             		   'Run all the tests.'
	@printf 'make %-32s # %s\n' lisp-tests        		   'Run the lisp tests.'
	@printf 'make %-32s # %s\n' javascript-install-dependencies 'Install npm dependencies. Once before tests.'
	@printf 'make %-32s # %s\n' javascript-tests  		   'Run the javascript tests.'
	@printf 'make %-32s # %s\n' javascript-unit-tests          'Run the javascript unit tests.'
	@printf 'make %-32s # %s\n' javascript-integration-tests   'Run the javascript integration tests.'
	@printf 'make %-32s # %s\n' ws-chat-client    		   'Run the chat client example.'
	@printf 'make %-32s # %s\n' ws-chat-server    		   'Run the chat server example.'
	@printf 'make %-32s # %s\n' clean             		   'Clean temporary files.'

test tests: lisp-tests javascript-tests

javascript-tests: \
	javascript-unit-tests \
	javascript-integration-tests \
	javascript-simple-client-test

javascript-unit-tests:
	cd src/js ; node test-wrapper.js
	cd src/js ; node test-with-ws-server.js

javascript-integration-tests: \
	javascript-simple-client-test

javascript-simple-client-test:
	examples/run-hunchentoot-server-example & server_pid=$$! ;\
	sleep 60 ;\
	cd src/js ;\
	node client.js > $(THISDIR)/$(ARTDIR)/simple-client-actual.output ;\
	kill $$server_pid ;\
	cmp $(THISDIR)/$(ARTDIR)/simple-client-actual.output simple-client-expected.output

javascript-install-dependencies:
	npm install ws modern-isomorphic-ws isomorphic-ws s-expression.js

lisp-tests:
	sbcl --noinform --no-userinit --non-interactive \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-websockets.tests)' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text)' \
		--eval '(cl-naive-tests:save-results cl-naive-tests:*suites-results* :file "$(ARTDIR)junit-results.xml" :format :junit)' \
		--eval '(sb-ext:exit :code (if (cl-naive-tests:report) 0 200))'

documentation:
	make -C docs pdfs

clean:
	- rm -f *.tex missfont.log ./--version.lock
	-rm *~ system-index.txt
	-find . \( -name \*.[dlw]x[36][24]fsl -o -name --version.lock \) -exec rm {} +
	make -C docs clean


server_host     = "localhost"
server_port     = $(PORT)
server_resource = "/ws"
secure          = NIL
subprotocol     = "chat"
origin          = NIL # "http://localhost:$(PORT)/"

SBCL = sbcl --noinform --no-userinit --non-interactive
SBCL_QUIT = --eval '(sb-ext:exit :code 0)'
CCL  = ccl --quiet --no-init
CCL_QUIT = --eval '(ccl:quit)'
# LISP = $(CCL)
# LISP_QUIT = $(CCL_QUIT)

LISP = $(SBCL)
LISP_QUIT = $(SBCL_QUIT)

ws-chat-client:
	$(LISP) \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-websockets.example.client.drakma)' \
		--eval '(cl-naive-websockets.example.client.drakma:run-client $(server_host) $(server_port) $(server_resource) $(secure) $(subprotocol) $(origin) )' \
		$(LISP_QUIT)


ws-chat-server:
	$(LISP) \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-websockets.example.server.webserver.hunchentoot)' \
		--eval '(cl-naive-websockets.example.server.webserver.hunchentoot:run-server $(server_host) $(server_port) $(server_resource) $(secure) $(subprotocol) $(origin) )' \
		$(LISP_QUIT)

