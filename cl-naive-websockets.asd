(defsystem "cl-naive-websockets"
  :description "Implements the WebSockets protocol RFC6455"
  :version "2022.9.12"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:ironclad
               :cl-base64
               :trivial-utf-8
               :bordeaux-threads
               :usocket
               :split-sequence)
  :components ((:file "src/package")
               (:file "src/lower-layer"  :depends-on ("src/package"))
               (:file "src/classes"      :depends-on ("src/package"))
               (:file "src/conditions"   :depends-on ("src/package"))
               (:file "src/frame"        :depends-on ("src/package"))
               (:file "src/queue"        :depends-on ("src/package"))
               (:file "src/transmission" :depends-on ("src/package"
                                                      "src/classes"
                                                      "src/conditions"
                                                      "src/lower-layer"
                                                      "src/queue"))
               (:file "src/client"       :depends-on ("src/package"
                                                      "src/classes"
                                                      "src/conditions"
                                                      "src/lower-layer"))
               (:file "src/server"       :depends-on ("src/package"
                                                      "src/classes"
                                                      "src/conditions"
                                                      "src/lower-layer"))))

