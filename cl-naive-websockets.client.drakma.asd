(defsystem "cl-naive-websockets.client.drakma"
  :description "Drakma client connection for cl-naive-websockets"
  :version "2021.9.11"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-websockets
               :drakma)
  :components ((:file "src/package")
               (:file "src/utilities/socket-stream" :depends-on ("src/package"))
               (:file "src/drakma/client-connect"   :depends-on ("src/package"))))
