(defsystem "cl-naive-websockets.example.client.drakma"
  :description "Example of websockets client using Drakma"
  :version "2021.10.12"
  :author "Pascal Bourguignon <pjb@sbde.fr>"
  :licence "MIT"
  :depends-on (:cl-naive-websockets.client.drakma)
  :components ((:file "examples/drakma-client-example")))
