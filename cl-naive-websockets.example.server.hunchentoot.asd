(defsystem "cl-naive-websockets.example.server.hunchentoot"
  :description "Example of websockets server using Hunchentoot"
  :version "2021.10.5"
  :author "Pascal Bourguignon <pjb@sbde.fr>"
  :licence "MIT"
  :depends-on (:cl-naive-websockets.server.hunchentoot)
  :components ((:file "examples/hunchentoot-server-example")))
