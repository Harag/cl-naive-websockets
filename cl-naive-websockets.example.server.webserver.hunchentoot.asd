(defsystem "cl-naive-websockets.example.server.webserver.hunchentoot"
  :description "Example of websockets server using webserver/Hunchentoot"
  :version "2023.6.6"
  :author "Pascal Bourguignon <pjb@sbde.fr>"
  :licence "MIT"
  :depends-on (:cl-naive-websockets.server.hunchentoot ; uses hunchentoot directly
               :cl-naive-webserver.hunchentoot) ; here we go thu this webserver.
  :components ((:file "examples/webserver-example")))
