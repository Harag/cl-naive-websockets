(defsystem "cl-naive-websockets.example.server.webserver.hunchentoot.wrapper"
  :description "Example of websockets server using webserver/Hunchentoot and wrapper"
  :version "2023.11.4"
  :author "Pascal Bourguignon <pjb@ogamita.com>"
  :licence "MIT"
  :depends-on (:babel
               :cl-naive-log
               :cl-naive-websockets.server.hunchentoot ; uses hunchentoot directly
               :cl-naive-webserver.hunchentoot  ; here we go thu this webserver.
               :cl-naive-websockets.wrapper)
  :components ((:file "examples/webserver-wrapper-example")
               (:file "../cl-naive-log/src/log-hunchentoot-patch")))
