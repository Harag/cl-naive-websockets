(defsystem "cl-naive-websockets.server.hunchentoot"
  :description "Hunchentoot server connection for cl-naive-websockets"
  :version "2021.9.11"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-websockets
               :hunchentoot)
  :components ((:file "src/package")
               (:file "src/utilities/socket-stream"    :depends-on ("src/package"))
               (:file "src/hunchentoot/server-accept"  :depends-on ("src/package"))))
