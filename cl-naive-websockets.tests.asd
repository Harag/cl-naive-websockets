(defsystem "cl-naive-websockets.tests"
  :description "Tests for cl-naive-websockets."
  :version "2023.11.3"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-tests
               :cl-naive-websockets
               ;; :drakma :hunchentoot
               :cl-naive-websockets.tests.issue-14
               :cl-naive-websockets.tests.wrapper
               )
  :components (
               (:file "tests/package")
               (:file "tests/null-modem-layer"        :depends-on ("tests/package"))

               (:file "tests/test-queue"              :depends-on ("tests/package"))
               (:file "tests/test-frame"              :depends-on ("tests/package"
                                                                   "tests/null-modem-layer"))
               (:file "tests/test-transmission"       :depends-on ("tests/package"
                                                                   "tests/test-frame"))

               (:file "tests/test-null-modem-layer"   :depends-on ("tests/package"
                                                                   "tests/test-queue"
                                                                   "tests/null-modem-layer"))))
