(defsystem "cl-naive-websockets.tests.issue-14"
  :description "Tests cl-naive-websockets with a nodejs client."
  :version "2023.7.15"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-tests
               :cl-naive-websockets
               :cl-naive-websockets.example.server.hunchentoot
               ;; :cl-naive-websockets.example.client.drakma
               ;; :drakma :hunchentoot
               )
  :components (
               (:file "tests/issue-14/package")
               (:file "tests/issue-14/server"        :depends-on ("tests/issue-14/package"))
               (:file "tests/issue-14/issue-14"      :depends-on ("tests/issue-14/package"
                                                                  "tests/issue-14/server"))))

