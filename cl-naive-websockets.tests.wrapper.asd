(defsystem "cl-naive-websockets.tests.wrapper"
  :description "Tests cl-naive-websockets.wrapper."
  :version "2023.11.3"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-tests
               :cl-naive-websockets
               :cl-naive-websockets.wrapper
               )
  :components (
               (:file "tests/unit-wrapper")))

