(defsystem "cl-naive-websockets.wrapper"
  :description "Provides a set of utility functions to wrap messages
with parameters and payload in websockets packets."
  :version "2023.10.24"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-websockets
               :cl-naive-log
               :babel)
  :components ((:file "src/wrapper")))

