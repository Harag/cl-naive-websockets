# -*- mode:org;coding:utf-8 -*-

* rfc6455 The WebSockets Protocol
** Opening Handshake

#+BEGIN_EXAMPLE
GET /chat HTTP/1.1
Host: server.example.com
Upgrade: websockets
Connection: Upgrade
Sec-WebSockets-Key: dGhlIHNhbXBsZSBub25jZQ==
Origin: http://example.com
Sec-WebSockets-Protocol: chat, superchat
Sec-WebSockets-Version: 13
#+END_EXAMPLE

Order of Header is not significant (rfc2616)

=Request-URI= identifies tne endpoint of the WebSockets connection both
to allow multiple domains to be served from one IP, and to allow
multiple WebSockets endpoints to be served by a single server.

=Host= header validates both client and server agree on which host
(server) is used.

=Sec-WebSockets-Protocol= subprotocol selector (application-level protocol).

=Sec-WebSockets-Extensions=

=Origin= (RFC6454) indicates the origin of the script using WebSockets
in the web browser. The server may acept or reject it. (optional for
non-browser clients).

=Sec-WebSockets-Key= is a base-64  value. The server concatenates it
with Globally Unique Identifier (GUID, [RFC4122])
"258EAFA5-E914-47DA-95CA-C5AB0DC85B11", and SHA-1 hash it (160-bit),
base64-encode it, and returns it to the client to validate the
WebSockets handshake.


#+BEGIN_EXAMPLE
HTTP/1.1 101 Switching Protocols
Upgrade: websockets
Connection: Upgrade
Sec-WebSockets-Accept: s3pPLMBiTxaQ9kYGzzhZRbK+xOo=
#+END_EXAMPLE

*** WebSockets Client

A WebSockets Client must:
- prepare the header requesting the upgrade to websockets,
- perform the GET request,
- validate the response headers,
If no error, switch to the WebSockets frame exchanges.

Request hooks:
- set the /host/
- set the /port/
- set the /resource name/ (endpoint)
- set the /secure flag/ (tls?)
- set the list of /subprotocols/
- set the list of /extensions/
- set the /origin/ (when the client is a web browser)

/resource name/ includes =path?query=  (fragments are meaningless and must not be used).
/secure flag/ = =wss://= instead of =ws://= (ie. ssl/tls).

Validation hooks:
- validate the upgrade
- validate the accept key

*** WebSockets Server

A WebSockets Server must:
- receive a Get request
- analyse and validate the headers,
- generate the response headers,
If no error, switch to the WebSockets frame exchanges.

Validation hooks:
- compute the key
- check the host
- check the endpoint
- check the origin
- check the subprotocols

** Closing Handshake

Either peer can initiate it.

- Send a control frame to begin the closing handshake.
- The peer sends a close frame response.
- When close frame is received, send a close frame response,
  and the connection is closed.

** Data I/O
*** RFC

5. Data Framing

| client -> server | client must MASK the frame.     |
| server -> client | server must NOT MASK the frame. |


client receives a frame:
- if masked, 1002
- process


client sends a a frame
- mask the frame
- send masked




5.2. Base Framing Protocol

| #x0     | denotes a continuation frame                |
| #x1     | denotes a text frame                        |
| #x2     | denotes a binary frame                      |
| #x3-#x7 | are reserved for further non-control frames |
| #x8     | denotes a connection close                  |
| #x9     | denotes a ping                              |
| #xA     | denotes a pong                              |
| #xB-#xF | are reserved for further control frames     |


Frames can be fragmented.
- message size unknown when started sending.
- multiplexing (a possible extension)

- fragments are delivered in the same order as sent.
- control frames may be injected in the middle of a framented message.
- control frames cannot be fragmented.
- fragment of one message MUST NOT be interleaved between the
  fragments of another unless an extension has been negociated
  (multiplexing).


5.5.1 Close

Close frames can be sent in the middle of a fragmented message, or
wait for the end of it.

When receiving a close frame, the endpoint can send the answer close
frame in the middle of a fragmented message, or wait for the end of
it, but no guarantee that the other endpoint will process the
fragmented message if received completely.

5.5.2 Ping

Ping -> Pong

5.6 Data frames

- Text (utf-8)
- Binary (octets)



6.1. Sending Data



   1.  The endpoint MUST ensure the WebSockets connection is in the OPEN
      state (cf. Sections 4.1 and 4.2.2.)  If at any point the state of
      the WebSockets connection changes, the endpoint MUST abort the
      following steps.

   2.  An endpoint MUST encapsulate the /data/ in a WebSockets frame as
      defined in Section 5.2.  If the data to be sent is large or if
      the data is not available in its entirety at the point the
      endpoint wishes to begin sending the data, the endpoint MAY
      alternately encapsulate the data in a series of frames as defined
      in Section 5.4.

   3.  The opcode (frame-opcode) of the first frame containing the data
      MUST be set to the appropriate value from Section 5.2 for data
      that is to be interpreted by the recipient as text or binary
      data.

   4.  The FIN bit (frame-fin) of the last frame containing the data
      MUST be set to 1 as defined in Section 5.2.

   5.  If the data is being sent by the client, the frame(s) MUST be
      masked as defined in Section 5.3.

   6.  If any extensions (Section 9) have been negotiated for the
      WebSockets connection, additional considerations may apply as per
      the definition of those extensions.

   7.  The frame(s) that have been formed MUST be transmitted over the
      underlying network connection.


6.2. Receiving Data


To receive WebSockets data, an endpoint listens on the underlying
network connection.  Incoming data MUST be parsed as WebSockets frames
as defined in Section 5.2.  If a control frame (Section 5.5) is
received, the frame MUST be handled as defined by Section 5.5.  Upon
receiving a data frame (Section 5.6), the endpoint MUST note the
/type/ of the data as defined by the opcode (frame-opcode) from
Section 5.2.  The "Application data" from this frame is defined as
the /data/ of the message.  If the frame comprises an unfragmented
message (Section 5.4), it is said that _A WebSockets Message Has Been
Received_ with type /type/ and data /data/.  If the frame is part of
a fragmented message, the "Application data" of the subsequent data
frames is concatenated to form the /data/.  When the last fragment is
received as indicated by the FIN bit (frame-fin), it is said that _A
WebSockets Message Has Been Received_ with data /data/ (comprised of
the concatenation of the "Application data" of the fragments) and
type /type/ (noted from the first frame of the fragmented message).
Subsequent data frames MUST be interpreted as belonging to a new
WebSockets message.

Extensions (Section 9) MAY change the semantics of how data is read,
specifically including what comprises a message boundary.
Extensions, in addition to adding "Extension data" before the
"Application data" in a payload, MAY also modify the "Application
data" (such as by compressing it).

A server MUST remove masking for data frames received from a client
as described in Section 5.3.

*** API

WebSockets protocol uses an underlying protocol, which can be HTTP, or
HTTPS (TLS on HTTP).

1- Connecting involves HTTP protocols to negociate and upgrade to websockets.

2- Then we have a normal socket (over TCP or over TLS/TCP).

3- WebSockets can send and receive messages (frames)

4- Closing involves close WebSockets frames, then closing cleanly the
underlying protocol (TCP or TLS/TCP).


Lower layer protocol:

#+BEGIN_SRC lisp
  (defgeneric connect        (layer host port secure resource-name headers)) ; for client layers
  (defgeneric accept         (layer host port secure)) ; for server layers
  (defgeneric send-octet     (layer byte))
  (defgeneric send-octets    (layer buffer &key start end))
  (defgeneric receive-octet  (layer)) ; -> octet
  (defgeneric receive-octets (layer buffer &key start end))
  (defgeneric disconnect     (layer))
#+END_SRC


WebSockets classes:

#+BEGIN_SRC lisp
  (defclass websockets-endpoint ()
    ())
  (defclass websockets-server (websockets-endpoint)
    ())
  (defclass websockets-client (websockets-endpoint)
    ())

  (defclass websockets-message ()
    ())
  (defclass websockets-control-message (websockets-message)
    ())
  (defclass websockets-pong-message (websockets-control-message)
    ((data)
     (elapsed-time)))
  (defclass websockets-data-message (websockets-message)
    ((data)))
  (defclass websockets-text-message (websockets-data-message)
    ())
  (defclass websockets-binary-message (websockets-data-message)
    ())
#+END_SRC


WebSockets conditions:

#+BEGIN_SRC lisp
  (define-condition websockets-condition (condition) ())
  (define-condition websockets-error (websockets-condition) ())
  (define-condition websockets-closed-error (end-of-file websockets-error) ())
#+END_SRC


WebSockets methods:

#+BEGIN_SRC lisp
  (defgeneric message-elapsed-time (websockets-pong-message)) ; -> double-float
  (defgeneric message-string (websockets-text-message)) ; -> string
  (defgeneric message-data (websockets-data-message)) ; -> (vector octet)

  (defun websockets-connect  (host resource-name
                             &key (port 80) (secure nil)
                               (subprotocols '())
                               (extensions '())
                               (origin nil) ; must be a "scheme://host[:port]" string.
                               (additionnal-http-headers '()) ; passed to the http-client
                               layer)) ; --> websockets-client

  (defun websockets-accept (host &key (port 80) (secure nil)
                                  layer)) ; -> websockets-server


  (defgeneric send-text-message    (websockets-endpoint text   &key may-fragment))
  (defgeneric send-binary-message  (websockets-endpoint binary &key may-fragment))

  (defgeneric send-text-fragment   (websockets-endpoint text-fragment   &key final))
  (defgeneric send-binary-fragment (websockets-endpoint binary-fragment &key final))

  (defgeneric send-ping-control (websockets-endpoint data))
  (defgeneric send-pong-control (websockets-endpoint data)) ; unsolicited heartbeat.

  (defgeneric receive-message (websockets-endpoint &optional eof-error-p eof-value)) ; --> websockets-message

  (defgeneric websockets-disconnect (websockets-endpoint))
#+END_SRC

For reception we have either a stream API (reading),
or an event API (receiving messages from wherever the data comes from).

When using the stream API, we may have to use threads (to read and
write at the same time).

With an event API, we need a thread to read until something is
received to send the synchronous event.

*** Design

All the messages to the =websockets-client= or =websockets-server= objects
may be sent asynchronously from multiple-threads.

But those object don't have a specific thread or an event loop.
Processing is performed in the send or receive messages, notably:

- the send messages may either queue frames, or send the lower layer
  =send-octets= message, which may call a blocking =send= or =write=
  on a socket.

- the receive message may either return a queued message, or send the
  lower layer =receive-octets= message, which may call a blocking
  =recv= or =read= on a socket.  It may also queue frames for sending,
  or send the =send-octets= message to the lower layer (and thus block
  on a call to =send= or =write= on the socket).

Therefore the client must call receive periodically, to receive the
frames from the peer endpoint, and to process them.

* Design
** Client

A client calls the function =websockets-connect= to establish a websocket connection.

#+begin_src lisp
  websockets-connect (host resource-name
                           &key (port 80) (secure nil)
                           (subprotocols '())
                           (extensions '())
                           (origin nil) ; must be a "scheme://host[:port]" string.
                           (additionnal-http-headers '()) ; passed to the http-client
                           http-client-connect-function)
#+end_src

The =websocket-connect= function implements the client-side WebSockets
protocol, computing the HTTP headers to negociate the upgrade to
WebSockets, and pass them to the function passed in the
=http-client-connect-function= parameter.

The =http-client-connect-function= parameter is a function that
performs the actual connection to the HTTP/HTTPS server; it returns
the headers from server, which are returned to the =websocket-connect=
function, along with the socket stream (or nil in case of error), and
/socket-info/.  Socket-info is a list of the form:

#+begin_example
((local-address local-port) (remote-address remote-port))
#+end_example

#+begin_src lisp
  (defun http-client-connect (host port secure resource-name headers)
    ;; Open the http connection.
    ;; If secure, use ssl/tls.
    ;; Send the GET request HTTP/1.1 or HTTPS/1.1
    ;; Send the headers, with keep-alive and other options.
    ;; Get the server headers
    ;; Return them with the connection stream.
    ;; If an error occurn, close the connection and return the
    ;; server-header with the status header.
    ;; 1.   The handshake MUST be a valid HTTP request as specified by
    ;;      [RFC2616].
    ;;
    ;; 2.   The method of the request MUST be GET, and the HTTP version MUST
    ;;      be at least 1.1.
    ;;
    ;;      For example, if the WebSocket URI is "ws://example.com/chat",
    ;;      the first line sent should be "GET /chat HTTP/1.1".
    ;;
    ;; 3.   The "Request-URI" part of the request MUST match the /resource
    ;;      name/ defined in Section 3 (a relative URI) or be an absolute
    ;;      http/https URI that, when parsed, has a /resource name/, /host/,
    ;;      and /port/ that match the corresponding ws/wss URI.
    (not-implemented-yet)
    (values (acons :status-code status-code server-headers)
            stream
            socket-info))
#+end_src

The =websocket-connect= function returns a
=websockets-client-endpoint= object that can be used to send and
receive data and control messages.

** Server

The WebSockets server is implemented thru a http/https server that
will perform the initial HTTP negociation and upgrade to the
WebSockets protocol.

Typically:

The http server is configured to listen on a /host/ & /port/,
and to dispatch to an handler depending on the /resource name/.

The http server can be customized with subclasses and overriden
methods that let us piggyback on the server.

A websocket handler is registered with a http server on a /resource
name/, and with a =websockets-server= instance.

When an incoming connection arrives on the WebSockets resource, a
method on our handler is called, that will send the
=websockets-server-negociate= message to the =websockets-server=
instance; it will answer with a set of headers to be replied to the
client, and an =websockets-server-endpoint= instance to be registered
with the http request.

Once the answer is sent, the handler will call the
=websockets-server-server= method of the =websockets-server=
instance, passing it the connection stream and the
=websockets-server-endpoint= instance.

The same =websockets-server= instance can be used to serve multiple
http requests for different clients, each with its own
=websockets-server-endpoint= instance.

The =websockets-server-negociate= method takes a http-request object
(which can be the actual http server request instance, or a proxy
object, used to interface with the =websockets-server= instance, and
retrieve auxiliary information about the request: local and peer
address and port, the resource name, peer certificate and key when a
secure connection is used, etc.



#+begin_src lisp
    (defmethod websockets-listen (http-server
                                  host port secure resource-name
                                  websocket-server-function
                                  &key
                                    (subprotocols '())
                                    (extensions '())
                                    validate-origin ; a function to validate the client origin
                                    ))
#+end_src
