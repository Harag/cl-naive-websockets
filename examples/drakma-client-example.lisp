(defpackage :cl-naive-websockets.example.client.drakma
  (:use
   :common-lisp
   :cl-naive-websockets)
  (:export :chat :print-endpoint :ws-chat-client
           :run-client))
(in-package :cl-naive-websockets.example.client.drakma)


(defun chat (endpoint)
  (with-debugging
    (format t "~&Enter:  quit   to end, or a message to send to the server.~%")
    (loop
       (when (endpoint-closedp endpoint)
         (return))

       (let ((message (receive-message endpoint)))
         (typecase message
           (websockets-message-fragment
            (format t "~&Client Received a message fragment ID ~A opcode ~A ~S (~A bytes)~%~S~%"
                    (message-fragment-id message)
                    (message-fragment-opcode message)
                    (message-fragment-position message)
                    (length (message-fragment-data message))
                    (message-data message)))
           (websockets-close-message
            (let ((data (message-data message)))
              (if (< 2 (length data))
                  (format t "~&Client Received a close control message ~A ~S~%"
                          (dpb (aref data 0) (byte 8 8) (aref data 1))
                          (handler-case
                              (trivial-utf-8:utf-8-bytes-to-string data :start 2)
                            (trivial-utf-8:utf-8-decoding-error (err)
                              (websockets-error endpoint 1002 "utf-8 decoding error: ~A" err))))
                  (format t "~&Client Received an illegal close control message~%")))
            (return))
           (websockets-pong-message
            (format t "~&Client Received a pong control message ~A ~S~%"
                    (message-elapsed-time message) (message-data message)))
           (websockets-ping-message
            (format t "~&Client Received a ping control message ~S~%" (message-data message)))
           (websockets-control-message
            (format t "~2%SHOULD NOT OCCUR!~%")
            (format t "~&Client Received a control message (abstract class)~%"))
           (websockets-text-message
            (format t "~&Client Received a text message ~S~%" (message-string message)))
           (websockets-binary-message
            (format t "~&Client Received a binary message ~S~%" (message-data message)))
           (websockets-data-message
            (format t "~2%SHOULD NOT OCCUR!~%")
            (format t "~&Client Received a data message (abstract class) ~S~%" (message-data message)))
           (websockets-message
            (format t "~2%SHOULD NOT OCCUR!~%")
            (format t "~&Client Received a message (abstract class)~%"))
           (t
            (format t "~2%SHOULD NOT OCCUR!~%")
            (format t "~&Client Received something of type ~S: ~S~%" (type-of message) message))))

       (format *query-io* "~&Text Message: ")
       (finish-output *query-io*)
       (let ((text (read-line *query-io*)))
         (when (string-equal "quit" (string-trim " " text))
           (return))
         (send-text-message endpoint text)))))


(defun print-endpoint (endpoint &optional (stream t))
  (typecase endpoint
    (websockets-endpoint
     (loop
       :for reader :in '(endpoint-extensions endpoint-subprotocol endpoint-cookies
                         cl-naive-websockets::lower-layer
                         endpoint-local-address endpoint-local-port
                         endpoint-remote-address endpoint-remote-port
                         endpoint-closedp)
       :do (format stream "~32A  ~S~%"
                   (let ((name (symbol-name reader)))
                     (if (and (< (length "endpoint-") (length name))
                              (string-equal "endpoint-" name))
                         (subseq name (length "endpoint-"))
                         name))
                   (funcall reader endpoint))))
    (t
     (format stream "~S~%" endpoint))))


(defun ws-chat-client (remote-host remote-port remote-resource
                       &key (secure nil) (subprotocol "chat") origin)
  (let ((subprotocols                 (list subprotocol))
        (extensions                   '())
        (additionnal-http-headers     '())
        (http-client-connect-function (function cl-naive-websockets.client.drakma:drakma-http-client-connect)))
    (format *trace-output* "~&Connecting to ws://~A:~A~A~%" remote-host remote-port remote-resource)
    (force-output *trace-output*)
    (let ((endpoint (websockets-connect
                     remote-host remote-resource
                     :port remote-port
                     :secure secure
                     :subprotocols subprotocols
                     :extensions extensions
                     :origin origin
                     :additionnal-http-headers additionnal-http-headers
                     :http-client-connect-function http-client-connect-function)))
      (if endpoint
          (unwind-protect
               (progn
                 (format *trace-output* "~&Connected~%")
                 (print-endpoint endpoint *trace-output*)
                 (force-output *trace-output*)
                 (chat endpoint))
            (websockets-disconnect endpoint)
            (format *trace-output* "~&Disconnected~%")
            (force-output *trace-output*))
          (progn
            (format *error-output* "~&Could not connect~%")
            (force-output *error-output*))))))


(defun query-string (prompt)
  (fresh-line *query-io*)
  (write-string prompt *query-io*)
  (finish-output *query-io*)
  (string-trim #(#\space #\tab) (read-line *query-io*)))

(defun query-integer (prompt)
  (fresh-line *query-io*)
  (write-string prompt *query-io*)
  (finish-output *query-io*)
  (parse-integer (string-trim #(#\space #\tab) (read-line *query-io*))))

(defun query-boolean (prompt)
  (fresh-line *query-io*)
  (write-string prompt *query-io*)
  (finish-output *query-io*)
  (not (not (read *query-io*))))

(defun run-client (remote-host remote-port remote-resource &optional (secure nil securep) (subprotocol "chat") (origin nil originp))
  (ws-chat-client
   (or remote-host (query-string "Remote host: "))
   (or remote-port (query-integer "Remote port: "))
   (or remote-resource (query-string "Remote resource: "))
   :secure (if securep secure (query-boolean "Secure (nil): "))
   :subprotocol (or subprotocol (query-string "Subprotocol (chat): "))
   :origin (if originp origin nil)))
