(defpackage :cl-naive-websockets.example.server.hunchentoot
  (:use
   :common-lisp
   :cl-naive-websockets))
(in-package :cl-naive-websockets.example.server.hunchentoot)

(defun format-log (control-string &rest arguments)
  (hunchentoot:acceptor-log-message hunchentoot:*acceptor* :info "~?" control-string arguments))

(defun example-ws-server (server endpoint)
  (declare (ignore server))
  (with-debugging
    (unwind-protect
         (loop
           :initially (send-text-message endpoint "HELLO I will echo text and binary messages")
           :for message := (receive-message endpoint)
           :do (with-output-to-string (*standard-output*)
                 (typecase message
                   (websockets-message-fragment
                    (format-log "Server Received a message fragment ID ~A opcode ~A ~S (~A bytes) ~S"
                            (message-fragment-id message)
                            (message-fragment-opcode message)
                            (message-fragment-position message)
                            (length (message-fragment-data message))
                            (message-data message))
                    (when (eql :final (message-fragment-position message))
                      (send-text-message endpoint "Got last fragment.")))
                   (websockets-close-message
                    (let ((data (message-data message)))
                      (if (< 2 (length data))
                          (format-log "Server Received a close control message ~A ~S"
                                      (close-message-status message)
                                      (close-message-reason message))
                          (format-log "Server Received an illegal close control message")))
                    (loop-finish))
                   (websockets-pong-message
                    (format-log "Server Received a pong control message ~A ~S"
                            (message-elapsed-time message) (message-data message)))
                   (websockets-ping-message
                    (format-log "Server Received a ping control message ~S" (message-data message)))
                   (websockets-control-message
                    (format-log "SHOULD NOT OCCUR!")
                    (format-log "Server Received a control message (abstract class)"))
                   (websockets-text-message
                    (format-log "Server Received a text message ~S" (message-string message))
                    (send-text-message endpoint (format nil "Server Received a text message ~S" (message-string message))))
                   (websockets-binary-message
                    (format-log "Server Received a binary message ~S" (message-data message))
                    (send-binary-message endpoint (message-data message)))
                   (websockets-data-message
                    (format-log "SHOULD NOT OCCUR!")
                    (format-log "Server Received a data message (abstract class) ~S" (message-data message))
                    (send-text-message endpoint (format nil "Server Received a data message (abstract class) ~S" (message-data message))))
                   (websockets-message
                    (format-log "SHOULD NOT OCCUR!")
                    (format-log "Server Received a message (abstract class)")
                    (send-text-message endpoint (format nil "Server Received a message (abstract class)")))
                   (null
                    (format-log "Server Received a null message: the other side disconnected.")
                    (ignore-errors (websockets-disconnect endpoint :reason "Other side disconnected." :status +ws-protocol-error+))
                    (loop-finish))
                   (t
                    (format-log "SHOULD NOT OCCUR!")
                    (format-log "Server Received something of type ~S: ~S" (type-of message) message)
                    (ignore-errors (websockets-disconnect endpoint :reason "Received an unknown message type" :status +ws-protocol-error+))
                    (loop-finish)))))
      (ignore-errors (websockets-disconnect endpoint)))))


;; This example acceptor will take only one resource-name and direct
;; it to the EXAMPLE-WS-SERVER.  Other resources will defer to normal
;; hunchentoot with default processing.


(defclass example-ws-server (websockets-server)
  ()
  (:default-initargs
   :subprotocols '("chat")
   :server-function (function example-ws-server)))

(defclass example-resource-mixin ()
  ((resource-name :initarg :resource-name :reader resource-name)))

(defclass example-acceptor (example-resource-mixin websockets-acceptor-mixin hunchentoot:acceptor)
  ())

(defclass example-ssl-acceptor (example-resource-mixin websockets-acceptor-mixin hunchentoot:ssl-acceptor)
  ())


(defmethod hunchentoot:acceptor-dispatch-request ((acceptor example-resource-mixin) request)
  (hunchentoot:acceptor-log-message acceptor :warn
                                    "script-name   = ~S~%resource-name = ~S~%"
                                    (hunchentoot:script-name request) (resource-name acceptor))
  (if (and (string= (hunchentoot:script-name request) (resource-name acceptor))
           (accept-websockets-negociation acceptor request))
      ;; It was a websocket request:
      (values "" nil nil)
      ;; Otherwise, defer to the superclass.
      (when (next-method-p)
        (call-next-method))))

(defun start-server (host port secure resource-name)
  (let* ((server   (make-instance 'example-ws-server))
         (acceptor (make-instance (if secure
                                      'example-ssl-acceptor
                                      'example-acceptor)
                                  :port port
                                  :address host
                                  :resource-name resource-name
                                  :access-log-destination  *error-output*
                                  :message-log-destination *error-output*
                                  :websockets-server server)))
    (hunchentoot:start acceptor)))

(defun stop-server (server)
  (hunchentoot:stop server))

