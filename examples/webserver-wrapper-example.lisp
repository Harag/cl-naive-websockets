(defpackage :cl-naive-websockets.example.server.webserver.hunchentoot.wrapper
  (:documentation "This is a server using webserver.hunchentoot and wrapper,
used to test the javascript wrapper.")
  (:use
   :common-lisp
   :cl-naive-log
   :cl-naive-webserver
   :cl-naive-webserver.hunchentoot
   :cl-naive-websockets
   :cl-naive-websockets.wrapper)
  (:export :run-server :*port* :*start-own-server* :*server-log-file*))
(in-package :cl-naive-websockets.example.server.webserver.hunchentoot.wrapper)

(defparameter *runtime-package-name* :cl-naive-websockets.example.server.webserver.hunchentoot.wrapper)

(defvar *port* 33004)
 
(defvar *server-host* "localhost")
(defvar *server-port* *port*)
(defvar *secure* nil)
(defvar *server-resource* "/ws")


(defvar *server* nil)
(defvar *token* nil)
(defvar *cookie-jar* nil)
(defvar *site* nil)
(defvar *site-with-credentials* nil)
(defvar *site-with-sessions* nil)

(eval-when (:compile-toplevel)
 (defvar *source-dir* (make-pathname :name nil :type nil :version nil
				     :defaults *compile-file-truename*)))




(defun format-log (control-string &rest arguments)
  (hunchentoot:acceptor-log-message hunchentoot:*acceptor*
                                    :info "~?" control-string arguments))


#|

client sends:     T (:payload "SEND text \"payload\"" :request "text" :source "parameter")
server responds:  T (:payload "SEND text \"payload\"" :source "parameter")

client sends:     B (:binary  4 :request "text" :source "parameter")ABCD
server responds:  T (:payload "ABCD" :source "parameter")

client sends:     T (:payload "SEND text \"payload\""  :request "binary" :source "parameter")
server responds:  B (:binary 19 :source "parameter")SEND text "payload"

client sends:     B (:binary 4 :request "binary" :source "parameter")ABCD
server responds:  B (:binary 4 :source "parameter")ABCD

client sends:     T (:quit "Bye")
server disconnets.

|#

(defun wrapper-test-ws-server (server endpoint)
  ;; This is the local-websockets-service websockets server loop
  ;; It's installed in instances of the service-ws-server class.
  (with-debugging
    ;; (setf (cl-naive-websockets:endpoint-upper-layer endpoint) service)
    (log-message :info endpoint "Starting ~S loop" 'service-ws-server)
    (unwind-protect
         (loop
           :do (with-error-reporting
                 (multiple-value-bind (status parameters payload)
                     (receive-parameters-payload endpoint :status :binary :payload)
                   (when (string= status "closed")
                     (loop-finish))
                   (handler-case
                       (multiple-value-bind (parameters payload)
                           (let ((request (getf parameters :request)))
                             (cond ((string-equal request "text")
                                    (values parameters
                                            (if (stringp payload)
                                                payload
                                                (if (every (lambda (code) (<= 32 code 126)) payload)
                                                    (map 'string (function code-char) payload)
                                                    (prin1-to-string payload)))))
                                   ((string-equal request "binary")
                                    (values parameters
                                            (if (stringp payload)
                                                (babel:string-to-octets payload :encoding :utf-8)
                                                payload)))
                                   (t
                                    (error "Please send a message with :request \"text\" or \"binary\" in the parameters."))))
                         (send-parameters-payload endpoint (list* :status "ok" (remove-indicator parameters :binary))
                                                  payload :binary :payload))
                     (error (err)
                       (log-message :error endpoint "Error handling the request: ~A" err)
                       (send-parameters-payload endpoint (list* :status "error" (remove-indicator parameters :binary))
                                                (format nil "~a" err) :binary :payload))))))
      ;; cleanup
      (ignore-errors (with-error-reporting
                       (cl-naive-websockets:websockets-disconnect endpoint))))))


;; This example acceptor will take only one resource-name and direct
;; it to the EXAMPLE-WS-SERVER.  Other resources will defer to normal
;; hunchentoot with default processing.

(defclass example-ws-server (websockets-server)
  ()
  (:default-initargs
   :subprotocols '("wrapper")
   :server-function (function wrapper-test-ws-server)))

(defclass example-resource-mixin ()
  ((resource-name :initarg :resource-name :reader resource-name)))

(defclass example-acceptor (example-resource-mixin websockets-acceptor-mixin hunchentoot-acceptor)
  ;; hunchentoot-acceptor isa (exclusion-acceptor-mixin token-acceptor-mixin session-acceptor-mixin
  ;;                           site-acceptor-mixin hunchentoot:acceptor)
  ())

(defclass example-ssl-acceptor (example-resource-mixin websockets-acceptor-mixin hunchentoot:ssl-acceptor hunchentoot-acceptor) ; TODO
  ())

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor example-resource-mixin) request)
  (hunchentoot:acceptor-log-message acceptor :warn
                                    "script-name   = ~S~%resource-name = ~S~%"
                                    (hunchentoot:script-name request) (resource-name acceptor))
  (if (and (string= (hunchentoot:script-name request) (resource-name acceptor))
           (accept-websockets-negociation acceptor request))
      ;; It was a websocket request:
      (values "" nil nil)
      ;; Otherwise, defer to the superclass.
      (when (next-method-p)
        (call-next-method))))


(defun start-websocket-server (host port secure resource-name)
  "Creates a websockets-server, thru a webserver with a hunchentoot backend.
Return the new acceptor = hunchentoot server."
  (let ((websockets-server (make-instance 'example-ws-server))
	(webserver-server  (make-instance 'hunchentoot-server
                                          :id "websocket/webserver/hunchentoot/wrapper example"
                                          :port port
                                          :address host)))
    ;; (setf (port webserver-server) port
    ;;       (address webserver-server) host
    ;;       ;; (request-class webserver-server) 'hunchentoot:acceptor-request
    ;;       )
    (setf (acceptor webserver-server) ; = acceptors are hunchentoot server
          (make-instance (if secure
                             'example-ssl-acceptor
                             'example-acceptor)
                         :port port
                         :address host
                         :server webserver-server
                         
                         :resource-name resource-name
                         :access-log-destination  *error-output*
                         :message-log-destination *error-output*
                         :websockets-server websockets-server))
    (start-server webserver-server)))


(defun stop-hunchentoot-server (acceptor)
  (hunchentoot:stop acceptor))


;;; --------------------


(eval-when (:compile-toplevel)
  (defvar *source-dir* (make-pathname :name nil :type nil :version nil
				      :defaults *compile-file-truename*)))

(defmethod authenticate ((credentials credentials-basic) &key &allow-other-keys)
  (if (and (equalp (user-name credentials) "Piet")
           (equalp (password credentials) "Snot"))
      (make-instance 'authorization
                     :credentials credentials)))

(defgeneric get-urls (site)
  (:method ((site site))
    (let ((url (url site))
          (urls '()))
      (maphash (lambda (path handler)
                 (declare (ignore handler))
                 (push (format nil "http://localhost:~A~A~A" *port* url path) urls))
               (handlers site))
      urls)))

(defun get-cookie (name)
  (cdr (assoc name (hunchentoot:cookies-in*) :test (function equalp))))

(defun serve-an-error ()
  (/ 3 0))


(defun setup ()
  (setf *server* (make-instance 'cl-naive-webserver.hunchentoot::hunchentoot-server
                                :id "test"
                                :port *port*))

  (register-server *server*)

  (setf *server* (start-websocket-server *server-host* *server-port* *secure* *server-resource*))

  ;; -------------------- 

  (setf *site* (register-site (make-instance 'site  :url "/wrapper")))

  ;; test a static file thru exclusion-acceptor-mixin
  (setf (cl-naive-webserver:find-resource *site* "/favicon.ico")
	#.(make-pathname :defaults *source-dir* :name "favicon" :type "ico"))

  ;; -------------------- 

  (flet ((create-site (url &key sessions-p credentials-class)
	   (let ((site (register-site (make-instance 'site  
                                                     :url url
                                                     :credentials-class credentials-class))))
             (setf (sessions-p site) sessions-p)

             (setf (cl-naive-webserver:find-resource site "/example")
                   (lambda (script-name)
                     (setf (cl-naive-webserver:return-code cl-naive-webserver:*reply*) 200)
                     (push (cons :content-type "text/plain") (cl-naive-webserver:headers cl-naive-webserver:*reply*))
	             (format nil "Yeeeha she is a live and kicking!~2%script-name  = ~S~%cookie yummy = ~S~2%"
                             script-name
                             (get-cookie "yummy"))))

             (setf (cl-naive-webserver:find-resource site "/error")
                   (lambda (script-name)
                     (declare (ignore script-name))
                     (serve-an-error)))

             (setf (cl-naive-webserver:find-resource site "/example-cookie")
                   (lambda (script-name)
                     (setf (cl-naive-webserver:return-code cl-naive-webserver:*reply*) 200)
                     (push (cons :content-type "text/plain") (cl-naive-webserver:headers cl-naive-webserver:*reply*))
                     (set-cookie "yummy" :value "cookie")
                     (format nil "Yeeeha she is a live and kicking!~2%script-name  = ~S~%cookie yummy = ~S~%Check headers for cookie.~2%"
                             script-name
                             (get-cookie "yummy"))))
             site)))

    (setf *site*                   (create-site "/basic"))
    (setf *site-with-sessions*     (create-site "/session" :sessions-p t))
    (setf *site-with-credentials*  (create-site "/authed"  :sessions-p t :credentials-class 'credentials-basic))) 

  (finish-output *standard-output*)
  (format *trace-output* "~&DID SET UP~%")
  (finish-output *trace-output*)
  *server*)

(defun teardown ()
  (finish-output *standard-output*)
  (format *trace-output* "~&WILL TEARDOWN~%")
  (finish-output *trace-output*)
  (when *server*
    (stop-hunchentoot-server *server*)
    (when *site-with-credentials* (deregister-site *site-with-credentials*))
    (when *site-with-sessions*    (deregister-site *site-with-sessions*))
    (when *site*                  (deregister-site *site*))
    (deregister-server (server *server*))
    (setf *server* nil
          *site* nil
          *site-with-sessions* nil
          *site-with-credentials* nil)
    (sleep 1)))

(defmacro with-test-environment (&body body)
  `(handler-bind ((USOCKET:ADDRESS-IN-USE-ERROR
                    (lambda (condition)
                      (cl-naive-webserver::report-error condition)
                      nil))
                  (error
                    (lambda (condition)
                      (cl-naive-webserver::report-error condition)
                      nil)))
     (unwind-protect
          (progn (setup)
                 ,@body)
       (teardown))))


(defun list-server-urls ()
  (format t "~&Urls:")
  (map nil 'print (append (get-urls *site*)
                          (get-urls *site-with-sessions*)
                          (get-urls *site-with-credentials*)))
  (terpri))

(defun list-server-sites ()
  (format t "~&Sites:~%")
  (maphash (lambda (k v)
             (declare (ignore v))
             (format t "  ~S~%" k))
           cl-naive-webserver::*sites*))


;; (defvar *quit-server* nil)
;; (defvar *setup-teardown-thread* nil)
;; (defun quit ()
;;   (when *setup-teardown-thread*
;;     (setf *quit-server* t)
;;     (bt:join-thread *setup-teardown-thread*)
;;     (setf *quit-server* nil
;;           *setup-teardown-thread* nil))
;;   #+ccl (ccl:quit)
;;   #+sbcl (sb-ext:quit))
;; 
;; #+(or ccl sbcl) (shadowing-import 'quit "CL-USER")
;; 
;; #+sbcl
;; (let ((counter 0))
;;   (defun enclosed-prompt (stream)
;;     (format stream "~2%S/~A[~D]> "
;;             (if (packagep *package*)
;;                 (first (sort (cons (package-name *package*)
;;                                    (copy-list (package-nicknames *package*)))
;;                              (function <=) :key (function length)))
;;                 "#<INVALID *PACKAGE*>")
;;             (incf counter))))
;; 
;; #+sbcl
;; (defun prompt (stream)
;;   (enclosed-prompt stream))
;; 
;; #+sbcl
;; (setf sb-int:*repl-prompt-fun* (function prompt))

;; (defun run-server ()
;;   "Starts the test server from a separate thread, and wait for *quit-server* to tear it down."
;;   (setf *package* (find-package *runtime-package-name*))
;;   (setf *setup-teardown-thread*
;;         (bt:make-thread
;;          (lambda ()
;;            (with-test-environment
;;              (format t "Welcome to the cl-naive-webserver test server.~2%")
;;              (format t "Interesting commands: ~{~A~^ ~}~%" '((list-server-urls) (list-server-sites) (quit)))
;;              (loop :do (sleep 1)
;;                    :until *quit-server*)))
;;          :name "server-setup-teardown")))

(defun quit ()
  (throw 'interactive-quitter nil))

(defun run-server/test-environment ()
  (setf *package* (find-package *runtime-package-name*))
  (catch 'interactive-quitter
    (with-test-environment
      (format t "Welcome to the cl-naive-webserver test server.~2%")
      (format t "Interesting commands: ~{~A~^ ~}~%" '((list-server-urls) (list-server-sites) (quit)))
      (loop (format t "~^~A> " (package-name *package*))
            (finish-output)
            (handler-case
                (let ((form (read)))
                  (format t "~&--> ~{~S~^; ~%    ~}~%" (multiple-value-list (eval form))))
              (error (err)
                     (format t "~&Error: ~A~%" err)))))))

(defun run-server ()
  ;; (load #P"~/works/mts/Harag/cl-naive-websockets/src/debugging.lisp")
  ;; (setf *DEBUGGING* t  *DEBUG* t)
  (setf *package* (find-package *runtime-package-name*))
  (catch 'interactive-quitter
    (format t "Welcome to the cl-naive-webserver test server.~2%")
    (format t "Interesting commands: ~{~A~^ ~}~%" '((list-server-urls) (list-server-sites) (quit)))
    (loop (format t "~^~A> " (package-name *package*))
          (finish-output)
          (handler-case
              (let ((form (read)))
                (format t "~&--> ~{~S~^; ~%    ~}~%" (multiple-value-list (eval form))))
            (error (err)
                   (format t "~&Error: ~A~%" err))))))

