(in-package :cl-naive-websockets)

;; TODO: add print-object methods

(defclass websockets-endpoint ()
  ((extensions                  :initarg       :extensions
                                :initform      '()
                                :reader        endpoint-extensions)
   (subprotocol                 :initarg       :subprotocol
                                :initform      nil
                                :reader        endpoint-subprotocol)
   (cookies                     :initarg       :cookies
                                :initform      '()
                                :reader        endpoint-cookies)
   (lower-layer                 :initarg      :lower-layer
                                :initform      nil
                                :reader        lower-layer)
   (local-address               :initarg       :local-address
                                :initform      nil
                                :reader        local-address
                                :reader        endpoint-local-address)
   (local-port                  :initarg       :local-port
                                :initform      nil
                                :reader        local-port
                                :reader        endpoint-local-port)
   (remote-address              :initarg       :remote-address
                                :initform      nil
                                :reader        remote-address
                                :reader        endpoint-remote-address)
   (remote-port                 :initarg       :remote-port
                                :initform      nil
                                :reader        remote-port
                                :reader        endpoint-remote-port)
   (closedp                     :initarg       :closedp
                                :initform      nil
                                :reader        closedp
                                :reader        endpoint-closedp)
   (upper-layer                 :initarg       :upper-layer
                                :initform      nil
                                :accessor      endpoint-upper-layer)
   ;; ==== private stuff ====
   (%state                      :initform      0
                                :accessor      endpoint-state)
   ;; ---- sending ----
   (%control-message-queue-lock :initform      (bt:make-lock "websockets-endpoint %control-message-queue-lock")
                                :reader        endpoint-control-message-queue-lock
                                :documentation "mutex control-message-queue")
   (%control-message-queue      :initform      (make-queue)
                                :reader        endpoint-control-message-queue)
   (%send-message-lock          :initform      (bt:make-lock "websockets-endpoint %send-message-lock")
                                :reader        endpoint-send-message-lock
                                :documentation "mutex message sending")
   (%write-frame-lock           :initform      (bt:make-lock "websockets-endpoint %write-frame-lock")
                                :reader        endpoint-write-frame-lock
                                :documentation "mutex write-frame")
   (%send-current-message-id    :initform      0
                                :reader        endpoint-send-current-message-id)
   ;; ---- receiving ----
   (%receive-message-lock       :initform      (bt:make-lock "websockets-endpoint %receive-message-lock")
                                :reader        endpoint-receive-message-lock)
   (%can-receive                :initform      (bt:make-condition-variable :name "websockets-endpoint %can-receive")
                                :reader        endpoint-can-receive)
   (%receive-fragment-queue     :initform      (make-queue)
                                :reader        endpoint-receive-fragment-queue)
   (%receive-current-message-id :initform      0
                                :reader        endpoint-receive-current-message-id)))


(defgeneric must-mask-when-sending-p (endpoint))

(defclass websockets-server-endpoint (websockets-endpoint)
  ((must-mask-when-sending-p :initform nil
                             :allocation :class
                             :reader must-mask-when-sending-p)))

(defclass websockets-client-endpoint (websockets-endpoint)
  ;; All frames sent from client to server have this bit set to 1.
  ((must-mask-when-sending-p :initform t
                             :allocation :class
                             :reader must-mask-when-sending-p)))



(defgeneric message-elapsed-time (websockets-pong-message)) ; -> double-float
(defgeneric message-string (websockets-text-message)) ; -> string
(defgeneric message-data (websockets-data-message)) ; -> (vector octet)

(defgeneric close-message-status (websockets-close-message)) ; -> integer
(defgeneric close-message-reason (websockets-close-message)) ; -> string

(defclass websockets-message ()
  ())

(defclass websockets-message-fragment (websockets-message)
  ((id       :initarg :id
             :reader  message-fragment-id)
   (opcode   :initarg :opcode
             :reader  message-fragment-opcode)
   (position :initarg :position
             :reader  message-fragment-position
             :type    (member :initial :continuation :final))
   (data     :initarg :data
             :reader  message-fragment-data)))

(defclass websockets-control-message (websockets-message)
  ())

(defclass websockets-close-message (websockets-control-message)
  ((data         :initarg :data         :initform nil :reader message-data)))

(defclass websockets-pong-message (websockets-control-message)
  ((data         :initarg :data         :initform nil :reader message-data)
   (elapsed-time :initarg :elapsed-time :initform nil :reader message-elapsed-time)))

;; TODO: make an option to receive the ping messages. Normally they're processed by the endpoint.
(defclass websockets-ping-message (websockets-control-message)
  ((data         :initarg :data         :initform nil :reader message-data)))

(defclass websockets-data-message (websockets-message)
  ((data         :initarg :data         :initform nil :reader message-data)))

(defclass websockets-binary-message (websockets-data-message)
  ())

(defclass websockets-text-message (websockets-data-message)
  ((string :initarg :string :initform nil :reader message-string)))




(defgeneric receive-message       (endpoint &optional eof-error-p eof-value &key accept-fragments)
  (:documentation "
When ACCEPT-FRAGMENTS is true, it may return either a
WEBSOCKETS-CONTROL-MESSAGE, or a FRAME fragment,

Otherwise a WEBSOCKETS-MESSAGE is returned, unless the connection is
closed, in which case, either an error is signaled (if EOF-ERROR-P is
true), or the EOF-VALUE is returned.
"))


(defgeneric send-text-message     (endpoint text   &key may-fragment))
(defgeneric send-binary-message   (endpoint binary &key may-fragment))

(defgeneric send-text-fragment    (endpoint text   &key final fragment))
(defgeneric send-binary-fragment  (endpoint binary &key final fragment))

(defgeneric send-ping-control     (endpoint data))
(defgeneric send-pong-control     (endpoint data))

(defgeneric websockets-disconnect (endpoint &key status reason)
  (:documentation "The default STATUS is 1000, REASON \"Normal disconnection\"."))




(defgeneric get-local-address (stream)  ; -> (or null string (vector (unsigned-byte 8)))
  (:method ((self t)) nil))
(defgeneric get-local-port    (stream)  ; -> (or null (unsigned-byte 16))
  (:method ((self t)) nil))
(defgeneric get-peer-address  (stream)  ; -> (or null string (vector (unsigned-byte 8)))
  (:method ((self t)) nil))
(defgeneric get-peer-port     (stream)  ; -> (or null (unsigned-byte 16))
  (:method ((self t)) nil))

(defgeneric get-local-name    (stream)  ; -> (list (or null string (vector (unsigned-byte 8))) (or null (unsigned-byte 16)))
  (:method ((self t)) nil))
(defgeneric get-peer-name     (stream)  ; -> (list (or null string (vector (unsigned-byte 8))) (or null (unsigned-byte 16)))
  (:method ((self t)) nil))




(defmacro not-implemented-yet (what &rest with)
  `(error "~S is not implemented yet ~S" ',what (list ,@with)))

;;;; THE END ;;;;
