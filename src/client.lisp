(in-package :cl-naive-websockets)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; WebSocket -- Client Side
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|

We'll have a http client and connection.

The parameters to connect are:

Request hooks:
- set the /host/
- set the /port/
- set the /resource name/ (endpoint)
- set the /secure flag/ (tls?)
- set the list of /subprotocols/
- set the list of /extensions/
- set the /origin/ (the domain where the script comes from, when the client is a web browser)

|#

#-(and)
(defun http-client-connect (host port secure resource-name headers)
  ;; Open the http connection.
  ;; If secure, use ssl/tls.
  ;; Send the GET request HTTP/1.1 or HTTPS/1.1
  ;; Send the headers, with keep-alive and other options.
  ;; Get the server headers
  ;; Return them with the connection stream.
  ;; If an error occurn, close the connection and return the
  ;; server-header with the status header.
#|
   1.   The handshake MUST be a valid HTTP request as specified by
        [RFC2616].

   2.   The method of the request MUST be GET, and the HTTP version MUST
        be at least 1.1.

        For example, if the WebSocket URI is "ws://example.com/chat",
        the first line sent should be "GET /chat HTTP/1.1".

   3.   The "Request-URI" part of the request MUST match the /resource
        name/ defined in Section 3 (a relative URI) or be an absolute
        http/https URI that, when parsed, has a /resource name/, /host/,
        and /port/ that match the corresponding ws/wss URI.

  |#
  (values (acons :status-code status-code server-headers)
          stream))

(defvar *http-default-ports* '(80)) ; TODO: check the RFC about that?

(defun sha1 (bytes)
  (ironclad:digest-sequence :sha1 bytes))

(defun ascii-string-to-bytes (string)
  (ironclad:ascii-string-to-byte-array string))

(defun base64-encode-bytes (bytes)
  (with-output-to-string (output)
    (cl-base64:usb8-array-to-base64-stream bytes output)))

(defun compute-websocket-nonce ()
  (map-into (make-array 16 :element-type '(unsigned-byte 8) :initial-element 0)
            (lambda () (random 256))))

(defun format-extensions (extensions)
  ;; extensions ::= () | ( extension-token-and-param … ) .
  ;; extension-token-and-param ::= extension-token | ( extension-token [ parameter … ] ) .
  ;; extension-token ::= token .
  ;; token ::= symbol | string . -- symbol is downcased if all-uppercase
  ;; parameter ::= token | ( token [ value ] ) .
  ;; value ::= token .
  (declare (ignore extensions))
  (error "~S not implemented yet." 'format-extensions)
  nil)

(defun compose-headers (&rest header-elements)
  (let ((headers '()))
    (labels ((add-header  (header value) (setf headers (acons header value headers)))
             (add-headers (header-alist) (setf headers (append header-alist headers))))
      (loop
        :while header-elements
        :do (if (endp (rest header-elements))
                ;; additionnal header-elements list
                (if (listp (first header-elements))
                    (add-headers (apply (function compose-headers) (pop header-elements)))
                    (error "Additionnal headers must be a list, not ~S" (first header-elements)))
                (let ((header (pop header-elements))
                      (value  (pop header-elements)))
                  (unless (null value)
                    (add-header header value))))))
    headers))


(defun websockets-connect (host resource-name
                           &key (port 80) (secure nil)
                             (subprotocols '())
                             (extensions '())
                             (origin nil) ; must be a "scheme://host[:port]" string.
                             (additionnal-http-headers '()) ; passed to the http-client-connect-function for the server.
                             http-client-connect-function)
  (check-type host string)
  (check-type resource-name string)
  (check-type port (unsigned-byte 16))
  (check-type subprotocols list)
  (check-type extensions list)
  (check-type origin (or null string))
  (check-type additionnal-http-headers list)
  (check-type http-client-connect-function function)
  (let* ((nonce (compute-websocket-nonce))
         (key   (base64-encode-bytes nonce)))
    (multiple-value-bind (headers stream socket-info)
        (funcall http-client-connect-function host port secure resource-name
                 (compose-headers
                  ;; 4.   The request MUST contain a |Host| header field whose value
                  ;;      contains /host/ plus optionally ":" followed by /port/ (when not
                  ;;      using the default port).
                  "Host" (format nil "~A~:[:~A~;~]" host (member port *http-default-ports*) port)

                  ;; 5.   The request MUST contain an |Upgrade| header field whose value
                  ;;      MUST include the "websocket" keyword.
                  "Upgrade" "websocket"

                  ;; 6.   The request MUST contain a |Connection| header field whose value
                  ;;      MUST include the "Upgrade" token.
                  "Connection" "Upgrade"

                  ;; 7.   The request MUST include a header field with the name
                  ;;      |Sec-WebSocket-Key|.  The value of this header field MUST be a
                  ;;      nonce consisting of a randomly selected 16-byte value that has
                  ;;      been base64-encoded (see Section 4 of [RFC4648]).  The nonce
                  ;;      MUST be selected randomly for each connection.
                  "Sec-WebSocket-Key" key

                  ;; 8.   The request MUST include a header field with the name |Origin|
                  ;;      [RFC6454] if the request is coming from a browser client.  If
                  ;;      the connection is from a non-browser client, the request MAY
                  ;;      include this header field if the semantics of that client match
                  ;;      the use-case described here for browser clients.  The value of
                  ;;      this header field is the ASCII serialization of origin of the
                  ;;      context in which the code establishing the connection is
                  ;;      running.  See [RFC6454] for the details of how this header field
                  ;;      value is constructed.
                  "Origin" (and origin (string-downcase origin))

                  ;; 9.   The request MUST include a header field with the name
                  ;;      |Sec-WebSocket-Version|.  The value of this header field MUST be
                  ;;      13.
                  "Sec-WebSocket-Version" 13

                  ;; 10.  The request MAY include a header field with the name
                  ;;      |Sec-WebSocket-Protocol|.  If present, this value indicates one
                  ;;      or more comma-separated subprotocol the client wishes to speak,
                  ;;      ordered by preference.  The elements that comprise this value
                  ;;      MUST be non-empty strings with characters in the range U+0021 to
                  ;;      U+007E not including separator characters as defined in
                  ;;      [RFC2616] and MUST all be unique strings.  The ABNF for the
                  ;;      value of this header field is 1#token, where the definitions of
                  ;;      constructs and rules are as given in [RFC2616].
                  "Sec-WebSocket-Protocol" (and subprotocols
                                                (format nil "~{~A~^,~}"
                                                        (remove-duplicates subprotocols
                                                                           :test (function string=))))
                  ;; TODO: check that subprotocols are case-sensitive

                  ;; 11.  The request MAY include a header field with the name
                  ;;      |Sec-WebSocket-Extensions|.  If present, this value indicates
                  ;;      the protocol-level extension(s) the client wishes to speak.  The
                  ;;      interpretation and format of this header field is described in
                  ;;      Section 9.1.
                  "Sec-WebSocket-Extensions" (and extensions (format-extensions extensions))

                  ;; 12.  The request MAY include any other header fields, for example,
                  ;;      cookies [RFC6265] and/or authentication-related header fields
                  ;;      such as the |Authorization| header field [RFC2616], which are
                  ;;      processed according to documents that define them.
                  additionnal-http-headers))

      ;;    Once the client's opening handshake has been sent, the client MUST
      ;;    wait for a response from the server before sending any further data.
      ;;    The client MUST validate the server's response as follows:

      (flet ((fail-connection (&optional reason)
               (unwind-protect
                    (when reason (cerror "Cleanup and continue"
                                         "websocket connection failed: ~A" reason))
                 (when stream (close stream)))
               (return-from websockets-connect nil)))

        ;;    1.  If the status code received from the server is not 101, the
        ;;        client handles the response per HTTP [RFC2616] procedures.  In
        ;;        particular, the client might perform authentication if it
        ;;        receives a 401 status code; the server might redirect the client
        ;;        using a 3xx status code (but clients are not required to follow
        ;;        them), etc.  Otherwise, proceed as follows.
        (let ((status-code (cdr (assoc :status-code headers))))
          (unless (= status-code 101)
            (fail-connection (format nil "status code ~A; expected 101" status-code))))

        (unless stream
          (fail-connection))

        ;;    2.  If the response lacks an |Upgrade| header field or the |Upgrade|
        ;;        header field contains a value that is not an ASCII case-
        ;;        insensitive match for the value "websocket", the client MUST
        ;;        _Fail the WebSocket Connection_.
        (let ((upgrade (cdr (assoc :upgrade headers))))
          (unless (string-equal "websocket" upgrade)
            (fail-connection "No header upgrade to websocket.")))

        ;;    3.  If the response lacks a |Connection| header field or the
        ;;        |Connection| header field doesn't contain a token that is an
        ;;        ASCII case-insensitive match for the value "Upgrade", the client
        ;;        MUST _Fail the WebSocket Connection_.
        (let ((connection (cdr (assoc :connection headers))))
          (unless (string-equal "upgrade" connection)
            (fail-connection "No header connection to upgrade.")))

        ;;    4.  If the response lacks a |Sec-WebSocket-Accept| header field or
        ;;        the |Sec-WebSocket-Accept| contains a value other than the
        ;;        base64-encoded SHA-1 of the concatenation of the |Sec-WebSocket-
        ;;        Key| (as a string, not base64-decoded) with the string "258EAFA5-
        ;;        E914-47DA-95CA-C5AB0DC85B11" but ignoring any leading and
        ;;        trailing whitespace, the client MUST _Fail the WebSocket
        ;;        Connection_.
        (let ((accept     (cdr (assoc :Sec-WebSocket-Accept headers)))
              (validation (sha1 (ascii-string-to-bytes (concatenate 'string key "258EAFA5-E914-47DA-95CA-C5AB0DC85B11")))))
          (unless (string-equal (base64-encode-bytes validation) accept)
            (terpri *error-output*)
            (format *error-output* "nonce                = ~S~%" nonce)
            (format *error-output* "Sec-WebSocket-Key    = ~S~%" (base64-encode-bytes nonce))
            (format *error-output* "validation           = ~S~%" validation)
            (format *error-output* "base-64 validation   = ~S~%" (base64-encode-bytes validation))
            (format *error-output* "Sec-WebSocket-Accept = ~S~%" accept)
            (fail-connection "Invalid header Sec-WebSocket-Accept.")))

        (let ((extensions nil)
              (subprotocol nil)
              (cookies nil))
          ;;    5.  If the response includes a |Sec-WebSocket-Extensions| header
          ;;        field and this header field indicates the use of an extension
          ;;        that was not present in the client's handshake (the server has
          ;;        indicated an extension not requested by the client), the client
          ;;        MUST _Fail the WebSocket Connection_.  (The parsing of this
          ;;        header field to determine which extensions are requested is
          ;;        discussed in Section 9.1.)
          (setf extensions (cdr (assoc :Sec-WebSocket-Extensions headers)))
          ;; TODO: check extensions.
          ;; (unless (string-equal "upgrade" connection)
          ;;   (fail-connection "No header connection to upgrade."))


          ;;    6.  If the response includes a |Sec-WebSocket-Protocol| header field
          ;;        and this header field indicates the use of a subprotocol that was
          ;;        not present in the client's handshake (the server has indicated a
          ;;        subprotocol not requested by the client), the client MUST _Fail
          ;;        the WebSocket Connection_.
          (setf subprotocol (cdr (assoc :Sec-WebSocket-Protocol headers)))
          ;; TODO: check that subprotocols are case-sensitive
          (unless (find subprotocol subprotocols :test (function string=))
            (fail-connection "Sec-WebSocket-Protocol negociation failure."))

          ;; The WebSocket Connection is Established
          ;; The WebSocket Connection is in the OPEN state.
          ;; extensions
          ;; subprotocol
          ;; Cookies Set During the Server's Opening Handshake

          (make-instance 'websockets-client-endpoint
                         :extensions      extensions
                         :subprotocol     subprotocol
                         :cookies         cookies
                         :local-address   (first  (first  socket-info))
                         :local-port      (second (first  socket-info))
                         :remote-address  (first  (second socket-info))
                         :remote-port     (second (second socket-info))
                         :lower-layer     stream))))))


