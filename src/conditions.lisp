(in-package :cl-naive-websockets)

(defvar *debugging* nil)

(defmacro with-debugging (&body body)
  (let ((fody (gensym)))
    `(flet ((,fody () ,@body))
       (if *debugging*
           (handler-bind ((error (function invoke-debugger)))
             (,fody))
           (,fody)))))



(defconstant +ws-normal-closure+             1000 "Normal Closure")
(defconstant +ws-going-away+                 1001 "Going Away")
(defconstant +ws-protocol-error+             1002 "Protocol Error")
(defconstant +ws-unsupported-data+           1003 "Unsupported Data")
(defconstant +ws-reserved-status+            1004 "Reserved")
(defconstant +ws-no-status-received+         1005 "No Status Received")
(defconstant +ws-abnormal-closure+           1006 "Abnormal Closure")
(defconstant +ws-invalid-frame-payload-data+ 1007 "Invalid Frame Payload Data")
(defconstant +ws-policy-violation+           1008 "Policy Violation")
(defconstant +ws-message-too-big+            1009 "Message Too Big")
(defconstant +ws-mandatory-extension+        1010 "Mandatory Extension")
(defconstant +ws-internal-server-error+      1011 "Internal Server Error")
(defconstant +ws-tls-handshare-error+        1015 "TLS Handshake Error")



(define-condition websockets-condition (simple-condition)
  ((endpoint         :initarg :endpoint         :reader websockets-condition-endpoint)))


(define-condition websockets-error (websockets-condition simple-error)
  ((error-status :initarg :status :reader websockets-error-status)))

(defgeneric websockets-error (endpoint status format-control &rest format-arguments))

(defmethod websockets-error (endpoint status format-control &rest format-arguments)
  "Signals an error of type WEBSOCKETS-ERROR with the provided STATUS,
FORMAT-CONTROL and FORMAT-ARGUMENTS."
  ;; Note: we don't check the status here, because the condition
  ;; doesn't necessarily translate to a reason for a close packet.
  ;; It can be handled internally or by the client code..
  (error 'websockets-error
         :endpoint endpoint
         :status status
         :format-control format-control
         :format-arguments format-arguments))


(define-condition websockets-wrong-message-fragment-error (websockets-error)
  ((expected-fragment-id :initarg :expected-fragment-id
                         :reader websockets-wrong-message-fragment-error-expected-fragment-id)
   (actual-fragment-id   :initarg :actual-fragment-id
                         :reader websockets-wrong-message-fragment-error-actual-fragment-id)))

(defgeneric websockets-wrong-message-fragment-error (endpoint &key expected-fragment-id actual-fragment-id))



(define-condition websockets-closed-error (end-of-file websockets-error)
  ((status  :initarg :status  :reader websockets-closed-error-status)
   (reason  :initarg :reason  :reader websockets-closed-error-reason)
   (message :initarg :message :reader websockets-closed-error-message)))

(defgeneric websockets-closed-error (endpoint &key status reason message))

(defmethod websockets-closed-error ((endpoint websockets-endpoint)
                                    &key (status 1000) (reason "Normal closure") message)
  "Signals an error of type WEBSOCKETS-CLOSED-ERROR."
  (error 'websockets-closed-error
         :endpoint endpoint
         :status status
         :reason reason
         :message message
         :format-control "The connection is closed, no more data is available, or can be send.  status = ~S ; reason = ~S"
         :format-arguments (list status reason)))


;;;; THE END ;;;;
