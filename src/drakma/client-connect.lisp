(defpackage :cl-naive-websockets.client.drakma
  (:use #:cl #:cl-naive-websockets)
  (:export #:drakma-http-client-connect))
(in-package :cl-naive-websockets.client.drakma)

(defun drakma-http-client-connect (host port secure resource-name headers)
  ;; SECURE can be an alist of :CERTIFICATE :KEY :CERTIFICATE-PASSWORD :VERIFY :MAX-DEPTH :CA-FILE :CA-DIRECTORY
  ;;
  ;; Open the http connection.
  ;; If secure, use ssl/tls.
  ;; Send the GET request HTTP/1.1 or HTTPS/1.1
  ;; Send the headers, with keep-alive and other options.
  ;; Get the server headers
  ;; Return them with the connection stream.
  ;; If an error occurn, close the connection and return the
  ;; server-header with the status header.

  ;; 1.   The handshake MUST be a valid HTTP request as specified by
  ;;      [RFC2616].
  ;;
  ;; 2.   The method of the request MUST be GET, and the HTTP version MUST
  ;;      be at least 1.1.
  ;;
  ;;      For example, if the WebSocket URI is "ws://example.com/chat",
  ;;      the first line sent should be "GET /chat HTTP/1.1".
  ;;
  ;; 3.   The "Request-URI" part of the request MUST match the /resource
  ;;      name/ defined in Section 3 (a relative URI) or be an absolute
  ;;      http/https URI that, when parsed, has a /resource name/, /host/,
  ;;      and /port/ that match the corresponding ws/wss URI.

  (multiple-value-bind (body status-code server-headers uri stream should-be-closed-p reason)
      ;; TODO: Handle error status-code and signal errors?
      (drakma:http-request (format nil "~:[http~;https~]://~A~:[:~A~;~*~]~:[/~;~]~A"
                                   secure host (member port *http-default-ports*) port
                                   (and (plusp (length resource-name))
                                       (char= #\/ (aref resource-name 0)))
                                   resource-name)
                           :protocol :http/1.1
                           :method :get
                           :force-ssl secure
                           :certificate          (cdr (assoc :certificate          secure))
                           :key                  (cdr (assoc :key                  secure))
                           :certificate-password (cdr (assoc :certificate-password secure))
                           :verify               (cdr (assoc :verify               secure))
                           :max-depth        (or (cdr (assoc :max-depth            secure)) 10)
                           :ca-file              (cdr (assoc :ca-file              secure))
                           :ca-directory         (cdr (assoc :ca-directory         secure))
                           :additional-headers headers
                           :close nil
                           :keep-alive nil ; this goes and merges with: Connection: keep-alive,upgrade ; which fails in 400.
                           :content-type "application/octet-stream"
                           :force-binary t
                           :external-format-out :octet
                           :external-format-out :octet
                           :want-stream t)
    (declare (ignore body uri should-be-closed-p reason))
    (let ((socket-info (list (get-local-name stream)
                             (get-peer-name stream))))
      (unless (= status-code 101)
        (close stream)
        (setf stream nil))
      (values (acons :status-code status-code server-headers)
              (when stream (flexi-streams:flexi-stream-stream stream))
              socket-info))))

#-(and)
(progn

  (defvar *connection* nil)
  (dolist (host '("www.informatimago.com" "proteus.sbde.fr"))
    (let ((secure nil)
          (host host)
          (port 80)
          (*http-default-ports* '())
          (resource-name "")
          (headers '()))
      (setf *connection*
            (multiple-value-list
             (drakma:http-request (format nil "~:[http~;https~]://~A~:[:~A~;~*~]/~A"
                                          secure host (member port *http-default-ports*) port resource-name)
                                  :protocol :http/1.1
                                  :method :get
                                  :force-ssl secure
                                  :certificate          (cdr (assoc :certificate          secure))
                                  :key                  (cdr (assoc :key                  secure))
                                  :certificate-password (cdr (assoc :certificate-password secure))
                                  :verify               (cdr (assoc :verify               secure))
                                  :max-depth        (or (cdr (assoc :max-depth            secure)) 10)
                                  :ca-file              (cdr (assoc :ca-file              secure))
                                  :ca-directory         (cdr (assoc :ca-directory         secure))
                                  :additional-headers headers
                                  :close nil
                                  :keep-alive nil ; this goes and merges with: Connection: keep-alive,upgrade ; which fails in 400.
                                  :force-binary t
                                  :want-stream t))))

    (dolist (name (list (cl-naive-websockets::get-local-name (fifth *connection*))
                        (cl-naive-websockets::get-peer-name  (fifth *connection*))))
      (if (< 4 (length (first name)))
          (format t "~&~(~{~2,'0X~2,'0X~^:~} ~A~)~%"
                  (coerce (first name) 'list)
                  (second name))
          (format t "~&~(~{~D~^.~} ~A~)~%"
                  (coerce (first name) 'list)
                  (second name)))))

  2a01:cb04:0b1a:e100:d9f6:10b4:9811:b2b5 57831
  2a01:04f8:0161:52e2:0000:0000:0000:0002 80
  192.168.7.10 57833
  145.239.195.204 80

  )

;;;; The End ;;;;
