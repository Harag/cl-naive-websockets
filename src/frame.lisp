(in-package :cl-naive-websockets)

;;;
;;; Binary reading/writing machinery
;;;

(defun read-unsigned-big-endian (lower-layer n)
  "Read N octets from the lower-layer and return the big-endian integer."
  (loop :with value := 0
        :for position :from (* (- n 1) 8) :downto 0 :by 8
        :do (setf value (dpb (receive-octet lower-layer) (byte 8 position) value))
        :finally (return value)))


;;;
;;; Frames
;;;



#|

** Frames

Receiving a frame involves the following steps (it's the same whether
it's done by the server endpoint or by the client endpoint):

1. read the frame header, and build a frame object.

2. when it's a control frame, we read the frame data immediately (it's
   less than 126 octets).

3. otherwise reading the frame data is defered to the processing of
   the frame: it can be avoided in case of error, but it must be read
   in all normal cases.

4. when reading the frame data, it is unmasked according to the MASK
   but in the header.  Up to the processor of the frame to detect if
   this is an error (and possibly not even read the frame data if
   there's a masking-key when it should not, or vice-versa).

The current implementation makes no provision for extensions.

*** defun read-frame (lower-layer)
*** defmethod read-frame-data (lower-layer (frame frame))

|#



;; opcodes (other values between 0 and #xF are reserved for extensions):

(defconstant +continuation-frame+    #x0)
(defconstant +text-frame+            #x1)
(defconstant +binary-frame+          #x2)
(defconstant +close-frame+           #x8)
(defconstant +ping-frame+            #x9)
(defconstant +pong-frame+            #xA)

(defclass frame ()
  ((opcode         :initarg :opcode         :initform nil :accessor frame-opcode)
   (data           :initarg :data           :initform nil :accessor frame-data)
   (finp           :initarg :finp           :initform nil :accessor frame-finp)
   (payload-length :initarg :payload-length :initform nil :accessor frame-payload-length)
   (masking-key    :initarg :masking-key    :initform nil :accessor frame-masking-key)))

(defgeneric control-frame-p (frame)
  (:method ((opcode integer))
    (plusp (logand #x8 opcode)))
  (:method ((frame frame))
    (control-frame-p (frame-opcode frame))))


(defun masking-key (maskp)
  (when maskp
    (vector (random 256) (random 256) (random 256) (random 256))))

(defun make-text-frame (text &optional maskp)
  (let ((octets (trivial-utf-8:string-to-utf-8-bytes text)))
    (make-instance 'frame
                   :opcode +text-frame+
                   :data octets
                   :finp t
                   :payload-length (length octets)
                   :masking-key (masking-key maskp))))

(defun make-binary-frame (octets &optional maskp)
  (make-instance 'frame
                 :opcode +binary-frame+
                 :data octets
                 :finp t
                 :payload-length (length octets)
                 :masking-key (masking-key maskp)))

(defun mask-unmask (data masking-key &key (start 0) end)
  ;; RFC6455 Masking
  ;;
  ;; Octet i of the transformed data
  ;; ("transformed-octet-i") is the XOR of octet i
  ;; of the original data ("original-octet-i")
  ;; with octet at index i modulo 4 of the masking
  ;; key ("masking-key-octet-j"):
  (let ((end (or end (length data))))
    (loop :for i :below (- end start)
          :do (setf (aref data (+ start i)) (logxor (aref data (+ start i))
                                                    (aref masking-key (mod i 4)))))
    data))

(defgeneric read-frame-data (lower-layer frame))

(defvar *endpoint* nil)

(defun check-length-read (expected-length read-length)
  (unless (= expected-length read-length)
    (websockets-error *endpoint* 1007 "could not read payload-length = ~A octet~:*~P"
                      expected-length)))

(defmethod read-frame-data (lower-layer (frame frame))
  (with-slots (payload-length data) frame
    (unless data ; already read.
      (let ((buffer      (make-buffer payload-length))
            (masking-key (frame-masking-key frame)))
        (check-length-read payload-length (receive-octets lower-layer buffer))
        (when masking-key
          (mask-unmask buffer masking-key))
        (setf data buffer)))))

(defun read-frame (lower-layer)
  (handler-case
      (let* ((first-octet      (receive-octet lower-layer))
             (fin-bit          (ldb (byte 1 7) first-octet))
             (reserved-bits    (ldb (byte 3 4) first-octet))
             (opcode           (ldb (byte 4 0) first-octet))
             (second-octet     (receive-octet lower-layer))
             (maskp            (plusp (ldb (byte 1 7) second-octet)))
             (payload-length   (ldb (byte 7 0) second-octet))
             (payload-length   (if (<= 0 payload-length 125)
                                   payload-length
                                   (read-unsigned-big-endian
                                    lower-layer (case payload-length
                                                  (126 2)
                                                  (127 8)))))
             (masking-key      (when maskp
                                 (let ((buffer (make-buffer 4)))
                                   (unless (= 4 (receive-octets lower-layer buffer))
                                     (websockets-error *endpoint* 1002 "Cannot receive the masking key"))
                                   buffer))))
        (when (and (control-frame-p opcode)
                   (< 125 payload-length))
          (websockets-error *endpoint* 1002 "Control frame is too large"))
        (when (plusp reserved-bits)
          (websockets-error *endpoint* 1002 "No extension negotiated, but peer sends reserved bits: #b~3B"
                            reserved-bits))
        (let ((frame (make-instance 'frame :opcode opcode
                                           :finp (plusp fin-bit)
                                           :masking-key masking-key
                                           :payload-length payload-length)))
          (when (control-frame-p opcode)
            (read-frame-data lower-layer frame))
          frame))

    (end-of-file (err)
      (websockets-error *endpoint* 1001 "Lower-layer has disconnected ~A" err))))


(defmethod write-frame (lower-layer (frame frame))
  (handler-case
      (let* ((first-byte     #x00)
             (second-byte    #x00)
             (data           (frame-data frame))
             (masking-key    (frame-masking-key frame))
             (len            (if data (length data) 0))
             (payload-length (cond ((<= len 125)          len)
                                   ((< len #.(expt 2 16)) 126)
                                   (t                     127))))
        (setf (ldb (byte 1 7) first-byte)  (if (frame-finp frame) 1 0)
              (ldb (byte 3 4) first-byte)  0 ; extensions
              (ldb (byte 4 0) first-byte)  (frame-opcode frame)
              (ldb (byte 1 7) second-byte) (if masking-key 1 0)
              (ldb (byte 7 0) second-byte) payload-length)
        (send-octet lower-layer first-byte)
        (send-octet lower-layer second-byte)
        (loop :for i :from (1- (cond ((<= len 125)          0)
                                     ((< len #.(expt 2 16)) 2)
                                     (t                     8)))
              :downto 0
              :for octet := (ldb (byte 8 (* 8 i)) len)
              :do (send-octet lower-layer octet))
        (when masking-key
          (send-octets lower-layer masking-key)
          (mask-unmask data masking-key))
        (when data
          (send-octets lower-layer data))
        (flush lower-layer))

    (end-of-file (err)
      ;; TODO: what error is signaled when we try to write a socket that is closed?
      (websockets-error *endpoint* 1001 "Lower-layer has disconnected ~A" err))))

;;;; THE END ;;;;
