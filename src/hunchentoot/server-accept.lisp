(in-package :cl-naive-websockets)

;;
;; Hook onto normal Hunchentoot processing
;;
;; The `:after' specialization of `process-request' will happen after
;; the main Hunchentoot one.  It is hunchentoot which eventually calls
;; our specialization of `acceptor-dispatch-request', who will, in
;; turn, try to figure out if the client is requesting
;; websockets.  Hunchentoot's `process-request' will also eventually
;; reply to the client.  In the `:after' specialization we might enter
;; into `read-handle-loop' and thus keep the socket alive. That happens
;; if:
;;
;; 1. There are suitable "Connection" and "Upgrade" headers and
;;    `websockets-resource' object is found for request.
;;
;; 2. The websocket handshake completes sucessfully, whereby the
;;    callees of `acceptor-dispatch-request' will have set
;;    `+http-switching-protocols+' accordingly.
;;
;; If any of these steps fail, errors might be signalled, but normal
;; hunchentoot processing of the HTTP request still happens.
;;

;;;
;;; Hunchentoot Request Overrides
;;;

(defclass websockets-request (hunchentoot:request)
  ((websockets-resource         :accessor websockets-resource
                                :initform nil
                                :documentation "Websocket resource of the current request")
   (websockets-endpoint         :initarg  :websockets-endpoint
                                :reader   websockets-endpoint)))

(defmethod print-object ((object websockets-request) stream)
  (handler-bind ((error #'invoke-debugger))
    (print-unreadable-object (object stream :identity t :type t)
      (format stream "~@{~S~^ ~}"
              :acceptor        (when (slot-boundp object 'hunchentoot::acceptor)         (hunchentoot::request-acceptor object))
              :headers-in      (when (slot-boundp object 'hunchentoot::headers-in)       (hunchentoot::headers-in       object))
              :method          (when (slot-boundp object 'hunchentoot::method)           (hunchentoot::request-method   object))
              :uri             (when (slot-boundp object 'hunchentoot::uri)              (hunchentoot::request-uri      object))
              :server-protocol (when (slot-boundp object 'hunchentoot::server-protocol)  (hunchentoot::server-protocol  object))
              :local-addr      (when (slot-boundp object 'hunchentoot::local-addr)       (hunchentoot::local-addr       object))
              :local-port      (when (slot-boundp object 'hunchentoot::local-port)       (hunchentoot::local-port       object))
              :remote-addr     (when (slot-boundp object 'hunchentoot::remote-addr)      (hunchentoot::remote-addr      object))
              :remote-port     (when (slot-boundp object 'hunchentoot::remote-port)      (hunchentoot::remote-port      object))
              :content-stream  (when (slot-boundp object 'hunchentoot::content-stream)   (hunchentoot::content-stream   object))
              :cookies-in      (when (slot-boundp object 'hunchentoot::cookies-in)       (hunchentoot::cookies-in       object))
              :get-parameters  (when (slot-boundp object 'hunchentoot::get-parameters)   (hunchentoot::get-parameters   object))
              :post-parameters (when (slot-boundp object 'hunchentoot::post-parameters)  (hunchentoot::post-parameters  object))
              :script-name     (when (slot-boundp object 'hunchentoot::script-name)      (hunchentoot::script-name      object))
              :query-string    (when (slot-boundp object 'hunchentoot::query-string)     (hunchentoot::query-string     object))
              :session         (when (slot-boundp object 'hunchentoot::session)          (hunchentoot::session          object))
              :aux-data        (when (slot-boundp object 'hunchentoot::aux-data)         (hunchentoot::aux-data         object))
              :raw-post-data   (when (slot-boundp object 'hunchentoot::raw-post-data)    (slot-value object 'hunchentoot::raw-post-data))
              :resource        (when (slot-boundp object 'websockets-resource) (websockets-resource object))
              :endpoint        (when (slot-boundp object 'websockets-endpoint) (websockets-endpoint object)))))
  object)

(defmethod get-local-address        ((request websockets-request))  (hunchentoot:local-addr      request))
(defmethod get-local-port           ((request websockets-request))  (hunchentoot:local-port      request))
(defmethod get-peer-address         ((request websockets-request))  (hunchentoot:remote-addr     request))
(defmethod get-peer-port            ((request websockets-request))  (hunchentoot:remote-port     request))

(defmethod get-local-name           ((request websockets-request))  (list (get-local-address request) (get-local-port request)))
(defmethod get-peer-name            ((request websockets-request))  (list (get-peer-address request)  (get-peer-port request)))

(defmacro once (&body body)
  (let ((vdone (gensym)))
    `(let ((,vdone (load-time-value nil)))
       (unless ,vdone
         (setf ,vdone t)
         ,@body))))

(defmethod get-request-host         ((request websockets-request))
  (once (let ((*print-right-margin* nil))
          (warn "~S is not implemented yet" '(get-request-host websockets-request))))
  (get-local-address request))

(defmethod get-resource-name        ((request websockets-request))  (hunchentoot:script-name  request))
(defmethod get-cookies              ((request websockets-request))  (hunchentoot:cookies-in   request))
(defmethod get-headers              ((request websockets-request))  (hunchentoot:headers-in   request))

(defmethod connection-secure-p      ((request websockets-request))
  (once (let ((*print-right-margin* nil))
          (warn "~S is not implemented yet" '(connection-secure-p websockets-request))))
  nil)

(defmethod get-ssl-peer-certificate ((request websockets-request))
  (once (let ((*print-right-margin* nil))
          (warn "~S is not implemented yet" '(get-ssl-peer-certificate websockets-request))))
  nil)

(defmethod get-ssl-verify-result    ((request websockets-request))
  (once (let ((*print-right-margin* nil))
          (warn "~S is not implemented yet" '(get-ssl-verify-result websockets-request))))
  nil)


(defvar *websockets-socket*)

(defmethod hunchentoot:process-request ((request websockets-request))
  "After HTTP processing REQUEST, maybe hijack into WebSocket loop."
  (when (next-method-p)
    (call-next-method))
  (when (= hunchentoot:+http-switching-protocols+ (hunchentoot:return-code*))
    (let ((acceptor (hunchentoot:request-acceptor request))
          (stream   (hunchentoot::content-stream request))
          (endpoint (websockets-endpoint request)))
      (force-output stream)

      (let ((timeout  (websockets-timeout acceptor)))
        ;; See https://github.com/joaotavora/hunchensocket/pull/23
        ;; LispWorks in Hunchentoot passes around a USOCKET:USOCKET
        ;; handle, not an actual such object. Unfortunately, abusing
        ;; Hunchentoot's internals consequently forces us to tend to
        ;; this LispWorks particularity.
        #-lispworks (hunchentoot::set-timeouts *websockets-socket* timeout timeout)
        #+lispworks (setf (stream:stream-read-timeout stream) timeout
                          (stream:stream-write-timeout stream) timeout))

      (let ((err (catch 'websockets-done
                   (handler-bind ((error (lambda (err)
                                           (hunchentoot:maybe-invoke-debugger err)
                                           (hunchentoot:log-message* :error "Error: ~a" err)
                                           (throw 'websockets-done err))))
                     (websockets-server-serve (websockets-server acceptor)
                                              stream endpoint)
                     nil))))
        (unless (endpoint-closedp endpoint)
          (websockets-disconnect endpoint
                                 :status (if err 1011 1000)
                                 :reason (if err
                                             (princ-to-string err)
                                             "Good bye!")))))))

;;;
;;; Hunchentoot Acceptor Hooks
;;;

(defclass websockets-acceptor-mixin ()
  ((websockets-server           :initarg  :websockets-server
                                :reader   websockets-server)
   (websockets-timeout          :initarg  :websockets-timeout
                                :reader   websockets-timeout
                                :initform 300
                                :documentation "Custom Websockets timeout override.")
   (websockets-back-end-request :initarg :back-end-request
                                :initform nil
                                :reader back-end-request))
  (:default-initargs            :request-class 'websockets-request)
  (:documentation "
As a mixin it should be inherited by user acceptor classes, /before/
HUNCHENTOOT:ACCEPTOR, so that its HUNCHENTOOT:ACCEPTOR-DISPATCH-REQUEST
method get a chance to run before hunchentoot (which doesn't have a
CALL-NEXT-METHOD hook).
Obviously, the user accept class can also define its own method that
will be called first, to perform additionnal dispatching.
"))

(defgeneric accept-websockets-negociation (acceptor raw-request)
  (:documentation "Return a generalized boolean indicating the request was for a websockets
and negociation has been performed."))

(defmethod accept-websockets-negociation ((acceptor websockets-acceptor-mixin) request)
  (when (and (string-equal "websocket" (hunchentoot:header-in* :upgrade))
             (member "upgrade" (cl-ppcre:split "\\s*,\\s*" (hunchentoot:header-in* :connection))
                     :test #'string-equal))

    ;; yes, negociate
    (multiple-value-bind (headers-out endpoint)
        (websockets-server-negociate (websockets-server acceptor)
                                     (hunchentoot:headers-in request)
                                     request)
      (setf (slot-value request 'websockets-endpoint) endpoint)
      (when (typep request 'websockets-acceptor-mixin)
        (setf (slot-value request 'websockets-endpoint) endpoint))
      (let ((status-code hunchentoot::+http-switching-protocols+)
            (content-type "application/octet-stream"))
        (loop :for (header . value) :in  headers-out
              :do (cond
                    ((eq header :status-code)  (setf status-code  value))
                    ((eq header :Content-Type) (setf content-type value))
                    (t (setf (hunchentoot:header-out header) value))))
        (setf (hunchentoot:return-code*)  status-code
              (hunchentoot:content-type*) content-type
              (hunchentoot:reply-external-format hunchentoot:*reply*)
              (flexi-streams:make-external-format :utf8 :eol-style :lf))))
    t))

(defmethod hunchentoot:process-connection :around ((hunchentoot:*acceptor* websockets-acceptor-mixin) (socket t))
  "Sprinkle the current connection with dynamic bindings."
  (let ((*websockets-socket* socket))
    (with-debugging
        (call-next-method))))


(defmethod hunchentoot:acceptor-dispatch-request ((acceptor websockets-acceptor-mixin) raw-request)
  "
Hunchentoot only calls this if there was no error up to that point and
the return code of the reply is eql to +HTTP-OK+ which means we don't
have to worry about error codes etc.  If our code signals an error
hunchentoot will catch it and do the right thing aka
report-error-to-client.

We highjack the hunchentoot process at this point to bypass
hunchentoot's dispatchers completely except for static files. Your
static file extensions should be listed in *handling-exclusions*. We
allow hunchentoot to handle any requests that match
*handling-exclusions* first just incase you want hunchentoot to serve
static files.

When websockets are enabled it will get first-crack at handling the request.

Note: subclasses may override this method calling
ACCEPTED-WEBSOCKET-NEGOCIATION only when selected (eg. from the
resource-name).
"
  (if (accept-websockets-negociation acceptor raw-request)
      ;; It was a websocket request:
      (values "" nil nil)
      ;; Otherwise, defer to the superclass.
      (when (next-method-p)
        (call-next-method))))

;;;; THE END ;;;;
