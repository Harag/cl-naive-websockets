// -*- mode:javascript -*-

const WebSocket = require('modern-isomorphic-ws');
const wrapper = require('./wrapper.js')
const serverUrl = 'ws://localhost:33003/example/ws'

const ws = new WebSocket(serverUrl);

const text = ['Hello!',
              'How do you do?',
              'This is a little client tests.',
              'That will be all for today.',
              'Good bye.'];

i=0;

ws.on('error', console.error);

ws.on('open', function open() {
    ws.send(text[i]);
    i++;
});

ws.on('close', function close() {
    console.log('disconnected');
    process.exit();
});

ws.on('message', function message(data) {
    console.log('received: %s', data);
    if(i<text.length){
        ws.send(text[i]);
        i++;
    }else{
        ws.close(1000,'Done');
    }
});
