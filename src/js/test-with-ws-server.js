const WebSockets = require('ws');
const SExpr = require("s-expression.js")
const S = new SExpr();
const test = require("./test.js")
const wrapper = require('./wrapper.js')

test.set_verbose(false);

///
/// test suites:
///

// client sends:     T (:payload "SEND text \"payload\"" :request "text" :source "parameter")
// server responds:  T (:payload "SEND text \"payload\"" :source "parameter")
// 
// client sends:     B (:binary  4 :request "text" :source "parameter")ABCD
// server responds:  T (:payload "ABCD" :source "parameter")
// 
// client sends:     T (:payload "SEND text \"payload\""  :request "binary" :source "parameter")
// server responds:  B (:binary 19 :source "parameter")SEND text "payload"
// 
// client sends:     B (:binary 4 :request "binary" :source "parameter")ABCD
// server responds:  B (:binary 4 :source "parameter")ABCD
// 
// client sends:     T (:quit "Bye")
// server disconnets.

function gotExpectedParameters(expectedParameters,actualParameters){
    for(i = 0 ; i<wrapper.list_length(expectedParameters) ; i+=2){
        var key = expectedParameters[i];
        var expectedValue = expectedParameters[i+1];
        var actualValue = wrapper.getf(actualParameters, key, undefined);
        if(!wrapper.isEqualP(expectedValue, actualValue)){
            return false;
        }
    }
    return true;
}    


var expect = {
    kind: '',
    parameters: S.null(),
    payload: S.null(),
};

function send_text_text(ws){
    test.log('send_text_text');
    // TODO: allow quotes and backquotes in strings!!!
    // var payload = wrapper.string("Hello, this is a \"nice\" \\text\\ payload for a text response!");
    var payload = wrapper.string("Hello, this is a nice text payload for a text response!");
    var parameters = S.expression(S.identifier(':request'),wrapper.string('text'),
                                  S.identifier(':test'),wrapper.string("send text, receive text"));
    expect.kind=wrapper.string('text');
    expect.parameters=S.expression(S.identifier(':status'),wrapper.string('ok')).concat(parameters);
    expect.payload=payload;
    test.log('parameters=%s',parameters);
    test.log('payload=%s',payload);
    var message = wrapper.wrap_parameters_text_payload(parameters,payload,S.identifier(':payload'));
    ws.send(message);
    test.log('sent message=%s',message);
}

function send_text_binary(ws){
    test.log('send_text_binary');
    // TODO: allow quotes and backquotes in strings!!!
    // var payload = wrapper.string("Hello, this is a \"nice\" \\text\\ payload for a binary response!");
    var payload = wrapper.string("Hello, this is a nice text payload for a binary response!");
    var parameters = S.expression(S.identifier(':request'),wrapper.string('binary'),
                                  S.identifier(':test'),wrapper.string("send text, receive binary"));
    expect.kind=wrapper.string('binary');
    expect.parameters=S.expression(S.identifier(':status'),wrapper.string('ok')).concat(parameters);
    expect.payload=(new TextEncoder('utf-8')).encode(S.valueOf(payload));
    var message = wrapper.wrap_parameters_text_payload(parameters,payload,S.identifier(':payload'));
    ws.send(message);
    test.log('sent message=%s',message);
}

function send_binary_text(ws){
    test.log('send_binary_text');
    // TODO: allow quotes and backquotes in strings!!!
    // const text = 'Nice \"binary\" \\data\\ for a text response.'
    const text = 'Nice binary data for a text response.'
    var payload = new Uint8Array((new TextEncoder('utf-8').encode(text)));
    var parameters = S.expression(S.identifier(':request'),wrapper.string('text'),
                                  S.identifier(':test'),wrapper.string("send binary, receive text"));
    expect.kind=wrapper.string('text');
    expect.parameters=S.expression(S.identifier(':status'),wrapper.string('ok')).concat(parameters);
    expect.payload=wrapper.string(text);
    var message = wrapper.wrap_parameters_binary_payload(parameters,payload,S.identifier(':binary'));
    ws.send(message);
    test.log('sent message=%s',message);
}

function send_binary_binary(ws){
    test.log('send_binary_binary');
    // TODO: allow quotes and backquotes in strings!!!
    // const text = wrapper.string('Nice \"binary\" \\data\\ for a binary response.');
    const text = wrapper.string('Nice binary data for a binary response.');
    var payload = new Uint8Array((new TextEncoder('utf-8').encode(text)));
    var parameters = S.expression(S.identifier(':request'),wrapper.string('binary'),
                                  S.identifier(':test'),wrapper.string("send binary, receive binary"));
    expect.kind=wrapper.string('binary');
    expect.parameters=S.expression(S.identifier(':status'),wrapper.string('ok')).concat(parameters);
    expect.payload=payload
    var message = wrapper.wrap_parameters_binary_payload(parameters,payload,S.identifier(':binary'));
    ws.send(message);
    test.log('sent message=%s',message);
}

function send_quit(ws){
    test.log('send_quit');
    var payload = "Bye!";
    var parameters = S.expression(S.identifier(':quit'),wrapper.string('Bye!'));
    expect.kind=wrapper.string('disconnect');
    var message=wrapper.wrap_parameters_text_payload(parameters,payload,S.identifier(':payload'));
    ws.send(message);
    test.log('sent message=%s',message);
}

function receive_text(data){
    test.log('receive_text %s',data);
    test.assert(expect.kind===wrapper.string('text'),
                'receive text',
                'text', expect.kind,
                'check test-with-ws-server.js');
    const [parameters, payload] = wrapper.unwrap_parameters_text_payload(data,S.identifier(':payload'));
    if(expect.kind===wrapper.string('text')){
        test.assert(gotExpectedParameters(expect.parameters,parameters),
                    'send text receive text',
                    parameters,expect.parameters,
                    'check test-with-ws-server.js');
        test.assert(S.isEqual(payload,expect.payload),
                    'send text receive text',
                    payload,expect.payload,
                    'check test-with-ws-server.js');
    }
}

function receive_binary(data){
    const k_binary=S.identifier(':binary');
    test.log('receive_binary %s expect.kind = %s',data, expect.kind);
    test.log('typeof data = '+(typeof data));
    test.assert(expect.kind===wrapper.string('binary'),
                'receive binary',
                'binary', expect.kind,
                'check test-with-ws-server.js');
    const [parameters, binaryPayload] = wrapper.unwrap_parameters_binary_payload(data,k_binary);
    const actualBinary = wrapper.getf(parameters,k_binary,false);
    test.assert(actualBinary,
                ':binary indicator in parameters',
                actualBinary, true,
                'check unwrap_parameters_binary_payload('+data+','+k_binary+')');
    var payload = binaryPayload;
    if(actualBinary){
        payload = wrapper.string(new TextDecoder('utf-8').decode(binaryPayload));
    }
    test.log('receive_binary parameters = %s',S.serialize(parameters));
    test.log('receive_binary binaryPayload = %s',binaryPayload);
    test.log('receive_binary payload = %s',payload);
    if(expect.kind===wrapper.string('binary')){
        test.assert(gotExpectedParameters(expect.parameters,parameters),
                    'receive binary',
                    parameters,expect.parameters,
                    'check test-with-ws-server.js');
        test.assert((expect.payload.length == binaryPayload.byteLength),
                    'receive binary',
                    binaryPayload.byteLength, expect.payload.length,
                    'check test-with-ws-server.js');
        test.log('typeof expect.payload = '+(typeof expect.payload));
        test.assert(expect.payload.every((value, index) => value === binaryPayload[index]),
                    'receive binary',
                    binaryPayload, expect.payload,
                    'check test-with-ws-server.js');
    }
}

function disconnect(){
    test.log('disconnect');
    test.summary();
}

const messages = [send_text_text, receive_text,
                  send_text_binary, receive_binary,
                  send_binary_text, receive_text,
                  send_binary_binary, receive_binary,
                  send_quit, disconnect];

test.log('testsuite test-with-ws-server');

{
    var m = 0;
    const ws = new WebSockets('ws://localhost:33004/wrapper/ws');

    ws.on('error', function error(err){
        test.log('error: %s', err);
        console.error(err);
        test.failure++;
        test.summary()
    });

    ws.on('open', function open() {
        test.log('open, next %s',messages[m].name);
        test.test_case(messages[m].name);
        messages[m++](ws);
    });

    ws.on('message', function message(data) {
        test.log('received: %s', data);
        test.log('next %s',messages[m].name);
        messages[m++](data);
        test.test_case(messages[m].name);
        messages[m++](ws);
    });

    ws.on('close', function close() {
        test.log('disconnected');
        test.log('next %s',messages[m].name);
        if(messages[m]==disconnect){
            test.test_case(messages[m].name);
            messages[m++]();
        }else{
            console.error('unexpected disconnect');
            test.failure++;
        }
        test.summary();
    });

    // while(true){
    //     test.log('after ws: %s', ws);
    //     test.log('after ws: %s', ws);
    //     sleep(60);
    // }
    
}

// import WebSockets from 'ws';
// 
// const ws = new WebSockets('ws://www.host.com/path');
// 
// ws.on('error', console.error);
// 
// ws.on('open', function open() {
//   const array = new Float32Array(5);
// 
//   for (var i = 0; i < array.length; ++i) {
//     array[i] = i / 2;
//   }
// 
//   ws.send(array);
// });
// 
// 
// {
//     const expectedPayload = S.expression(S.identifier(':add'),
//                                          S.identifier(':id'),S.number(42),
//                                          S.identifier(':name'),wrapper.string('Pascal'),
//                                          S.identifier(':dimensions'),S.expression(S.number(190),S.number(60),S.number(60)));
//     const expectedParameters = [
//         S.identifier(':payload'), expectedPayload,
//         S.identifier(':x-foo'), S.number(42),
//         S.identifier(':content-type'), wrapper.string('application/json'),
//         S.identifier(':list'),S.expression(S.identifier('foo'),S.identifier('bar'),S.identifier('baz')),
//         S.identifier(':textP'),wrapper.string('((foo) (bar) (quux))'),
//         S.identifier(':text'),wrapper.string("foo\\\"bar\\\"quux")
//     ];
//     const inputText = "(:payload (:add :id 42 :name \"Pascal\" :dimensions (190 60 60)) :x-foo 42 :content-type \"application/json\" :list (foo bar baz) :textP \"((foo) (bar) (quux))\" :text \"foo\\\"bar\\\"quux\")";
//     const [actualParameters, actualPayload] = wrapper.unwrap_parameters_text_payload(inputText, S.identifier(':payload'));
//     test.assert(S.isEqual(actualParameters, expectedParameters),
//                 [inputText, S.identifier(':payload')],
//                 actualParameters, expectedParameters,
//                 'check unwrap_parameters_text_payload parameters');
//     test.assert(S.isEqual(actualPayload, expectedPayload),
//                 [inputText, S.identifier(':payload')],
//                 actualPayload, expectedPayload,
//                 'check unwrap_parameters_text_payload payload');
// }



///
/// epilog
///

// test.summary();
