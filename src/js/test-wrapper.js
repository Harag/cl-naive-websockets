const SExpr = require("s-expression.js")
const S = new SExpr();
const test = require("./test.js")
const wrapper = require('./wrapper.js')

test.set_verbose(false);

///
/// test suites:
///

test.test_case('check isEqualP');
{
    const tests = [[':PAYLOAD',':payload',true],
                   [':STATUS',':payload',false],
                   [':REQUEST',':payload',false],
                   [':TEST',':payload',false]];

    for(i=0;i<tests.length;i++){
        var expected = tests[i][2];
        var actual = wrapper.isEqualP(tests[i][0],tests[i][1]);
        test.assert(expected == actual, tests[i], actual, expected, 'check isEqualP');
    }
}


test.test_case('check encode_parameters');
{
    const parameters = [
        S.identifier(':x-foo'), S.number(42),
        S.identifier(':content-type'), wrapper.string('application/json'),
        S.identifier(':list'),S.expression(S.identifier('foo'),S.identifier('bar'),S.identifier('baz')),
        S.identifier(':textP'),wrapper.string('((foo) (bar) (quux))'),
        S.identifier(':text'),wrapper.string("foo\\\"bar\\\"quux")
    ];
    const expectedResult = '(:x-foo 42 :content-type "application/json" :list (foo bar baz) :textP "((foo) (bar) (quux))" :text "foo\\\"bar\\\"quux")'
    const actualResult = wrapper.encode_parameters(parameters);
    test.assert(actualResult === expectedResult, parameters, actualResult, expectedResult, 'check encode_parameters');
}


test.test_case('check decode_parameters');
{
    const sExpression = '(:x-foo 42 :content-type "application/json" :list (foo bar baz) :textP "((foo) (bar) (quux))" :text "foo\\\"bar\\\"quux")'
    const expectedResult = [
        S.identifier(':x-foo'), S.number(42),
        S.identifier(':content-type'), wrapper.string('application/json'),
        S.identifier(':list'),S.expression(S.identifier('foo'),S.identifier('bar'),S.identifier('baz')),
        S.identifier(':textP'),wrapper.string('((foo) (bar) (quux))'),
        S.identifier(':text'),wrapper.string('foo\\"bar\\"quux')
    ];
    const actualResult = wrapper.decode_parameters(sExpression);
    test.assert(S.isEqual(actualResult, expectedResult), sExpression, actualResult, expectedResult, 'check decode_parameters');
}


test.test_case('check wrap_parameters_text_payload');
{
    const parameters = [
        S.identifier(':x-foo'), S.number(42),
        S.identifier(':content-type'), wrapper.string('application/json'),
        S.identifier(':list'),S.expression(S.identifier('foo'),S.identifier('bar'),S.identifier('baz')),
        S.identifier(':textP'),wrapper.string('((foo) (bar) (quux))'),
        S.identifier(':text'),wrapper.string("foo\\\"bar\\\"quux")
    ];
    const payload = S.expression(S.identifier(':add'),
                                 S.identifier(':id'),S.number(42),
                                 S.identifier(':name'),wrapper.string('Pascal'),
                                 S.identifier(':dimensions'),S.expression(S.number(190),S.number(60),S.number(60)));

    const expectedResult = "(:payload (:add :id 42 :name \"Pascal\" :dimensions (190 60 60)) :x-foo 42 :content-type \"application/json\" :list (foo bar baz) :textP \"((foo) (bar) (quux))\" :text \"foo\\\"bar\\\"quux\")";
    const actualResult = wrapper.wrap_parameters_text_payload(parameters, payload, S.identifier(':payload'));
    test.assert(S.isEqual(actualResult, expectedResult),
                [parameters, payload, S.identifier(':payload')],
                actualResult, expectedResult,
                'check wrap_parameters_text_payload');
}


test.test_case('check unwrap_parameters_text_payload');
{
    const expectedPayload = S.expression(S.identifier(':add'),
                                         S.identifier(':id'),S.number(42),
                                         S.identifier(':name'),wrapper.string('Pascal'),
                                         S.identifier(':dimensions'),S.expression(S.number(190),S.number(60),S.number(60)));
    const expectedParameters = [
        S.identifier(':x-foo'), S.number(42),
        S.identifier(':content-type'), wrapper.string('application/json'),
        S.identifier(':list'),S.expression(S.identifier('foo'),S.identifier('bar'),S.identifier('baz')),
        S.identifier(':textP'),wrapper.string('((foo) (bar) (quux))'),
        S.identifier(':text'),wrapper.string("foo\\\"bar\\\"quux")
    ];
    const inputText = "(:payload (:add :id 42 :name \"Pascal\" :dimensions (190 60 60)) :x-foo 42 :content-type \"application/json\" :list (foo bar baz) :textP \"((foo) (bar) (quux))\" :text \"foo\\\"bar\\\"quux\")";
    const [actualParameters, actualPayload] = wrapper.unwrap_parameters_text_payload(inputText, S.identifier(':payload'));
    test.assert(S.isEqual(actualParameters, expectedParameters),
                [inputText, S.identifier(':payload')],
                actualParameters, expectedParameters,
                'check unwrap_parameters_text_payload parameters');
    test.assert(S.isEqual(actualPayload, expectedPayload),
                [inputText, S.identifier(':payload')],
                actualPayload, expectedPayload,
                'check unwrap_parameters_text_payload payload');
}



///
/// epilog
///

test.summary();
