///
/// minimal test framework
///

var verbose = false;
var success = 0;
var failure = 0;
var current_test_case = '';

function set_verbose(newVerbose = true){
    verbose = newVerbose;
}

function test_case(newTestCase){
    current_test_case = newTestCase;
}
function assert(condition, input, actualResult, expectedResult, description) {
    if(condition){
        success++;
        if(verbose){
            console.log("success: "+current_test_case);
        }
    }else{
        failure++;
        console.log('FAIL: ' + current_test_case + ' ' + description);
        console.log('  input = '+JSON.stringify(input,null,2));
        console.log('  expectedResult = '+JSON.stringify(expectedResult,null,2));
        console.log('  actualResult = '+JSON.stringify(actualResult,null,2));
    }
}

function summary(){
    console.log('Passed: '+success);
    console.log('Failed: '+failure);
    if(0<failure){
        process.exit(1);
    }else{
        process.exit(0);
    }
}


function log(...args){
    if(verbose){
        console.log(...args);
    }
}


module.exports = {
    success,
    failure,
    set_verbose,
    test_case,
    assert,
    summary,
    log,
}
