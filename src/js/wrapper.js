// Note: this uses TextDecoder.
// 
// TextDecoder is available in most modern browsers like Chrome, Firefox,
// Safari, Edge, and also in Node.js 11 and higher. 
// 
// However, it is not supported in Internet Explorer. If you need to
// support Internet Explorer or other older environments, you may need to
// use a polyfill or alternative method for UTF-8 text decoding.


const SExpr = require("s-expression.js")
const S = new SExpr();


function escapeString(str) {
    return str.replace(/(["\\])/g, "\\$1");
}

function encode_parameters(plist) {
    // PLIST is an array of alternative key value entries.
    // keys and values are string.
    // Return a string containing "( :key \"value\" … )"
    // each key being prefixed with a colon, and strings being wrapped
    // in double-quotes, with inner double-quotes and backspaces escaped
    // with backspaces.

    return S.serialize(plist);
}

function decode_parameters(str) {
    // Parses the string str with the following syntax:
    //
    // value := '(' pair… ')' .
    // pair := key  value .
    // key := ':' symbol .
    // value := '"' ( not-backslash-or-double-quote | '\\\\' | '\\"' )* '"' .
    //
    // and return an array of key value pairs [k1, v1, k2, v2, … kn, vn].

    return S.parse(str);
}

function prepare_payload(payload) {
    return payload;
}

function wrap_parameters_text_payload (parameters, payload, k_payload) {
    // Wraps the prepared parameters and payload, using the k_payload key.
    // 
    // PARAMETERS are a p-list of key values (keys are keywords, values are strings).
    // PAYLOAD is a string.
    // K_PAYLOAD is a keyword.
    // 
    // Returns a p-list (K-PAYLOAD PAYLOAD . PARAMETERS) serialized to a string.

    return encode_parameters([k_payload, prepare_payload(payload)].concat(parameters));
}

function wrap_parameters_binary_payload (parameters, payload, k_binary) {
    // Wraps the prepared parameters and payload, using the k_payload key.
    // 
    // PARAMETERS are a p-list of key values (keys are keywords, values are strings).
    // PAYLOAD is a string.
    // K_BINARY is a keyword.
    //
    // Returns a byte array with the parameters encoded, and concatenated with the payload.

    const encoded = (new TextEncoder('utf-8').encode(encode_parameters(S.expression(k_binary, payload.length)
                                                                       .concat(parameters))));
    const concatenated = new Uint8Array(encoded.length + payload.length);
    concatenated.set(encoded);
    concatenated.set(payload, encoded.length);
    return concatenated;
}


function string(text){
    // (prin1-to-string text)
    // generate a string that starts and ends with "
    // and where all \ and " are escaped with a \.
    
    // return `"${text.replace(/[\\""]/g, '\\$&')}"`;
    return S.string(text);
}

function unstring(string){
    // (read-from-string string) -> text
    //
    // generate a string that contains the characters from the string representation in string:
    // the first and last " are removed,
    // and all escaped characters are un-escaped. \x --> x
    return string.slice(1, -1).replace(/\\(.)/g, "$1");
}

function list_length(e) {
    if (Array.isArray(e)) {
        return e.length
    }
    return undefined
}

  /**
   * Compare whether 2 nodes are identical. Compare strings case insensitively.
   *
   * @param {any} a a node
   * @param {any} b another node to compare to
   * @return {boolean} true if they are the same
   */
function isEqualP(a, b) {
    const aArray = Array.isArray(a)
    const bArray = Array.isArray(b)
    if (aArray != bArray) {
        return false
    }

    if (!aArray) {
        const aString = (typeof a === 'string');
        const bString = (typeof b === 'string');
        if(aString != bString){
            return false;
        }
        if(!aString){
            // Compare non Array non String:
            return a === b
        }
        // Compare strings:
        if (a.length != b.length) {
            return false
        }
        return a.toLowerCase() === b.toLowerCase()
    }
    // Compare Array:
    if (a.length != b.length) {
        return false
    }

    for (let i in a) {
        if (!this.isEqual(a[i], b[i])) {
            return false
        }
    }
    return true
}

function getf(plist, indicator, def = S.boolean(false)) {
    for(i = 0 ; i<list_length(plist) ; i+=2){
        if(isEqualP(plist[i],indicator)){
            // console.log("getf(%s %s %s) --> %s",plist, indicator, def, plist[i+1]);
            return plist[i+1];
        }
    }
    // console.log("getf(%s %s %s) --> %s",plist, indicator, def, def);
    return def;
}

function remove_indicator(plist, indicator){
    var result = [];
    for(i = 0 ; i<list_length(plist) ; i+=2){
        if(!isEqualP(plist[i],indicator)){
            result.push(plist[i]);
            result.push(plist[i+1]);
        }
    }
    return result;
}

function unwrap_parameters_text_payload (text, k_payload) {
    // Return: parameters; payload
    const parameters = S.parse(text)
    const payload = getf( parameters, k_payload )
    return [remove_indicator(parameters, k_payload), payload]
}


function unwrap_parameters_binary_payload (bytes, k_binary) {

    // bytes is an array of octets.
    // k_binary is a string containing only ASCII printable characters.
    // convert k_binary to an array of octet.
    // search the octets of k_binary (terminated with a space, code 32) in bytes.
    // search after the occurence of k_binary, for a sequence of spaces (code 32), of digits (codes between 48 and 57), and a terminating space (code 32) or closing parenthese (code 41).
    // convert the sequence of digits to an integer
    // the suffix of that length of the bytes is the payload bytes.
    // everything before the payload bytes should be converted from utf-8 to a string and S.parse'd as the parameters.

    
    var index, endOfLength;
    const k_binary_array = [...k_binary.toUpperCase(), ' '].map(c => c.charCodeAt(0));
    const bytes_length = bytes.length;

    for(index = 0 ; index < bytes_length; ++index) {
        if (bytes.slice(index, index + k_binary_array.length).every((v, i) => ((v === k_binary_array[i])
                                                                               || (v === k_binary_array[i]+32)))) {
            break;
        }
    }

    if (index<bytes_length-k_binary_array.length){

        endOfLength = index + k_binary_array.length;
        // Skip the spaces.
        while ((endOfLength < bytes_length) && (bytes[endOfLength] === 32)) {
            endOfLength++;
        }
        while((endOfLength < bytes_length) && (48<=bytes[endOfLength]) && (bytes[endOfLength]<=57)){
            endOfLength++;  
        }
        // assume bytes[endOfLength] == 32 or 41.

        let length = 0;
        for (let i = index + k_binary_array.length; i < endOfLength; i++) {
            length = length * 10 + (bytes[i] - 48);
        }

        const payload = bytes.slice(-length);
        const parameters = S.parse(new TextDecoder('utf-8').decode(bytes.slice(0, -length)));
        // We need a confirmation that we received binary, so we keep the :binary indicator.
        return [parameters, new Uint8Array(payload)];
    }else{
        const parameters= S.parse(new TextDecoder('utf-8').decode(bytes));
        return [[S.identifier('status'),wrapper.string('error')].concat(parameters),
                `Missing a ${k_binary} indicator.`];
    }
}

module.exports = {
    string, unstring,
    getf, remove_indicator, isEqualP, list_length,
    encode_parameters,
    decode_parameters,
    wrap_parameters_text_payload,
    wrap_parameters_binary_payload,
    unwrap_parameters_text_payload,
    unwrap_parameters_binary_payload,
};
