;; -*- mode:lisp;coding:utf-8 -*-
(in-package :cl-naive-websockets)

(defvar *debugging* nil)
(defvar *debugging-masking-key* nil)

(deftype octet () '(unsigned-byte 8))

(defun make-buffer (size)
  (make-array size :element-type 'octet :initial-element 0))


(defgeneric connect        (layer host port secure resource-name headers)) ; for client layers
(defgeneric accept         (layer host port secure)) ; for server layers

(defgeneric send-octet     (layer byte))
(defgeneric send-octets    (layer buffer &key start end))
(defgeneric receive-octet  (layer))                        ; -> octet
(defgeneric receive-octets (layer buffer &key start end))  ; -> received length
(defgeneric flush          (layer))
(defgeneric disconnect     (layer))

(defmethod flush          ((stream stream))
  (when *debugging*
    (format *trace-output* "~&Sending: new frame~%"))
  (force-output stream))

(defmethod send-octet     ((stream stream) byte)
  (when *debugging*
    (format *trace-output* "~&Sending: ~2,'0x  ~@[~A~]~%" byte (when (<= 32 byte 126) (code-char byte))))
  (write-byte byte stream))

(defun dump-bytes (title buffer &optional (start 0) end (width 32))
  (let ((end (if end
                 (min end (length buffer))
                 (length buffer))))
    (loop :with title-width := (1+ (length title))
          :for i :from start :below end :by width
          :do ;; one line
          (if (= i start)
              (format *trace-output* "~&~A:" title)
              (format *trace-output* "~VA" title-width ""))
          (loop
            :for k :below width
            :do (if (< (+ i k) end)
                    (format *trace-output* " ~2,'0x" (aref buffer (+ i k)))
                    (format *trace-output* "   ")))
          (format *trace-output* "  ")
          (loop
            :for k :below width
            :do (if (< (+ i k) end)
                    (format *trace-output* "~:[.~;~A~]"
                            (<= 32 (aref buffer (+ i k)) 126)
                            (code-char (aref buffer (+ i k))))
                    (format *trace-output* " ")))
          (format *trace-output* "~%")
          :finally (force-output *trace-output*))))

(defmethod send-octets    ((stream stream) buffer &key (start 0) end)
  (when *debugging*
    (dump-bytes "Sending " (if *debugging-masking-key*
                               (buffer)
                               buffer) start end))
  (write-sequence buffer stream :start start :end end))

(defmethod receive-octet  ((stream stream))
  (if *debugging*
      (let ((byte (read-byte stream)))
        (format *trace-output* "~&Received: ~2,'0x~%" byte)
        byte)
      (read-byte stream)))

(defmethod receive-octets ((stream stream) buffer &key (start 0) end)
  (if *debugging*
      (let ((result (- (read-sequence buffer stream :start start :end end) start)))
        (dump-bytes "Received" buffer start end)
        result)
      (- (read-sequence buffer stream :start start :end end) start)))

(defmethod disconnect     ((stream stream))
  (close stream))


;;;; THE END ;;;;
