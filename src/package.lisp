
(defpackage :cl-naive-websockets
  (:use :common-lisp)

  (:export                              ; classes

   :websockets-server

   :websockets-acceptor-mixin

   :websockets-endpoint                 ; abstract superclass of
                                        ; websockets-server-endpoint
                                        ; and
                                        ; websockets-client-endpoint
   :websockets-server-endpoint
   :websockets-client-endpoint

   :websockets-message                  ; abstract superclass of the
                                        ; following message classes.
   :websockets-control-message
   :websockets-close-message
   :websockets-ping-message
   :websockets-pong-message
   :websockets-data-message
   :websockets-text-message
   :websockets-binary-message
   :websockets-message-fragment)

  (:export                              ; conditions
   :websockets-condition
   :websockets-error
   :websockets-closed-error
   :*debugging*
   :with-debugging
   :+ws-normal-closure+
   :+ws-going-away+
   :+ws-protocol-error+
   :+ws-unsupported-data+
   :+ws-reserved-status+
   :+ws-no-status-received+
   :+ws-abnormal-closure+
   :+ws-invalid-frame-payload-data+
   :+ws-policy-violation+
   :+ws-message-too-big+
   :+ws-mandatory-extension+
   :+ws-internal-server-error+
   :+ws-tls-handshare-error+)

  (:export                              ; endpoint readers
   :endpoint-extensions
   :endpoint-subprotocol
   :endpoint-cookies
   :endpoint-local-address
   :endpoint-local-port
   :endpoint-remote-address
   :endpoint-remote-port
   :endpoint-closedp
   ;; accessor:
   :endpoint-upper-layer)

  (:export                              ; message readers
   :message-elapsed-time
   :message-string
   :message-data
   :close-message-status
   :close-message-reason
   :message-fragment-id
   :message-fragment-opcode
   :message-fragment-position
   :message-fragment-data)

  (:export                              ; operations

   :websockets-connect
   :websockets-accept
   :websockets-disconnect

   :send-text-message
   :send-binary-message
   :send-text-fragment
   :send-binary-fragment
   :send-ping-control
   :send-pong-control
   :receive-message

   :*http-default-ports*)


  (:export                            ; interface with the http server

   :websockets-server-endpoint
   :websockets-server-subprotocols
   :websockets-server-extensions
   :websockets-server-function

   :websockets-server-valid-origin-p
   :websockets-server-negociate
   :websockets-server-serve

   :get-local-name
   :get-local-address
   :get-local-port
   :get-peer-name
   :get-peer-address
   :get-peer-port

   :get-resource-name

   :connection-secure-p
   :get-ssl-peer-certificate
   :get-ssl-verify-result

   :accept-websockets-negociation
   )

  (:export                              ; lower layer protocol:
   :connect
   :accept
   :send-octet
   :send-octets
   :receive-octet
   :receive-octets
   :flush
   :disconnect
   :octet
   :make-buffer))

