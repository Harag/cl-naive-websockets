(in-package :cl-naive-websockets)

;;;
;;; queue
;;;
;;; Simple head/tail single-link queue
;;; We don't use locks, because it is expected
;;; that the callers have already their own mutexes.
;;;

(defun make-queue () (cons nil nil))

(defun queue-head (q)
  (car (car q)))

(defun enqueue (q data)
  (if (cdr q)
      (setf (cdr (cdr q)) (cons data nil)
            (cdr q) (cdr (cdr q)))
      (setf (cdr q) (cons data nil)
            (car q) (cdr q))))

(defun dequeue (q)
  (if (car q)
      (prog1 (car (car q))
        (setf (car q) (cdr (car q)))
        (if (null (car q))
            (setf (cdr q) nil)))
      nil))


(defun queue-reduce (fun q &key (initial-value nil initial-value-p) key)
  (loop
    :with list := (if initial-value-p
                      (car q)
                      (cdr (car q)))
    :with accumulator := (if initial-value-p
                             initial-value
                             (car (car q)))
    :for element :in list
    :do (setf accumulator (funcall fun
                                     accumulator
                                     (if key
                                         (funcall key element)
                                         element)))
    :finally (return accumulator)))

(defun queue-length (q)
  (queue-reduce (function +) q :key (constantly 1) :initial-value 0))

;;;; THE END ;;;;
