(in-package :cl-naive-websockets)

;; (listen host port secure resource-name websockets-server)
;; (websockets-server-negociate websockets-server headers http-request) --> answer-headers, websockets-server-endpoint

(defclass websockets-server ()
  ((subprotocols    :initarg :subprotocols
                    :initform '()
                    :reader websockets-server-subprotocols)
   (extensions      :initarg :extensions
                    :initform '()
                    :reader websockets-server-extensions)
   (server-function :initarg :server-function
                    :reader websockets-server-function)))

(defgeneric websockets-server-valid-origin-p (websockets-server origin)
  (:method ((self t) origin)
    (declare (ignore origin))
    t))

(defgeneric websockets-server-select-protocol (websockets-server subprotocols)
  (:method ((self t) subprotocols)
    (first subprotocols))
  (:method ((server websockets-server) subprotocols)
    (first (intersection subprotocols (websockets-server-subprotocols server)
                         :test (function equalp)))))

(defgeneric websockets-server-compute-cookies (websockets-server headers http-request)
  (:method ((self t) headers http-request)
    (declare (ignore headers http-request))
    ;; TODO: specify the format of the cookie sexp.
    ;; Something like: can be used directly in (apply (function make-instance) 'hunchentoot::cookie cookie-alist)
    ;; (:name "Foo" :value "42"
    ;;  :expires (+ 2000 (get-universal-time))
    ;;  :max-age 2000
    ;;  :path "/ws/some/resource"
    ;;  :domain "ws.some.server.com"
    ;;  :secure nil
    ;;  :http-only nil)
    '()))

(defgeneric websockets-server-negociate      (websockets-server headers http-request)) ; -> headers, endpoint
(defgeneric websockets-server-serve          (websockets-server stream  endpoint))





(defgeneric get-request-host         (http-request)  ; ie. the server host name
  (:method ((self t)) "localhost"))

(defgeneric get-resource-name        (http-request)
  (:method ((self t)) nil))

(defgeneric get-cookies              (http-request)
  (:method ((self t)) nil))

(defgeneric get-headers              (http-request)
  (:method ((self t)) nil))

(defgeneric connection-secure-p      (http-request)
  (:method ((self t)) nil))

(defgeneric get-ssl-peer-certificate (http-request)
  (:method ((self t)) nil))

(defgeneric get-ssl-verify-result    (http-request)
  (:method ((self t)) nil))


(defun aget (alist key) (cdr (assoc key alist)))

(defun websocket-uri (ssl host port resource-name)
  "Builds a WebSocket (ws:// or wss://) URL."
  (format nil "~:[ws~;wss~]://~A~@[:~A~]/~A" ssl host port resource-name))

(defun format-cookies (cookies)
  (declare (ignore cookies))
  ;; TODO: implement format-cookies
  nil)

(defmethod websockets-server-negociate ((server websockets-server) headers http-request)
  ;; -> headers, endpoint

  (let* ((cookies     (websockets-server-compute-cookies server headers http-request))
         (subprotocol (websockets-server-select-protocol server
                                                         (split-sequence:split-sequence
                                                          #\,
                                                          (aget headers :sec-websocket-protocol))))
         (endpoint    (make-instance 'websockets-server-endpoint
                                     :extensions      (websockets-server-extensions server)
                                     :subprotocol     subprotocol
                                     :cookies         (aget headers :cookie)
                                     :local-address   (get-local-address http-request)
                                     :local-port      (get-local-port    http-request)
                                     :remote-address  (get-peer-address  http-request)
                                     :remote-port     (get-peer-port     http-request))))

    (let ((requested-version (aget headers :sec-websocket-version)))
      (unless (equal "13" requested-version)
        (websockets-error endpoint 1002 "Unsupported websockets version ~A" requested-version)))

    (let ((draft (aget headers :sec-websocket-draft)))
      (when draft
        (websockets-error endpoint 1002 "Websocket draft is unsupported")))

    (let ((accept      (base64-encode-bytes
                        (sha1 (ascii-string-to-bytes
                               (concatenate 'string (aget headers :sec-websocket-key)
                                            "258EAFA5-E914-47DA-95CA-C5AB0DC85B11")))))
          (origin      (aget headers :origin)))

      (when origin
        (unless (websockets-server-valid-origin-p server origin)
          (websockets-error 1002 "Invalid origin ~S" origin)))

      (values (compose-headers
               "Sec-WebSocket-Accept"     accept
               "Sec-Websocket-Origin"     origin
               "Sec-Websocket-Location"   (websocket-uri (connection-secure-p http-request)
                                                         (get-request-host http-request)
                                                         (get-local-port http-request)
                                                         (get-resource-name http-request))
               "Sec-Websocket-Protocol"   subprotocol
               "Sec-Websocket-Extensions" (and (websockets-server-extensions server)
                                               (format nil "~{~A~^,~}" (websockets-server-extensions server)))
               "Upgrade"                  "WebSocket"
               "Connection"               "Upgrade"

               "SetCookie"                (and cookies (format-cookies cookies))

               ;; ---

               "Status-Code"              "110"
               "Content-Type"             "application/octet-stream")

              endpoint))))

;; This should be done by the caller of websockets-server-negociate
;;
;; (setf (hunchentoot:return-code* reply) hunchentoot:+http-switching-protocols+
;;       (hunchentoot:content-type* reply) "application/octet-stream")
;; ;; HACK! but a decent one I think. Notice that we set both
;; ;; in and out "Connection" headers to "Upgrade". The out
;; ;; header is per RFC, but the in-header is for a different
;; ;; reason. If the client additionally send "Keep-Alive" in
;; ;; that header, hunchentoot will eventually take it as a
;; ;; reason to clobber our out-header value of "Upgrade" with
;; ;; "Keep-Alive <timeout>".
;; (setf (cdr (find :connection (hunchentoot:headers-in request) :key #'car))
;;       "Upgrade")


(defmethod websockets-server-serve ((server websockets-server) stream endpoint)
  (setf (slot-value endpoint 'lower-layer) stream)
  ;; errors are caught and reported in (hunchetoot:process-request websockets-request)
  (funcall (websockets-server-function server) server endpoint))

;;;; THE END ;;;;

