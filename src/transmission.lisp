(in-package :cl-naive-websockets)

(defconstant +default-fragment-size+ 1024) ; it should be computed in function of the MTU…

#|
* Doc


The receive-message :accept-fragments parameter tells the endpoint what the client thread wants:

- :accept-fragments nil ⇨ the client wants to read a whole message.
- :accept-fragments t ⇨ the client wants to read a message fragment by fragment.
- :accept-fragments id ⇨ the client wants to read the next fragment of a message of which it has already received a fragment.

However, the endpoint may be in a state that doesn’t allow it, or may receive another message that must be processed first:

endpoint states:
- idle
- receiving-fragment
- queuing-fragments

reception:
- a control message
- a fragment (same  message, or other message ~)
- a full message


/return/ actions will unblock the next thread

If there's a thread in the receive-message loop, then it will hold the
receive-message-lock and therefore all the other thread will block
until it returns.

| endpoint state          | accept-fragments | reception | thread action                                                  |
|-------------------------+-----------------+-----------+----------------------------------------------------------------|
| idle                    | nil             | control   | return control                                                 |
|                         |                 | fragment  | queue fragment ; -> queuing-fragments ; and loop               |
|                         |                 | message   | return message                                                 |
|                         | t               | control   | return control                                                 |
|                         |                 | fragment  | message-id++ ; return fragment ; -> receive-fragments          |
|                         |                 | message   | return message                                                 |
|                         | id              | x         | error "did not start receiving message id" ; stays in idle     |
|-------------------------+-----------------+-----------+----------------------------------------------------------------|
| receiving-fragment      | nil             | x         | block thread                                                   |
|                         | t               | control   | return control                                                 |
|                         |                 | fragment  | return fragment ; if final -> idle                             |
|                         |                 | message   | error "did not receive final fragment" => close protocol error |
|                         | id (same)       | control   | return control                                                 |
|                         |                 | fragment  | return fragment ; if final -> idle                             |
|                         |                 | message   | error "did not receive final fragment" => close protocol error |
|                         | id (different)  | x         | error "invalid message id"[#]                                  |
|-------------------------+-----------------+-----------+----------------------------------------------------------------|
| queuing-fragments       | nil             | x         | loop                                                           |
|                         | t               | x         | block thread [***]                                             |
|                         | id              | x         | error "invalid message id"[**] ; stays in queuing-fragments    |
|-------------------------+-----------------+-----------+----------------------------------------------------------------|
| queuing-fragments(loop) | x               | control   | return control (stays in queuing-fragments)                    |
|                         |                 | fragment  | queue fragment ; if final -> idle and return message           |
|                         |                 | message   | error "did not receive final fragment" => close protocol error |
|-------------------------+-----------------+-----------+----------------------------------------------------------------|


| read-a-fragment | action                                                         |
|-----------------+----------------------------------------------------------------|
| control         | return control                                                 |
| fragment        | return fragment ; if final -> idle                             |
| message         | error "did not receive final fragment" => close protocol error |

| read-a-message | action                                                         |
|----------------+----------------------------------------------------------------|
| control        | return control                                                 |
| fragment       | queue fragment ; if final -> idle and return message           |
| message        | error "did not receive final fragment" => close protocol error |


[#] we cannot wait fragments on different message-id, since there's no
multiplexing.

[*] we cannot receive multiplexed fragmented messages, so there's no
point in blocking the thread asking a different message id.

[**] the client cannot have a message id since we didn't give one for
the fragments currently queued

[***] otherwise, we could assign a message ID and return one queued
fragment, and switch to another state similar to receiving-fragment
but with the fragment queue.  It's probably better to assume it's a
different thread wanting to read a different message.


|                  |                     | accept-fragments       | accept-fragments         | accept-fragments     |
| received-control | flagmenting-mode    | t                      | (or id message-fragment) | nil                  |
|------------------+---------------------+------------------------+--------------------------+----------------------|
| received control | queuing fragments   | pop control            | wait                     | pop control          |
|                  |                     |                        |                          |                      |
| no control       | queuing fragments   | wait                   | wait                     | wait                 |
|                  |                     |                        |                          |                      |
| received control | idle                | pop control            | error                    | pop control          |
|                  |                     |                        |                          |                      |
| no control       | idle                | -> receiving fragments | error                    | -> queuing fragments |
|                  |                     | read-frame             |                          | read-frame           |
|                  |                     |                        |                          |                      |
| received control | receiving fragments | pop control            | if match                 | pop control          |
|                  |                     |                        | then read-frame          |                      |
|                  |                     |                        | else error               |                      |
|                  |                     |                        |                          |                      |
| no control       | receiving fragments | wait                   | if match                 | wait                 |
|                  |                     |                        | then read-frame          |                      |
|                  |                     |                        | else error               |                      |
|------------------+---------------------+------------------------+--------------------------+----------------------|


** Fragments

Note that we want to send either whole messages, or fragments, where
the client gets a fragment and sends it, or receives a fragment and
puts it, repeatitively until the message is sent (eg. to send or
receive a big file directly to/from the disk).

Therefore the endpoint need to know in what state it is with respect
to the client threads / fragment processing.



** Threading

Perhaps it'd be simplier to create a endpoint thread to manage
asynchronously I/O with the lower-layer.

The initial idea is that we may have one or more threads calling the
endpoint methods, and each may perform part of the lower level job,
possibly looping or blocking in the lower-layer calls.

==> Actually, each thread must do its own job, since messages must be
sent one after the other without intermixing (until an extension
exists to multiplex fragmented messages).


For this we need to synchronize the threads, so that at most a single
thread is sending a frame to the lower layer, and at most a single
thread is receiving a thread from the lower layer.  ==> 2 locks
send-lock, receive-lock.

Sending methods won't queue messages or fragments.  Instead, the
thread will lock, and loop to process and send the fragments.  To
preserve order and fairness, threads may have to enqueue a token to be
unlocked in order. (but fairness is already ensured by eg. ccl locks
and condition-variables, or not?).

RECEIVE-MESSAGE may return a dequeued message, block on reading the
next frame, or block on waiting for a received message in the queue
(if another thread is already receiving a frame).

Note also that the thread running RECEIVE-MESSAGE may have to send
control frames (close, pong), either by calling SEND-OCTETS itself (if
no other thread is sending a frame), or by enqueing the frame.  But
when it sends the control frame itself, it shall not loop to send
other enqueued frames.

Therefore sending threads that may be blocked because the receiving
thread sends a frame, must block and loop to process the queue of
frames to send themselves.


A thread that calls send-<something> or receive-message,  should
perform the write-frame and read-frame corresponding to its message
itself, to keep fairness.   A thread that calls receive-message may
have to send a response (ping and close). In both cases, the thread
may have to call write-frame or read-frame (and write-frame) several
times, to process its message.

To preserve fairness between the client threads, we will need to
synchronize and queue the threads:

*** Sending a control frame in the middle of message fragments.

#+BEGIN_QUOTE
Thread A                 Thread B                Thread C
- send-message
    + write-frame        - send-ping
         W                   + write-frame     - send-message
    + write-frame        - message sent              W
    + write-frame                                    W
- message sent                                    + write-frame
                                                  + write-frame
                                                  + write-frame
                                                - message sent
#+END_QUOTE

*** Receiving a control frame in the middle of message fragments

#+BEGIN_QUOTE
Thread A                       Thread B                Thread C
- receive-message
   + read-frame (fragment)      - receive-message
   + read-frame (ping)               W
      + write-frame (pong)           W                 - receive-message
   + read-frame (fragment)           W                         W
   + read-frame (fragment)           W                         W
   + read-frame (pong)   --->   - returns pong message         W
   + read-frame (fragment)                                     W
- returns complete message                                     W
                                                         + read-frame (fragment)
                                                         + read-frame (fragment)
                                                         + read-frame (fragment)
                                                      - returns complete message
#+END_QUOTE

*** Sending and Receiving a ping

#+BEGIN_QUOTE
Thread A                       Thread B
- send-message
   + write-frame (fragment)
   + write-frame (fragment)      - receive-message
   + write-frame (fragment)         + read-frame (ping)
                            <----   + enqueue pong for sender
   + write-frame (pong)             + read-frame (fragment)
   + write-frame (fragment)         + read-frame (fragment)
- message sent                   - returns complete message
#+END_QUOTE

*** Receiving a ping alone

#+BEGIN_QUOTE
Thread B
- receive-message
   + read-frame (ping)
   + write-frame (pong)
   + read-frame (fragment)
   + read-frame (fragment)
- returns complete message
#+END_QUOTE

Note if receive ping option, then the complete message is the ping,
otherwise  the thread continues reading frames to build a complete
message.

** Sending messages

Sending is managed by serializing the threads that want to send.

SEND-TEXT-MESSAGE and SEND-BINARY-MESSAGE take a may-fragment
parameter that can be:

- NIL: no fragmentation, the message is sent in a single frame).
- T: fragmentation is allowed (a default maximum fragment size is used).
- size (integer): the message is fragmented by the given SIZE.

The default maximum fragment size and the given fragment size must be
greater than 125.

Those methods will loop on the message data to send the fragments, and if no sending is
occuring, loop until the whole message is send, fragment by fragment.

** Sending fragments

The send-text-fragment and send-binary-fragment  methods let the user
enqueue a message, fragment by fragment.  when the FRAGMENT parameter
is NIL, the first fragment of a new message is enqueued, and a
fragment object (message ID) is returned.  This fragment object must
then be passed to the successive calls to enqueue the following
fragments in the same message. When the FINAL parameter is true, the
last fragment is enqueued.

Fragmented messages can be scheduled for sending as soon as their
first fragment is enqueued.

1- the message order is preserved, first message (or first fragment of
   first message) received, first sent.

2- if the first fragments of a fragmented message are sent, then until
   the final fragment is sent, no other message (fragmented or not)
   can be sent, apart from the control messages (ping, pong and
   close).

3- ping, pong and close are enqueued with a higher priority, to be
   sent before the queued messages or fragments.

** Receiving

TODO: define an API to receive fragments while their arrive (we may not want to store them in RAM, but on disk).

The receive-message method dequeues a message if available in the
received message queue, and returns it.

If the queue is empty, then it will call receive-octet/receive-octets
from the lower layer, and will process the received frame.

When a ping is received, a pong frame is immediately created and
enqueued or immediately sent.

When a close is received, a close frame is immediately created and
enqueued or immediately sent (unless we're already closing). In
addition, the close message is enqueued, so that receive-message may
signal the end-of-file, either by signaling a condition or by
returning the eof-value.

When a fragment is received, it's enqueued; if it's a final fragment,
the fragments in the queue are used to build a message, which is then
enqueued for reception.

Finally if new messages are available receive-message may return, or
it may loop reading (and probably blocking) the next frame.

** Lower Layer

The lower layer is intended to be a thin layer over the network socket
stream in case of HTTP, or the SSL/TLS layer in case of HTTPS.  No
threading is expected from the lower layer.  Calls to send-octet[s]
and receive-octet[s] can block until the data is sent or received.

** Diagram

Very approximative dataflow diagram; the queuing of sending messages
and frames and thread blocking that may occur is not depicted clearly
or at all.


  websockets-disconnect
  send-ping-control
  send-pong-control
  send-binary-message send-binary-fragment
  send-text-message   send-text-fragment                               receive-message
         |              |                                                  ^
         v              v                                                  |
+----------------------------------------------------------------------------------------------------------------+
|        |              |             endpoint                             ^                                     |
|        |              |                                                  |                                     |
|        |              |                                                  |                                     |
|        |              |                                                  |                                     |
|        +------------->|                                               received-messages-queue:                 |
|        |              |                                      / \     +----------+    +----------+              |
|        |              |                                     <   >--->| message  |--->| message  |--->          |
|        |              |                                      \ /     +----------+    +----------+              |
|        |              |                                       ^          ^                                     |
|        |              |                                       |          |                                     |
|        |              |                                 data  |          |fragment completed                   |
|        |              |                                 pong  |          |                                     |
|        |              v                                close  |          |                                     |
|        v          sending-fragments-queue                     |      receiving-fragments-queue                 |
|   +----------+    +----------+    +----------+               / \     +----------+    +----------+              |
|   | message  |    | fragment |--->| fragment |--->          <   >--->| fragment |--->| fragment |--->          |
|   +----------+    +----------+    +----------+               \ /     +----------+    +----------+              |
|        |                                                      |                                                |
|        |                                                      |                                                |
|        |                                                      |                                                |
|        |                          +----------+     close     / \                                               |
|        |<-------------------------|   close  |<-------------<   >                                              |
|        |                          +----------+               \ /                                               |
|        |                                                      ^                                                |
|        |                                                      |                                                |
|        |                          +----------+      ping     / \                                               |
|        |<-------------------------|   pong   |<-------------<   >                                              |
|        |                          +----------+               \ /                                               |
|        +-------------+                                        ^                                                |
|        |             |                                        |                                                |
|        v             |                                        |                                                |
| +---------------+    |                                  +--------------+                                       |
| |  write-frame  |    |                                  |  read-frame  |                                       |
+-+---------------+---------------------------------------+--------------+---------------------------------------+
         |             |                                        ^
         |             |                                        |
         v             v                                    receive-octet
    send-octets    disconnect                               receive-octets
+----------------------------------------------------------------------------+
|                               lower-layer                                  |
+----------------------------------------------------------------------------+



receive-message:

1- attempt to read a control message from the message queue.
2- if there were no control message queued, then
3.1- read a frame;
3.2- if a ping or a close, then send the response or enqueue it;
3.3- if a fragment is received and :accept-fragments t was given,
        then return the fragment;
        else enqueue the fragment, and read the next frame.
3.3.1- if the final fragment was received, then build the message and return it.
3.4- if a whole message is received then return the whole message.


|#

(defgeneric record-ping-timestamp (endpoint frame))
(defmethod record-ping-timestamp ((endpoint websockets-endpoint) (frame frame))
  (warn "not-implemented-yet ~S ~S ~S" 'record-ping-timestamp endpoint frame))

(defgeneric check-frame-masking (endpoint frame))

(defmethod check-frame-masking ((endpoint websockets-endpoint) (frame frame))
  ;; We check that the received frame is masked when it should.
  ;; |          | client | server |
  ;; |----------+--------+--------|
  ;; | sends    | masked | clear  |
  ;; | receives | clear  | masked |
  (if (not (must-mask-when-sending-p endpoint)) ; receiving masking is negation of sending masking.
      ;; we're the server
      (unless (frame-masking-key frame)
        (websockets-error endpoint 1002 "Frame received from client by server must be masked"))
      ;; we're the client
      (when (frame-masking-key frame)
        (websockets-error endpoint 1002 "Frame received from server by client must not be masked"))))


(defun %make-message (endpoint opcode data)
  (case opcode
    ((#.+continuation-frame+)
     (websockets-error endpoint 1011 "Trying to make a message from a continuation fragment"))
    ((#.+text-frame+)
     (let ((text (handler-case
                     (trivial-utf-8:utf-8-bytes-to-string data)
                   (trivial-utf-8:utf-8-decoding-error (err)
                     (websockets-error endpoint 1002 "utf-8 decoding error: ~A" err)))))
       (make-instance 'websockets-text-message
                      :data data
                      :string text)))
    ((#.+binary-frame+)
     (make-instance 'websockets-binary-message
                    :data data))
    ((#.+close-frame+)
     (make-instance 'websockets-close-message
                    :data data))
    ((#.+ping-frame+)
     ;; we can enqueue a ping message
     (make-instance 'websockets-ping-message
                    :data data))
    ((#.+pong-frame+)
     (make-instance 'websockets-pong-message
                    :data data
                    :elapsed-time 0
                    ;; TODO: compute elapsed time from timestamps of sent PING, matching pong with pings.
                    ))
    (otherwise
     (websockets-error endpoint 1002 "unexpected frame opcode ~A" opcode))))

(defgeneric make-message (endpoint frame))

(defmethod make-message (endpoint (frame frame))
  ;; make a message from a complete frame
  (%make-message endpoint
                 (frame-opcode frame)
                 (frame-data frame)))

(defmethod make-message (endpoint (frames cons))
  ;; make a message from a queue of fragments.
  (let* ((total-length (queue-reduce (function +) frames
                                     :initial-value 0
                                     :key (lambda (frame) (length (frame-data frame)))))
         (buffer       (make-buffer total-length))
         (start        0))
    (queue-reduce (lambda (buffer data)
                    (replace buffer data :start1 start)
                    (incf start (length data))
                    buffer)
                  frames
                  :initial-value buffer
                  :key (function frame-data))
    (%make-message endpoint
                   (frame-opcode (queue-head frames))
                   buffer)))

(defgeneric flush-receive-fragments-queue (endpoint))
(defmethod flush-receive-fragments-queue ((endpoint websockets-endpoint))
  (setf (slot-value endpoint '%receive-fragment-queue) (make-queue)))

(defgeneric enqueue-frame (endpoint frame))

(defgeneric fragmentp (frame)
  (:method ((frame frame))
    (not (and (plusp (frame-opcode frame))
              (frame-finp frame)))))

(defmethod enqueue-frame ((endpoint websockets-endpoint) (frame frame))
  "Return nil when the frame is enqueued, or return a message, or signal a websockets-error."
  ;; | fin | opcode | what                  |
  ;; |-----+--------+-----------------------|
  ;; |   1 | ≠ 0    | not fragmented        |
  ;; |   0 | ≠ 0    | first fragment        |
  ;; |   0 | = 0    | continuation fragment |
  ;; |   1 | = 0    | last fragment         |
  (if (plusp (frame-opcode frame))
      (if (frame-finp frame)
          ;; not fragmented
          (if (control-frame-p frame)
              (make-message endpoint frame)
              ;; test if we are in the middle of a fragmented message
              (if (queue-head (endpoint-receive-fragment-queue endpoint))
                  (websockets-error endpoint 1002 "Received a unfragmented message in the middle of a fragmented message")
                  (make-message endpoint frame)))

          ;; first fragment
          ;; test if we are in the middle of a fragmented message
          (if (queue-head (endpoint-receive-fragment-queue endpoint))
              (websockets-error endpoint 1002 "Received a new fragmented message in the middle of a fragmented message")
              (progn
                (enqueue (endpoint-receive-fragment-queue endpoint) frame)
                nil)))

      (if (frame-finp frame)
          ;; last fragment
          (if (queue-head (endpoint-receive-fragment-queue endpoint))
              (progn
                (enqueue (endpoint-receive-fragment-queue endpoint) frame)
                (prog1 (make-message endpoint (endpoint-receive-fragment-queue endpoint))
                  (flush-receive-fragments-queue endpoint)))
              (websockets-error endpoint 1002 "Received a final fragment without a first fragment"))
          ;; continuation fragment
          (if (queue-head (endpoint-receive-fragment-queue endpoint))
              (progn
                (enqueue (endpoint-receive-fragment-queue endpoint) frame)
                nil)
              (websockets-error endpoint 1002 "Received a continuation fragment without a first fragment")))))

;; o  An unfragmented message consists of a single frame with the FIN
;;    bit set (Section 5.2) and an opcode other than 0.
;;
;; o  A fragmented message consists of a single frame with the FIN bit
;;    clear and an opcode other than 0, followed by zero or more frames
;;    with the FIN bit clear and the opcode set to 0, and terminated by
;;    a single frame with the FIN bit set and an opcode of 0.  A
;;    fragmented message is conceptually equivalent to a single larger
;;    message whose payload is equal to the concatenation of the
;;    payloads of the fragments in order; however, in the presence of
;;    extensions, this may not hold true as the extension defines the
;;    interpretation of the "Extension data" present.  For instance,
;;    "Extension data" may only be present at the beginning of the first
;;    fragment and apply to subsequent fragments, or there may be
;;    "Extension data" present in each of the fragments that applies
;;    only to that particular fragment.  In the absence of "Extension
;;    data", the following example demonstrates how fragmentation works.
;;
;;    EXAMPLE: For a text message sent as three fragments, the first
;;    fragment would have an opcode of 0x1 and a FIN bit clear, the
;;    second fragment would have an opcode of 0x0 and a FIN bit clear,
;;    and the third fragment would have an opcode of 0x0 and a FIN bit
;;    that is set.
;;
;; o  Control frames (see Section 5.5) MAY be injected in the middle of
;;    a fragmented message.  Control frames themselves MUST NOT be
;;    fragmented.
;;
;; o  Message fragments MUST be delivered to the recipient in the order
;;    sent by the sender.
;;
;; o  The fragments of one message MUST NOT be interleaved between the
;;    fragments of another message unless an extension has been
;;    negotiated that can interpret the interleaving.
;;
;; o  An endpoint MUST be capable of handling control frames in the
;;    middle of a fragmented message.
;;
;; o  A sender MAY create fragments of any size for non-control
;;    messages.
;;
;; o  Clients and servers MUST support receiving both fragmented and
;;    unfragmented messages.
;;
;; o  As control frames cannot be fragmented, an intermediary MUST NOT
;;    attempt to change the fragmentation of a control frame.
;;
;; o  An intermediary MUST NOT change the fragmentation of a message if
;;    any reserved bit values are used and the meaning of these values
;;    is not known to the intermediary.
;;
;; o  An intermediary MUST NOT change the fragmentation of any message
;;    in the context of a connection where extensions have been
;;    negotiated and the intermediary is not aware of the semantics of
;;    the negotiated extensions.  Similarly, an intermediary that didn't
;;    see the WebSocket handshake (and wasn't notified about its
;;    content) that resulted in a WebSocket connection MUST NOT change
;;    the fragmentation of any message of such connection.
;;
;; o  As a consequence of these rules, all fragments of a message are of
;;    the same type, as set by the first fragment's opcode.  Since
;;    control frames cannot be fragmented, the type for all fragments in
;;    a message MUST be either text, binary, or one of the reserved
;;    opcodes.


#|

| accept-fragments | received              | action                                                                                       |
|-----------------+-----------------------+----------------------------------------------------------------------------------------------|
| yes             | initial-fragment      | check fragment queue empty; record id and state; return fragment                             |
| yes             | continuation-fragment | check fragment queue empty; match id; update state; return fragment                          |
| yes             | final-fragment        | check fragment queue empty; match id; update state; return fragment                          |
| yes             | data-message          | check fragment queue empty; check state; return message                                      |
| yes             | control-message       | return message                                                                               |
| no              | initial-fragment      | check fragment queue empty; check state; enqueue fragment; loop                              |
| no              | continuation-fragment | check fragment queue not empty; check state; enqueue fragment; loop                          |
| no              | final-fragment        | check fragment queue not empty; check state; enqueue fragment; build message; return message |
| no              | data-message          | check fragment queue empty; check state; return message                                      |
| no              | control-message       |                                                                                              |


endpoint receiving states

|                  |                     | accept-fragments       | accept-fragments         | accept-fragments     |
| received-control | flagmenting-mode    | t                      | (or id message-fragment) | nil                  |
|------------------+---------------------+------------------------+--------------------------+----------------------|
| received control | queuing fragments   | pop control            | wait                     | pop control          |
|                  |                     |                        |                          |                      |
| no control       | queuing fragments   | wait                   | wait                     | wait                 |
|                  |                     |                        |                          |                      |
| received control | idle                | pop control            | error                    | pop control          |
|                  |                     |                        |                          |                      |
| no control       | idle                | -> receiving fragments | error                    | -> queuing fragments |
|                  |                     | read-frame             |                          | read-frame           |
|                  |                     |                        |                          |                      |
| received control | receiving fragments | pop control            | if match                 | pop control          |
|                  |                     |                        | then read-frame          |                      |
|                  |                     |                        | else error               |                      |
|                  |                     |                        |                          |                      |
| no control       | receiving fragments | wait                   | if match                 | wait                 |
|                  |                     |                        | then read-frame          |                      |
|                  |                     |                        | else error               |                      |
|------------------+---------------------+------------------------+--------------------------+----------------------|


received-control
--> lock %receive-message-queue-lock
    dequeue %receive-message-queue-lock
    test dequeued messages.
|#

(defgeneric send-frame (endpoint frame))
(defgeneric make-close-frame (endpoint status reason))

(defgeneric endpoint-send-next-message-id (endpoint)
  (:method ((endpoint websockets-endpoint))
    (incf (slot-value endpoint '%send-current-message-id))))

(defgeneric endpoint-receive-next-message-id (endpoint)
  (:method ((endpoint websockets-endpoint))
    (incf (slot-value endpoint '%receive-current-message-id))))

(defgeneric message-id-match-p (id expected-id)
  (:method ((message websockets-message-fragment) (expected-id integer))
    (message-id-match-p (message-fragment-id message) expected-id))
  (:method ((id integer) (expected-id integer))
    (= id expected-id)))

(defmacro define-bit-field-accessor (name class accessor byte)
  `(progn
     (defmethod ,name                  ((self ,class)) (ldb ,byte (,accessor self)))
     (defmethod (setf ,name)  (new-bits (self ,class)) (setf (,accessor self) (dpb new-bits ,byte (,accessor self))))))

(define-bit-field-accessor endpoint-closing          websockets-endpoint endpoint-state (byte 1 0))
(define-bit-field-accessor endpoint-fragmenting-mode websockets-endpoint endpoint-state (byte 2 1))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +fragmenting-mode-idle+      0)
  (defconstant +fragmenting-mode-queuing+   1)
  (defconstant +fragmenting-mode-receiving+ 2))

(defmethod receive-message ((endpoint websockets-endpoint) &optional eof-error-p eof-value &key accept-fragments)
  (labels ((return-closed (message)
             (if eof-error-p
                 (websockets-closed-error endpoint
                                          :status (close-message-status message)
                                          :reason (close-message-reason message)
                                          :message message)
                 (return-from receive-message (values eof-value
                                                      (close-message-status message)
                                                      (close-message-reason message)
                                                      message))))

           (wait ()
             (bt:condition-wait (endpoint-can-receive endpoint)
                                (endpoint-receive-message-lock endpoint)))

           (notify ()
             (bt:condition-notify (endpoint-can-receive endpoint)))

           (read-a-frame ()
             (let ((message (closedp endpoint)))
               (when message
                 (return-closed message)))
             (loop
               :named try
               :do ;; no other thread is trying to read a frame because of
                   ;; the receive-message-lock

                   (let ((frame (read-frame (lower-layer endpoint))))
                     (handler-case (check-frame-masking endpoint frame)
                       (:no-error (&rest ignored)
                         (declare (ignore ignored))
                         (cond

                           ((= (frame-opcode frame) +ping-frame+)
                            (record-ping-timestamp endpoint frame)
                            (let ((pong (make-instance 'frame
                                                       :opcode +pong-frame+
                                                       :finp t
                                                       :data (frame-data frame)
                                                       :payload-length (length (frame-data frame))
                                                       :masking-key (masking-key (must-mask-when-sending-p endpoint)))))
                              (send-frame endpoint pong)))

                           ((= (frame-opcode frame) +close-frame+)
                            (when (zerop (endpoint-closing endpoint))
                              (setf (endpoint-closing endpoint) 1)
                              (setf (frame-masking-key frame) (masking-key (must-mask-when-sending-p endpoint)))
                              (send-frame endpoint frame)
                              (setf (slot-value endpoint 'closedp) (make-message endpoint frame))
                              (return-closed (closedp endpoint))))

                           ((fragmentp frame)
                            (if (= +fragmenting-mode-queuing+ (endpoint-fragmenting-mode endpoint))
                                (progn
                                ;; TODO: we could read the current frame into the message buffer with (read-frame-data stream frame buffer :start s :end e)
                                  (read-frame-data (lower-layer endpoint) frame)
                                  (let ((message (enqueue-frame endpoint frame)))
                                    (when message
                                      (return-from receive-message message))))
                                (return-from receive-message
                                  (progn
                                    (read-frame-data (lower-layer endpoint) frame)
                                    (make-instance 'websockets-message-fragment
                                                   :id (if (plusp (frame-opcode frame))
                                                           (endpoint-receive-next-message-id endpoint)
                                                           (endpoint-receive-current-message-id endpoint)))))))
                           (t
                            (read-frame-data (lower-layer endpoint) frame)
                            (return-from receive-message (make-message endpoint frame)))))

                       (websockets-error (err)
                         (when (zerop (endpoint-closing endpoint))
                           (websockets-disconnect endpoint
                                                  :status (websockets-error-status err)
                                                  :reason (format nil "~?"
                                                                  (simple-condition-format-control err)
                                                                  (simple-condition-format-arguments err))))
                         (error err)))))))

    (unwind-protect
         (bt:with-lock-held ((endpoint-receive-message-lock endpoint))
           (ecase (endpoint-fragmenting-mode endpoint)
             ((#.+fragmenting-mode-idle+)
              (case accept-fragments
                ((nil)      (read-a-frame))
                ((t)        (read-a-frame))
                (otherwise  (error #|TODO|# "did not start receiving message id"))))
             ((#.+fragmenting-mode-receiving+)
              (case accept-fragments
                ((nil)      (wait))
                ((t)        (read-a-frame))
                (otherwise  (if (message-id-match-p accept-fragments (endpoint-receive-current-message-id endpoint))
                                (read-a-frame)
                                (error #|TODO|# "invalid message id cannot receive two fragmented messages at the same time")))))
             ((#.+fragmenting-mode-queuing+)
              (case accept-fragments
                ((nil)      (read-a-frame))
                ((t)        (wait))
                (otherwise  (error #|TODO|# "invalid message id cannot receive two fragmented messages at the same time"))))))
      (notify))))



(defmethod send-frame ((endpoint websockets-endpoint) (frame frame))
  (bt:with-lock-held ((endpoint-control-message-queue-lock endpoint))
    (enqueue (endpoint-control-message-queue endpoint) frame))
  (when (bt:acquire-lock (endpoint-write-frame-lock endpoint) nil)
    (unwind-protect
         (loop
           :for frame := (bt:with-lock-held ((endpoint-control-message-queue-lock endpoint))
                           (dequeue (endpoint-control-message-queue endpoint)))
           :while frame
           :do (write-frame (lower-layer endpoint) frame))
      (bt:release-lock (endpoint-write-frame-lock endpoint)))))
#|

| final | fragment | state        | acquire | release |
|-------+----------+--------------+---------+---------|
| x     | nil      | initial      | yes     | no      |
| nil   | id       | continuation | no      | no      |
| t     | x        | final        | no      | release |

|#

(defgeneric send-fragment (endpoint opcode data final fragment)
  (:method ((endpoint websockets-endpoint) opcode data final fragment)
    (let ((frame (make-instance 'frame
                                :opcode (if (null fragment)
                                            opcode
                                            +continuation-frame+)
                                :finp final
                                :data data
                                :payload-length (length data)
                                :masking-key (masking-key (must-mask-when-sending-p endpoint))))
          (message-id fragment))
      (cond
        ((null fragment)
         (bt:acquire-lock (endpoint-send-message-lock endpoint) t)
         (setf message-id (endpoint-send-next-message-id endpoint)))
        ((/= fragment (endpoint-send-current-message-id endpoint))
         (error "invalid message id cannot send two fragmented messages at the same time")))
      (send-frame endpoint frame)
      (when final
        (bt:release-lock (endpoint-send-message-lock endpoint)))
      message-id)))

(defgeneric send-message (endpoint opcode data may-fragment)
  (:method ((endpoint websockets-endpoint) opcode data may-fragment)
    (let* ((buffer-size  (case may-fragment
                           ((nil) (length data))
                           ((t)   (min (length data) +default-fragment-size+))
                           (otherwise
                            (check-type may-fragment (integer 125))
                            (min (length data) may-fragment))))
           (start        0)
           (buffer       (make-array buffer-size :element-type 'octet
                                                 :displaced-to data
                                                 :displaced-index-offset start
                                                 :fill-pointer buffer-size
                                                 :adjustable t))
           (frame        (make-instance 'frame
                                        :opcode opcode
                                        :finp (= (+ start buffer-size) (length data))
                                        :data buffer
                                        :payload-length (length buffer)
                                        :masking-key (masking-key (must-mask-when-sending-p endpoint)))))
      (bt:with-lock-held ((endpoint-send-message-lock endpoint))
        (loop
          :do (send-frame endpoint frame)
              (incf start buffer-size)
              (setf buffer-size (min (- (length data) start) buffer-size))
          :until (zerop buffer-size)
          :do (setf buffer (adjust-array buffer (array-dimensions buffer)
                                         :displaced-index-offset start :fill-pointer buffer-size)
                    (frame-opcode frame) +continuation-frame+
                    (frame-finp   frame) (= (+ start buffer-size) (length data))
                    (frame-payload-length frame) buffer-size
                    (frame-masking-key frame) (masking-key (must-mask-when-sending-p endpoint))))))))

;; Control messages:

(defmethod close-message-status ((message websockets-close-message)) ; -> integer
  (let ((data (message-data message)))
    (if (< (length data) 2)
        0
        (dpb (aref data 0) (byte 8 8) (aref data 1)))))

(defmethod close-message-reason ((message websockets-close-message)) ; -> string
  (let ((data  (message-data message)))
    (handler-case
        (trivial-utf-8:utf-8-bytes-to-string data :start 2)
      (trivial-utf-8:utf-8-decoding-error ()
        (subseq data 2)))))

(defmethod make-close-frame ((endpoint websockets-endpoint) status reason)
  (let ((data (trivial-utf-8:string-to-utf-8-bytes reason)))
    (unless (< (length data) (- 125 2))
      (error "reason too long (encoded in utf-8 it must be no more than 123 octets"))
    (let ((buffer (concatenate '(vector octet)
                               (multiple-value-call (function vector) (truncate status 256))
                               data)))
      (make-instance 'frame
                     :opcode +close-frame+
                     :finp t
                     :data buffer
                     :payload-length (length buffer)
                     :masking-key (masking-key (must-mask-when-sending-p endpoint))))))

(defmethod websockets-disconnect ((endpoint websockets-endpoint) &key (status 1000) (reason "Normal disconnection"))
  ;; TODO: MUTEX?!?
  (unless (zerop (endpoint-closing endpoint))
    (error "endpoint is closed"))
  (setf (endpoint-closing endpoint) 1)
  (let ((frame (make-close-frame endpoint status reason)))
    (send-frame endpoint frame)
    (setf (slot-value endpoint 'closedp) (make-message endpoint frame))))

(defmethod send-ping-control ((endpoint websockets-endpoint) data)
  (unless (< (length data) 125)
    (error "ping data too long: it must be no more than 125 octets"))
  (unless (zerop (endpoint-closing endpoint))
    (error "endpoint is closed"))
  (let ((frame (make-instance 'frame
                              :opcode +ping-frame+
                              :finp t
                              :data data
                              :payload-length (length data)
                              :masking-key (masking-key (must-mask-when-sending-p endpoint)))))
    (send-frame endpoint frame)))

(defmethod send-pong-control ((endpoint websockets-endpoint) data)
  (unless (< (length data) 125)
    (error "pong data too long: it must be no more than 125 octets"))
  (unless (zerop (endpoint-closing endpoint))
    (error "endpoint is closed"))
  ;; unsolicited heartbeat.
  (let ((frame (make-instance 'frame
                              :opcode +pong-frame+
                              :finp t
                              :data data
                              :payload-length (length data)
                              :masking-key (masking-key (must-mask-when-sending-p endpoint)))))
    (send-frame endpoint frame)))

;; Data Messages

(defmethod send-text-message    ((endpoint websockets-endpoint) text   &key may-fragment)
  (send-message endpoint +text-frame+ (trivial-utf-8:string-to-utf-8-bytes text) may-fragment))

(defmethod send-binary-message  ((endpoint websockets-endpoint) binary &key may-fragment)
  (send-message endpoint +binary-frame+ binary may-fragment))

(defmethod send-text-fragment   ((endpoint websockets-endpoint) text   &key final fragment)
  (send-fragment endpoint +text-frame+ (trivial-utf-8:string-to-utf-8-bytes text) final fragment))

(defmethod send-binary-fragment ((endpoint websockets-endpoint) binary &key final fragment)
  (send-fragment endpoint +binary-frame+ binary final fragment))


;;;; THE END ;;;;
