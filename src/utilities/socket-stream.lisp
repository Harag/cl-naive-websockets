(in-package :cl-naive-websockets)

;; Unwrap streams:

(defgeneric bare-streams (stream)
  (:documentation "RETURN: A list of streams that are not compound streams
(and therefore usable eg. by #+clisp SOCKET:SOCKET-STATUS).")

  (:method ((stream echo-stream))
    (append (bare-streams (echo-stream-output-stream stream))
            (bare-streams (echo-stream-input-stream  stream))))

  (:method ((stream two-way-stream))
    (append (bare-streams (two-way-stream-output-stream stream))
            (bare-streams (two-way-stream-input-stream stream))))

  (:method ((stream synonym-stream))
    (bare-streams (symbol-value (synonym-stream-symbol stream))))

  (:method ((stream broadcast-stream))
    (mapcar (function bare-streams) (broadcast-stream-streams stream)))

  (:method ((stream stream))
    (list stream)))

(defmethod bare-streams ((stream t))                          '())
(defmethod bare-streams ((stream flexi-streams:flexi-stream)) (bare-streams (flexi-streams:flexi-stream-stream stream)))
(defmethod bare-streams ((stream chunga:chunked-stream))      (bare-streams (chunga:chunked-stream-stream stream)))


;; Default implementation:

(defmethod get-local-name (stream)
  (list (get-local-address stream)
        (get-local-port stream)))

(defmethod get-peer-name (stream)
  (list (get-peer-address stream)
        (get-peer-port stream)))

(defmethod get-local-address (stream)
  (dolist (socket (bare-streams stream) nil)
    (let ((address (ignore-errors (usocket:get-local-address socket))))
      (when address (return-from get-local-address address)))))

(defmethod get-local-port    (stream)
  (dolist (socket (bare-streams stream) nil)
    (let ((port (ignore-errors (usocket:get-local-port socket))))
      (when port (return-from get-local-port port)))))

(defmethod get-peer-address (stream)
  (dolist (socket (bare-streams stream) nil)
    (let ((address (ignore-errors (usocket:get-peer-address socket))))
      (when address (return-from get-peer-address address)))))

(defmethod get-peer-port    (stream)
  (dolist (socket (bare-streams stream) nil)
    (let ((port (ignore-errors (usocket:get-peer-port socket))))
      (when port (return-from get-peer-port port)))))


;; Extracted from usocket/backend:

#-(and)
(
 #+abcl                     (:file "abcl")
 #+(or allegro cormanlisp)  (:file "allegro")
 #+clisp                    (:file "clisp")
 #+(or openmcl clozure)     (:file "openmcl")
 #+clozure                  (:file "clozure" :depends-on ("openmcl"))
 #+cmu                      (:file "cmucl")
 #+(or sbcl ecl clasp)      (:file "sbcl")
 #+ecl                      (:file "ecl" :depends-on ("sbcl"))
 #+clasp                    (:file "clasp" :depends-on ("sbcl"))
 #+lispworks                (:file "lispworks")
 #+mcl                      (:file "mcl")
 #+mocl                     (:file "mocl")
 #+scl                      (:file "scl")
 #+genera                   (:file "genera")
 #+mezzano                  (:file "mezzano"))


#+(or openmcl clozure)
(progn

  (defun hbo-to-vector-quad (integer)   ; exported
    "Host-byte-order integer to dotted-quad string conversion utility."
    (let ((first (ldb (byte 8 24) integer))
          (second (ldb (byte 8 16) integer))
          (third (ldb (byte 8 8) integer))
          (fourth (ldb (byte 8 0) integer)))
      (vector first second third fourth)))

  (defun usocket-host-address (address)
    (cond
      ((integerp address)
       (hbo-to-vector-quad address))
      ((and (arrayp address)
            (= (length address) 16)
            (every #'= address #(0 0 0 0 0 0 0 0 0 0 #xff #xff)))
       (make-array 4 :displaced-to address :displaced-index-offset 12))
      (t
       address)))

  (defmethod ccl::host ((object null))
    nil)

  (defmethod ccl::port ((object null))
    nil)

  (defmethod get-local-address ((socket ccl::socket))
    (usocket-host-address (openmcl-socket:local-host socket)))

  (defmethod get-peer-address ((socket ccl::socket))
    (usocket-host-address (openmcl-socket:remote-host socket)))

  (defmethod get-local-port ((socket ccl::socket))
    (openmcl-socket:local-port socket))

  (defmethod get-peer-port ((socket ccl::socket))
    (openmcl-socket:remote-port socket))

  (defmethod get-local-name ((socket ccl::socket))
    (list (get-local-address socket) (get-local-port socket)))

  (defmethod get-peer-name ((socket ccl::socket))
    (list (get-peer-address socket) (get-peer-port socket))))



#+(or sbcl ecl clasp)
(progn

  (defmethod get-local-name ((socket sb-bsd-sockets:socket))
    (sb-bsd-sockets:socket-name socket))

  (defmethod get-peer-name ((socket sb-bsd-sockets:socket))
    (sb-bsd-sockets:socket-peername socket))

  (defmethod get-local-address ((socket sb-bsd-sockets:socket))
    (nth-value 0 (get-local-name socket)))

  (defmethod get-peer-address ((socket sb-bsd-sockets:socket))
    (nth-value 0 (get-peer-name socket)))

  (defmethod get-local-port ((socket sb-bsd-sockets:socket))
    (nth-value 1 (get-local-name socket)))

  (defmethod get-peer-port ((socket sb-bsd-sockets:socket))
    (nth-value 1 (get-peer-name socket))))


;; And so on, to be copied from usocket/backend/*
;; until https://github.com/edicl/drakma/issues/115 is resolved successfully…
