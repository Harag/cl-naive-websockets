(defpackage :cl-naive-websockets.wrapper
  (:use
   :common-lisp
   :cl-naive-websockets
   :cl-naive-log)
  (:documentation
   "This package exports utility functions to wrap messages
with parameters and payload in websockets packets.")
  (:export
   #:send-message
   #:nsubseq
   #:prep-parameter
   #:prepare-parameters
   #:parse-parameters
   #:prepare-payload
   #:concatenate-fragments
   #:send-parameters-payload
   #:receive-parameters-payload
   #:*secure-schemes*
   #:secure-scheme-p
   #:wrap-parameters-text-payload
   #:unwrap-parameters-text-payload
   #:wrap-parameters-binary-payload
   #:unwrap-parameters-binary-payload
   #:remove-indicator))
(in-package :cl-naive-websockets.wrapper)

(defvar *secure-schemes* '("https" "wss" "ssl"))
(defun secure-scheme-p (scheme)
  (if (find scheme  *secure-schemes* :test (function equalp))
      t nil))

#|

The protocol used over the websockets stream, in both directions:

If the payload is a vector of octets (or a vector containing only octets),
then:
    the parameters are prepended with :m-binary length-of-payload and printed
    readably (with prin1-to-string), and encoded in utf-8, then the payload
    is concatenated (actually, two binary fragments are sent).
else:
    the payload is added to the parameters as :m-payload entry, and the whole
    printed readably (with prin1-to-string), and sent as a text message.


Packets are made of two parts:

- parameters, which is a map of keys to values, each strings.

  We accept as values also symbols, integers, floats and strings
  (and other lisp types that are printable readably, and thus
  converted to strings).  When receiving a string containing an
  integer or a float, we convert it automatically back to an integer
  or a float.

- payload, which is either a unicode string that can be converted to utf-8,
  or binary data in the form of a vector of octets.



encode_parameters takes a string -> string Map, and generates a string containing a lisp s-expression of the map represented as an a-list mapping keywords to strings.

Syntax of header values:

value := '(' pair… ')' .
pair := '(' key ' . ' value ')' .
key := ':' symbol .
value := '"' ( not-backslash-or-double-quote | '\\\\' | '\\"' )* '"' .



|#

(defgeneric send-message (endpoint parameters payload))

(defun nsubseq (sequence start &optional (end nil))
  "
RETURN:  When the SEQUENCE is a vector, the SEQUENCE itself, or a dispaced
         array to the SEQUENCE.
         When the SEQUENCE is a list, it may destroy the list and reuse the
         cons cells to make the subsequence.
"
  (if (vectorp sequence)
      (if (and (zerop start) (or (null end) (= end (length sequence))))
          sequence
          (make-array (- (if end
                             (min end (length sequence))
                             (length sequence))
                         start)
                      :element-type (array-element-type sequence)
                      :displaced-to sequence
                      :displaced-index-offset start))
      (let ((result (nthcdr start sequence)))
        (when end
          ;; TODO: when end is bigger than (length sequence), this (setf cdr) fails:
          (setf (cdr (nthcdr (- end start -1) sequence)) nil))
        result)))


(defun prep-parameter (item)
  "Makes sure a parameter is a string. Does not url encode it!"
  (typecase item
    (null   nil)
    (string item)
    (symbol (symbol-name item))
    (t      (with-io-syntax (prin1-to-string item)))))

(defun prepare-parameters (parameters-plist)
  "Converts a parameter plist into an a-list of headers."
  (loop :for (key value) :on parameters-plist :by (function cddr)
        :collect (cons (prep-parameter key)
                       (prep-parameter value))))

(assert (equalp (prepare-parameters '(:content-type "application/x-lisp" :content-length 1234 :charset :utf-8))
	            '(("CONTENT-TYPE" . "application/x-lisp")
                  ("CONTENT-LENGTH" . "1234")
                  ("CHARSET" . "UTF-8"))))

(defun parse-parameters (headers-alist)
  "Converts an a-list of strings (headers) into a p-list of parameters."
  (let ((plist ()))
    (loop :for (key . value) :in headers-alist
     	  :do (push (intern (string-upcase key) "KEYWORD") plist)
              (push (let ((ival (ignore-errors (parse-integer value :radix 10. :junk-allowed nil))))
                      (or ival value))
                    plist))
    (nreverse plist)))

(assert (equalp (parse-parameters '(("Content-Type" . "application/x-lisp") ("Content-Length" . "1234") ("Charset" . "UTF-8")))
	        '(:content-type "application/x-lisp" :content-length 1234 :charset "UTF-8")))

(defun prepare-payload (payload)
  "Replace any non-keyword symbol by a string.
Check for non-printable-readably objects: we print them non-readably to a string (using PRINTABLE)."
  (labels ((prepare (object)
             (cond
               ((null object)                object)
               ((keywordp object)            object)
               ((symbolp object)             (string object))
               ((stringp object)             object)
               ((numberp object)             object)
               ((consp object)               (cons (prepare (car object)) (prepare (cdr object))))
               ((typep object 'bit-vector)   object)
               ((vectorp object)             (map 'vector (function prepare) object))
               ((hash-table-p object)        (prepare (alexandria:hash-table-plist object)))
               (t                            (printable object)))))
    (prepare payload)))

(defun concatenate-fragments (fragments)
  (let ((result (make-array (reduce (function +) fragments :key (function length))
                            :element-type 'octet)))
    (loop :for start := 0 :then (+ start (length fragment))
          :for fragment :in fragments
          :do (replace result fragment :start1 start))
    result))



(defun wrap-parameters-text-payload (parameters payload k-payload)
  "Wraps the prepared parameters and payload, using the k-payload key.

PARAMETERS are a p-list of key values (keys are keywords, values are strings).
PAYLOAD is a string.
K-PAYLOAD is a keyword.

Returns a p-list (K-PAYLOAD PAYLOAD . PARAMETERS) serialized to a string.
"
  (with-io-syntax
    (let ((*print-readably* nil))
      (prin1-to-string (list* k-payload (prepare-payload payload) parameters)))))

(defun remove-indicator (plist indicator)
  (loop :for (key value) :on plist :by (function cddr)
        :unless (eq key indicator)
        :collect key :and :collect value))

(defun unwrap-parameters-text-payload (text k-payload)
  "Return: parameters; payload"
  (let ((parameters (with-io-syntax
                      (let ((*print-readably* nil))
                        (read-from-string text)))))
    (values (remove-indicator parameters k-payload)
            (getf parameters k-payload))))

(defun wrap-parameters-binary-payload (parameters payload k-binary)
  "Wraps the prepared parameters and payload, using the k-binary key.

PARAMETERS are a p-list of key values (keys are keywords, values are strings).
PAYLOAD is a vector of octets.
K-BINARY is a keyword.

Returns a p-list (K-BINARY (length PAYLOAD) . PARAMETERS) serialized to a string.
"
  (with-io-syntax
    (let ((*print-readably* nil))
      (prin1-to-string (list* k-binary (length payload) parameters)))))

(defvar *k-binary-cache* '())

(defun get-k-binary-octets (k-binary)
  (let ((entry (assoc k-binary *k-binary-cache*)))
    (if entry
      (cdr entry)
      (let ((entry (cons k-binary (babel:string-to-octets (format nil "(~S " k-binary) :encoding :utf-8))))
        (push entry *k-binary-cache*)
        (cdr entry)))))

(defun unwrap-parameters-binary-payload (bytes k-binary k-payload)
  "Return: parameters ; payload"
  (log-message :debug 'wrapper "bytes = ~S" bytes)
  (let ((prefix (get-k-binary-octets k-binary)))
    (handler-case 
     (if (= (length prefix) (mismatch prefix bytes :test (function char-equal) :key (function code-char)))
         (let* ((length (loop :for pos := (length prefix) :then (1+ pos)
                              :with n := 0
                              :while (and (< pos (length bytes))
                                          (digit-char-p (code-char (aref bytes pos))))
                              :do (setf n (+ (* n 10) (digit-char-p (code-char (aref bytes pos)))))
                              :finally (return n)))
                (start (- (length bytes) length)))
           (values (with-io-syntax
                     (let ((*print-readably* nil))
                       (read-from-string
                        (babel:octets-to-string (subseq bytes 0 start) :encoding :utf-8))))
                   (nsubseq bytes start)))
         (let ((parameters (with-io-syntax
                             (let ((*print-readably* nil))
                               (read-from-string
                                (babel:octets-to-string bytes :encoding :utf-8))))))
           (values (remove-indicator parameters k-payload)
                   (getf parameters k-payload))))
      (error (err)
        (log-message :error 'wrapper "Invalid wrapped message ~S; got error: ~A" bytes err)
        (values (list :status :error
                      :message bytes)
                (princ-to-string err))))))

(defun send-parameters-payload (endpoint parameters payload k-binary k-payload)
  "Encode and send a packet made from PARAMETERS and PAYLOAD to the websockets ENDPOINT."
  (cond
    ((typep payload '(vector octet))
     (let ((fragment (cl-naive-websockets:send-binary-fragment
                      endpoint (babel:string-to-octets
                                (wrap-parameters-binary-payload parameters payload k-binary)
                                :encoding :utf-8)
                      :fragment nil :final nil)))
       (cl-naive-websockets:send-binary-fragment
        endpoint payload
        :fragment fragment :final t)))
    ((and (typep payload 'vector) (every (lambda (byte) (typep byte 'octet)) payload))
     (send-parameters-payload endpoint parameters (coerce payload '(vector octet)) k-binary k-payload))
    (t
     (cl-naive-websockets:send-text-message
      endpoint (wrap-parameters-text-payload parameters payload k-payload)))))

(defun receive-parameters-payload (endpoint k-status k-binary k-payload)
  "Receive a packet from the websockets ENDPOINT, and parse it.
Return: status; parameters; payload"
  (let ((service (cl-naive-websockets:endpoint-upper-layer endpoint)))
    (multiple-value-bind (response-parameters response-payload)
        (loop :named get-response
              :with fragments := '()
              :for message := (cl-naive-websockets:receive-message endpoint)
              :do (typecase message

                    (cl-naive-websockets:websockets-message-fragment
                     (push message fragments)
                     (when (eql :final (cl-naive-websockets:message-fragment-position message))
                       (return-from get-response
                         (unwrap-parameters-binary-payload (concatenate-fragments (nreverse fragments)) k-binary k-payload)))
                     #| otherwise, wait for next fragment |#)

                    (cl-naive-websockets:websockets-close-message
                     (return-from get-response
                       (let ((data (cl-naive-websockets:message-data message)))
                         (if (< 2 (length data))
                             (progn
                               (log-message :WARN service "Received a close control message ~A: ~A"
                                            (cl-naive-websockets:close-message-status message)
                                            (cl-naive-websockets:close-message-reason message))
                               (values (list k-status "closed")
                                       (format nil "Received a close control message ~A: ~A"
                                               (cl-naive-websockets:close-message-status message)
                                               (cl-naive-websockets:close-message-reason message))))
                             (progn
                               (log-message :WARN service "Received an illegal close control message")
                               (values (list k-status "closed")
                                       (format nil "Received an illegal close control message: ~A"
                                               data)))))))

                    (cl-naive-websockets:websockets-text-message
                     (return-from get-response
                       (unwrap-parameters-text-payload (cl-naive-websockets:message-string message) k-payload)))

                    (cl-naive-websockets:websockets-binary-message
                     (return-from get-response
                       (unwrap-parameters-binary-payload (cl-naive-websockets:message-data message) k-binary k-payload)))

                    ((or cl-naive-websockets:websockets-pong-message
                         cl-naive-websockets:websockets-ping-message)
                     #| try again |#)

                    (t
                     (log-message :WARN service "SHOULD NOT OCCUR: received a ~A" (type-of message))
                     (handler-case
                         (cl-naive-websockets:websockets-disconnect
                          endpoint
                          :reason "Received an unknown message type"
                          :status cl-naive-websockets:+ws-protocol-error+)
                       (error (err)
                         (log-message :error endpoint "got an error in ~S: ~A"
                                      'receive-parameters-payload err)))
                     (return-from get-response
                       (values (list k-status "closed")
                               (format nil "Received an illegal message: ~A"
                                       (cl-naive-websockets:message-data message)))))))
      (values (getf response-parameters k-status)
              response-parameters
              response-payload))))

;;;; THE END ;;;;

