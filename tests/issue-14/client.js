// Create a WebSocket object
const WebSocket = require('ws');
const socket = new WebSocket('ws://localhost:33007/getdiv'); // Also tried wss://

// When the socket connection is open
socket.onopen = function() {
    // Send a message to the server
    console.log('Sending data request');
    socket.send('getData');
};

// When a message is received from the server
socket.onmessage = function(event) {
    if (event.data.startsWith('HELLO')) {
        // The server just greeted us.
        console.log('Server greeted us: ', event.data);
    } else if ((event.data.startsWith('<div>')
                || event.data.startsWith('<div '))
               && event.data.endsWith('</div>')) {
        // Update the <div> with the received data
        console.log('OK - received a div block.');
        console.log('Received HTML: ', event.data);
        socket.send('OK');
        socket.send('close');
    } else {
        console.log('KO - did not receive a div block.');
        console.log('Received text: ', event.data);
        socket.send('KO');
        socket.send('close');
    }
};

// When the socket connection is closed
socket.onclose = function(event) {
    // Handle the socket closure
    console.log('Socket connection closed: ', event.code);
    process.exit();
};
