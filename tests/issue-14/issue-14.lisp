(in-package :cl-naive-websockets.tests.issue-14)

(defvar *server*)
(defvar *port* 33007)

(defun browse-url (url)
  ;; Open the default browser on macOSX:
  #+darwin
  (handler-case (uiop:run-program (format nil "open -u ~S" url))
    (:no-error (&rest e) (return-from browse-url)))
  ;; Open firefox where it runs:
  #-darwin
  (handler-case (uiop:run-program (format nil "firefox --new-window ~A" url))
    (:no-error (&rest e) (return-from browse-url)))
  ;; Otherwise let the user do it manually:
  (format t "Please browse to this url: ~%~S~%" url))

(defun test-websockets (&key ((:debug cl-naive-websockets:*debugging*) nil) (port *port*) (resource-name "/getdiv"))
  (let ((client-url (format nil "http://~A:~A/client/client.html" "localhost" port)))
    (setf *server* (start-server "localhost" port nil resource-name))
    (unwind-protect

         ;; TODO:  We should wrap this test so we can run it with cl-naive-tests automatically.
         ;; Note: there may be communication errors, so the server-side logs may be incomplete.
         ;; In that case, it should be a failure.
         ;; success = we get in the server logs: "Server Received OK -- Test successful."
         ;; failure = timeout without getting this log.
         (progn
           (browse-url client-url)
           (sleep 60))
      
      (stop-server *server*)
      (setf *server* nil))))

;; TODO: write this test using eg. node.js
;; (test-websockets) ; don't run the "interactive" test when running the batch test


