(in-package :cl-naive-websockets.tests.issue-14)

;; This is a simple example of a websockets server that serves a HTML DIV block.

(defparameter *this-directory* #.(make-pathname :name nil :type nil :version nil 
                                                :defaults *compile-file-truename*))

(defun format-log (control-string &rest arguments)
  (hunchentoot:acceptor-log-message hunchentoot:*acceptor* :info "~?" control-string arguments))

(defmacro scase (keyform &rest clauses)
  "
DO:         A CASE, but for string keys. That is, it uses STRING= as test
            instead of the ''being the same'' test.
"
  (let ((key (gensym "KEY")))
    `(let ((,key ,keyform))
       (cond
         ,@(mapcar (lambda (clause)
                     (if (or (eq (car clause) 'otherwise) (eq (car clause) 't))
                         `(t ,@(cdr clause))
                         `((member ,key ',(car clause) :test (function string=))
                           ,@(cdr clause))))
                   clauses)))))

(defun example-ws-server (server endpoint)
  (declare (ignore server))
  (with-debugging
    (unwind-protect
         ;; This servers expects a getdata message and responds with a html div block.
         (loop
           :initially (send-text-message endpoint "HELLO  I accept getdata message and sends back a <div> block.")
           :for message := (receive-message endpoint)
           :do (typecase message

                 (websockets-text-message
                  (format-log "Server Received a text message ~S" (message-string message))
                  (scase (message-string message)
                         (("getData")
                          (format-log "Sending <div>.")
                          (send-text-message endpoint "<div style=\"background-color: #87cdeb;\">
This is a test.<br>
If you read it, then the test is successful:<br>
the javascript code in the page correctly<br>
fetched data from the websockets server<br>
and replaced it instead of the original &lt;div&gt;.<br>
Have fun!
</div>"))
                         (("OK")
	                      (format-log "Server Received OK -- Test successful."))
                         (("KO")
                          (format-log "Server Received KO -- Test failed."))
                         (("close")
                          (ignore-errors (websockets-disconnect endpoint :reason "Client requested close" :status +ws-normal-closure+))
                          (loop-finish))
                         (t
                          (send-text-message endpoint (format nil "Server Received a text message ~S -- ignored" (message-string message))))))

                 
                 (websockets-close-message
                  (let ((data (message-data message)))
                    (if (< 2 (length data))
                        (format-log "Server Received a close control message ~A ~S"
                                    (close-message-status message)
                                    (close-message-reason message))
                        (format-log "Server Received an illegal close control message")))
                  (loop-finish))
                 
                 (websockets-message
                  (format-log "Server Received a message ~S -- ignored." message))
                 
                 (null
                  (format-log "Server Received a null message: the other side disconnected.")
                  (ignore-errors (websockets-disconnect endpoint :reason "Other side disconnected." :status +ws-protocol-error+))
                  (loop-finish))
                 
                 (t
                  (format-log "SHOULD NOT OCCUR!")
                  (format-log "Server Received something of type ~S: ~S" (type-of message) message)
                  (ignore-errors (websockets-disconnect endpoint :reason "Received an unknown message type" :status +ws-protocol-error+))
                  (loop-finish))))
      (ignore-errors (websockets-disconnect endpoint)))))


;; This example acceptor will take only one resource-name and direct
;; it to the EXAMPLE-WS-SERVER.  Other resources will defer to normal
;; hunchentoot with default processing.


(defclass example-ws-server (websockets-server)
  ()
  (:default-initargs
   :subprotocols '("issue-14")
   :server-function (function example-ws-server)))

(defclass example-resource-mixin ()
  ((resource-name :initarg :resource-name :reader resource-name)))

(defclass example-acceptor (example-resource-mixin websockets-acceptor-mixin hunchentoot:acceptor)
  ())

(defclass example-ssl-acceptor (example-resource-mixin websockets-acceptor-mixin hunchentoot:ssl-acceptor)
  ())


(defmethod hunchentoot:acceptor-access-log-destination ((acceptor example-acceptor))
  *trace-output*)

(defmethod hunchentoot:acceptor-log-access ((acceptor example-acceptor) &key return-code)
  (format *trace-output* "acceptor-log-access example-acceptor return-code = ~S~%" return-code)
  (let ((hunchentoot::*access-log-lock* (load-time-value (bt:make-lock))))
	(call-next-method)))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor example-acceptor) request)
  (hunchentoot:acceptor-log-message acceptor :info "example-acceptor script-name = ~S" (hunchentoot:script-name request))
  (scase (hunchentoot:script-name request)
         (("/client/client.html")
          (setf (hunchentoot:content-type*) "text/html")
		  (hunchentoot:acceptor-log-message acceptor :info "Will serve ~S" "/client/client.html")
          (prog1
              "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"
<html>
  <head>
    <title>Javascript Websocket Client Testing Issue #14</title>
    <script src=\"client.js\"></script>
  </head>
  <body>
    <h1>Javascript Websocket Client Testing Issue #14</h1>
    <p>Open the console to see the output.</p>

    <p>The div obtained from the websockets server is:
      <div id=\"myDiv\"  style=\"background-color: #fe2500;\">
If you read it, then the test has failed:<br>
the javascript code in the page couldn't<br>
fetch the data from the websockets server<br>
and replaced it instead of the original &lt;div&gt;.<br>
More work is needed!
</div>
    </p>
  </body>
</html>"
            (hunchentoot:acceptor-log-message acceptor :info "Did serve ~S" "/client/client.html")))

         (("/client/client.js")
          (setf (hunchentoot:content-type*) "application/javascript")
          (hunchentoot:acceptor-log-message acceptor :info "Will serve ~S" "/client/client.js")
          (prog1
              "// Create a WebSocket object
const socket = new WebSocket('ws://localhost:33007/getdiv'); // Also tried wss://

// When the socket connection is open
socket.onopen = function() {
    // Send a message to the server
    console.log('Sending data request');
    socket.send('getData');
};

// When a message is received from the server
socket.onmessage = function(event) {
    const div = document.getElementById('myDiv');
    if (event.data.startsWith('HELLO')) {
        // The server just greeted us.
        div.innerText = event.data;
        console.log('Server greeted us - ', event.data);
    } else if ((event.data.startsWith('<div>')
                || event.data.startsWith('<div '))
               && event.data.endsWith('</div>')) {
        // Update the <div> with the received data
        div.innerHTML = event.data;
        console.log('OK - received a div block');
        socket.send('OK');
    } else {
        div.innerText = 'KO - did not receive a div block, but: ' + event.data;
        console.log('KO - did not receive a div block, but: ', event.data);
        socket.send('KO');
    }
};

// When the socket connection is closed
socket.onclose = function(event) {
    // Handle the socket closure
    console.log('Socket connection closed: ', event.code);
};
"
            (hunchentoot:acceptor-log-message acceptor :info "Did serve ~S" "/client/client.js")))
         (otherwise
          (hunchentoot:acceptor-log-message acceptor :info "Will serve default")
          (prog1 (when (next-method-p)
                   (call-next-method))
            (hunchentoot:acceptor-log-message acceptor :info "Did serve default")))))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor example-resource-mixin) request)
  (hunchentoot:acceptor-log-message acceptor :info "example-resource-mixin script-name = ~S ; resource-name = ~S"
                                    (hunchentoot:script-name request)
                                    (resource-name acceptor))
  (if (and (string= (hunchentoot:script-name request) (resource-name acceptor))
           (accept-websockets-negociation acceptor request))
      ;; It was a websocket request:
      (values "" nil nil)
      ;; Otherwise, defer to the superclass.
      (when (next-method-p)
        (call-next-method))))

(defun start-server (host port secure resource-name)
  (let* ((server   (make-instance 'example-ws-server))
         (acceptor (make-instance (if secure
                                      'example-ssl-acceptor
                                      'example-acceptor)
                                  :port port
                                  :address host
                                  :resource-name resource-name
                                  :access-log-destination  *error-output*
                                  :message-log-destination *error-output*
                                  :websockets-server server)))    
    (hunchentoot:start acceptor)))

(defun stop-server (server)
  (hunchentoot:stop server))

