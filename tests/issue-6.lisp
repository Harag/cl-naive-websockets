(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload "cl-naive-websockets")
  (ql:quickload "cl-naive-websockets.example.server.hunchentoot")
  (ql:quickload "cl-naive-websockets.example.client.drakma"))

(defpackage :cl-naive-websockets.test.issue-6
  (:use :cl :cl-naive-websockets))
(in-package :cl-naive-websockets.test.issue-6)

(defvar *server*)
(defun test-websockets (&key ((:debug cl-naive-websockets:*debugging*) nil) (port 8813) (resource-name "/test"))
  (setf *server* (cl-naive-websockets.example.server.hunchentoot::start-server "localhost" port nil resource-name))
  (unwind-protect
       (cl-naive-websockets.example.client.drakma:ws-chat-client "localhost" port resource-name)
    (cl-naive-websockets.example.server.hunchentoot::stop-server *server*)
    (setf *server* nil)))


(defclass example-resource-mixin ()
  ((resource-name :initarg :resource-name :reader resource-name)))


(defmethod hunchentoot:acceptor-dispatch-request ((acceptor example-resource-mixin) request)
  (hunchentoot:acceptor-log-message acceptor :warn
                                    "script-name   = ~S~%resource-name = ~S~%"
                                    (hunchentoot:script-name  request) (resource-name acceptor))
  (if (and (string= (hunchentoot:script-name request) (resource-name acceptor))
           (accept-websockets-negociation acceptor request))
      ;; It was a websocket request:
      (values "" nil nil)
      ;; Otherwise, defer to the superclass.
      (when (next-method-p)
        (call-next-method))))

(test-websockets)
