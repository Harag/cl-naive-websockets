(in-package :cl-naive-websockets.tests)

;;;
;;; layer-pair
;;;

(defstruct layer-pair
  client
  server)

;;;
;;; chunk
;;;

(defstruct chunk
  data
  start
  end)

(defun chunk-length (chunk)
  (- (chunk-end chunk) (chunk-start chunk)))

;;;
;;; null-modem-layer
;;;

(defclass null-modem-layer ()
  ((lock          :initform (bt:make-lock "null-modem-layer lock")
                  :reader   null-modem-layer-lock)
   (queue-full    :initform (bt:make-condition-variable :name "null-modem-layer receive-queue full")
                  :accessor null-modem-layer-queue-full)
   (receive-queue :initform (cl-naive-websockets::make-queue)
                  :reader   null-modem-layer-receive-queue)
   (peer          :initform nil
                  :accessor null-modem-layer-peer)))


(defmethod connect        ((layer null-modem-layer) host port secure resource-name headers)
  (declare (ignore host port secure resource-name headers))
  (error "~S layer cannot ~S" layer 'connect)) ; for client (layer null-modem-layer)s

(defmethod accept         ((layer null-modem-layer) host port secure)
  (declare (ignore host port secure))
  (error "~S layer cannot ~S" layer 'accept)) ; for server (layer null-modem-layer)s

(defmethod disconnect     ((layer null-modem-layer))
  (error "~S layer cannot ~S" layer 'disconnect))


;;;
;;; take-octets is called by the peer to give us some buffer data.
;;;

(defgeneric take-octets (layer data))

(defmethod take-octets ((layer null-modem-layer) data)
  (bt:with-lock-held ((null-modem-layer-lock layer))
    (cl-naive-websockets::enqueue (null-modem-layer-receive-queue layer)
                                 (make-chunk :data data :start 0 :end (length data)))
    (bt:condition-notify (null-modem-layer-queue-full layer))))

(defmethod flush          ((layer null-modem-layer))
  (values))

(defmethod send-octet     ((layer null-modem-layer) byte)
  (send-octets layer (map-into (cl-naive-websockets::make-buffer 1) (constantly byte))))

(defmethod send-octets    ((layer null-modem-layer) buffer &key (start 0) end)
  (let ((peer (null-modem-layer-peer layer)))
    (if peer
        (take-octets peer (if (and (zerop start)
                                    (or (null end)
                                        (= end (length buffer))))
                               (copy-seq buffer)
                               (subseq buffer start (or end (length buffer)))))
        (error "~S is not connected to a peer, cannot ~S"
               layer 'send-octets))))

(defmethod receive-octet  ((layer null-modem-layer)) ; -> octet
  (let ((buffer (cl-naive-websockets::make-buffer 1)))
    (unless (= 1 (receive-octets layer buffer))
      (error "~S cannot receive 1 octet" layer))
    (aref buffer 0)))

(defmethod receive-octets ((layer null-modem-layer) buffer &key (start 0) (end (length buffer)))
  (let* ((length-to-read (- (or end (length buffer)) start))
         (remaining-length length-to-read))
    (bt:with-lock-held ((null-modem-layer-lock layer))
      (loop
        :while (plusp remaining-length)
        :do (loop
              :for head := (cl-naive-websockets::queue-head (null-modem-layer-receive-queue layer))
              :while (null head)
              :do (bt:condition-wait (null-modem-layer-queue-full layer)
                                     (null-modem-layer-lock layer))
              :finally
                 (let ((head-length (chunk-length head)))
                   (if (<= head-length remaining-length)
                       (progn
                         (cl-naive-websockets::dequeue (null-modem-layer-receive-queue layer))
                         (replace buffer (chunk-data head)
                                  :start1 start
                                  :start2 (chunk-start head) :end2 (chunk-end head))
                         (incf start head-length)
                         (decf remaining-length head-length))
                       (progn
                         (replace buffer (chunk-data head)
                                  :start1 start :end1 (+ start remaining-length)
                                  :start2 (chunk-start head))
                         (incf (chunk-start head) remaining-length)
                         (setf remaining-length 0)))))))
    length-to-read))



(defun setup-null-modem-layer ()
  (let ((client (make-instance 'null-modem-layer))
        (server (make-instance 'null-modem-layer)))
    (setf (null-modem-layer-peer client) server
          (null-modem-layer-peer server) client)
    (make-layer-pair :client client :server server)))

(defun teardown-null-modem-layer (layer-pair)
  (setf (null-modem-layer-peer (layer-pair-server layer-pair)) nil
        (null-modem-layer-peer (layer-pair-client layer-pair)) nil
        (layer-pair-server layer-pair) nil
        (layer-pair-client layer-pair) nil))

(defun call-with-layer-pair (thunk)
  (let ((layer-pair (setup-null-modem-layer)))
    (unwind-protect
         (funcall thunk
                  (layer-pair-client layer-pair)
                  (layer-pair-server layer-pair))
      (teardown-null-modem-layer layer-pair))))

(defmacro with-layer-pair ((client server) &body body)
  `(call-with-layer-pair (lambda (,client ,server) ,@body)))
