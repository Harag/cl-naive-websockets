(in-package :cl-naive-websockets.tests)

(defvar *port*            13333)

(defvar *server*          nil)
(defvar *site*            nil)
(defvar *resource-simple* nil)


(testsuite
    :test/compose-headers

  (testcase #:g4481
            :expected 'nil
            :actual (cl-naive-websockets::compose-headers))
  (testcase #:g4482
            :expected '(("foo" . "42"))
            :actual (cl-naive-websockets::compose-headers "foo" "42"))
  (testcase #:g4483
            :expected '(("bar" . "33") ("foo" . "42"))
            :actual (cl-naive-websockets::compose-headers "foo" "42" "bar" "33"))
  (testcase #:g4484
            :expected '(("quux" . "133") ("baz" . "142"))
            :actual (cl-naive-websockets::compose-headers '("baz" "142" "quux" "133")))
  (testcase #:g4485
            :expected '(("quux" . "133") ("baz" . "142") ("foo" . "42"))
            :actual (cl-naive-websockets::compose-headers "foo" "42" '("baz" "142" "quux" "133")))
  (testcase #:g4486
            :expected '(("quux" . "133") ("baz" . "142") ("bar" . "33") ("foo" . "42"))
            :actual (cl-naive-websockets::compose-headers "foo" "42" "bar" "33" '("baz" "142" "quux" "133"))))


(defun websocket-resource/simple (resource &optional client &key event)
  (declare (ignorable resource client event))
  (format *trace-output* "// websocket-resource/simple resource = ~S~%" resource)
  (format *trace-output* "// websocket-resource/simple client   = ~S~%" client)
  (format *trace-output* "// websocket-resource/simple event    = ~S~%" event)
  "Yeeeha she is a live and kicking!!")

(defun setup-websockets-test-server (&key (port *port*))
  (setf *server* (make-instance 'cl-naive-webserver.hunchentoot::hunchentoot-server
                                :id "test-websockets"
                                :port port
                                :websockets-p t))
  (register-server *server*)
  (start-server *server*)

  (setf *site* (register-site (make-instance 'site
                                             :url "/ws-test")))

  (setf *resource-simple* (make-instance 'websocket-resource
                                         :url "/simple"
                                         :websocket-p t
                                         :func 'websocket-resource/simple))
  (register-resource *site* *resource-simple*)
  *server*)


(defun teardown-websockets-test-server ()
  (stop-server *server*)
  ;; How can we do that: (unregister-resource *site* *resource-simple*)
  (setf *resource-simple* nil)
  ;; How can we do that: (unregister-site *site*)
  (setf *site* nil)
  ;; How can we do that: (unregister-server *server*)
  (setf *server* nil))


(testsuite
    :websockets-simple

  (unwind-protect
       (progn

         (setup-websockets-test-server :port *port*)

         (testcase :simple-http-connection
                   :expected 200
                   :actual (let ((url (format nil "http://localhost:~A/ws-test/simple" *port*)))
                             (setf (sessions-p *site*) nil)
                             (pprint (list :server *server*
                                           :site  *site*
                                           :resource *resource-simple*))
                             (print (list :debug *debug*))
                             (multiple-value-bind (output code header)
                                 (drakma:http-request url :method :get)
                               (declare (ignore output))

                               (print url)
                               (print code)
                               (map nil 'print header)
                               (terpri)
                               (finish-output)
                               (values code header)))

                   :info "Simple HTTP Connection.")

         (testcase :simple-websockets-connection
                   :expected t
                   :actual (let ((host          "localhost")
                                 (port          *port*)
                                 (resource-name "ws-test/simple")
                                 (secure        nil)
                                 (subprotocols  '("test"))
                                 (extensions    '())
                                 (origin        nil)
                                 (additionnal-http-headers '())
                                 (http-client-connect-function (function cl-naive-webserver::drakma-http-client-connect)))

                             (setf (sessions-p *site*) nil)
                             (print (list :debug *debug*))
                             (pprint (list :server *server*
                                           :site  *site*
                                           :resource *resource-simple*))
                             (multiple-value-bind (stream extensions subprotocol cookies)
                                 (websockets-connect host resource-name
                                                    :port port :secure secure
                                                    :subprotocols subprotocols :extensions extensions
                                                    :origin origin
                                                    :additionnal-http-headers additionnal-http-headers
                                                    :http-client-connect-function http-client-connect-function)
                               (pprint (list :stream stream
                                             :extensions extensions
                                             :subprotocol subprotocol
                                             :cookies cookies))
                               (terpri)
                               (finish-output)
                               (when (streamp stream)
                                 (close stream)
                                 t)))

                   :info "Simple WebSockets Connection."))

    (teardown-websockets-test-server)))

;;;; THE END ;;;;
