(in-package :cl-naive-websockets.tests)

(defun peek-uint8  (buffer base)
  (aref buffer base))

(defun peek-uint16 (buffer base)
  (loop :with value := 0
        :repeat 2
        :for index :from base
        :for position :from 8 :downto 0 :by 8
        :do (setf value (dpb (aref buffer index) (byte 8 position) value))
        :finally (return value)))

(defun peek-uint32 (buffer base)
  (loop :with value := 0
        :repeat 4
        :for index :from base
        :for position :from 24 :downto 0 :by 8
        :do (setf value (dpb (aref buffer index) (byte 8 position) value))
        :finally (return value)))

(defun peek-uint64 (buffer base)
  (loop :with value := 0
        :repeat 8
        :for index :from base
        :for position :from 56 :downto 0 :by 8
        :do (setf value (dpb (aref buffer index) (byte 8 position) value))
        :finally (return value)))

(defun dump (buffer &key (start 0) end (byte-size 1) (stream *standard-output*) (margin ""))
  (let ((peek (case byte-size
                ((1) (function peek-uint8))
                ((2) (function peek-uint16))
                ((4) (function peek-uint32))
                ((8) (function peek-uint64))
                (otherwise (error "BYTE-SIZE must be either 1, 2, 4 or 8."))))
        (length (- (or end (length buffer)) start)))
    (do ((address start (+ byte-size address))
         (i       0     (+ byte-size i)))
        ((>= i length) (format stream "~&") (values))
      (when (zerop (mod i 16))
        (format stream "~&~A~8,'0X: " margin address))
      (format stream "~V,'0X " (* 2 byte-size) (funcall peek buffer address)))))



#|

.0                   1                   2                   3
.0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-------+-+-------------+-------------------------------+
|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
|N|V|V|V|       |S|             |   (if payload len==126/127)   |
| |1|2|3|       |K|             |                               |
+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
|     Extended payload length continued, if payload len == 127  |
+ - - - - - - - - - - - - - - - +-------------------------------+
|                               |Masking-key, if MASK set to 1  |
+-------------------------------+-------------------------------+
| Masking-key (continued)       |          Payload Data         |
+-------------------------------- - - - - - - - - - - - - - - - +
:                     Payload Data continued ...                :
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|                     Payload Data continued ...                |
+---------------------------------------------------------------+

|#


(defun store-16-bit-big-endian (vector base value)
  (loop :repeat 2
        :for index :from base
        :for position :from 8 :downto 0 :by 8
        :do (setf (aref vector index) (ldb (byte 8 position) value))
        :finally (return value)))

(defun load-16-bit-big-endian (vector base)
  (loop :with value := 0
        :repeat 2
        :for index :from base
        :for position :from 8 :downto 0 :by 8
        :do (setf value (dpb (aref vector index) (byte 8 position) value))
        :finally (return value)))

(defun store-64-bit-big-endian (vector base value)
  (loop :repeat 8
        :for index :from base
        :for position :from 54 :downto 0 :by 8
        :do (setf (aref vector index) (ldb (byte 8 position) value))
        :finally (return value)))

(defun load-64-bit-big-endian (vector base)
  (loop :with value := 0
        :repeat 8
        :for index :from base
        :for position :from 54 :downto 0 :by 8
        :do (setf value (dpb (aref vector index) (byte 8 position) value))
        :finally (return value)))


(defgeneric compute-frame-length (frame))
(defmethod compute-frame-length ((frame cl-naive-websockets::frame))
  (let* ((mlen   (if (cl-naive-websockets::frame-masking-key frame) 4 0))
         (len    (cl-naive-websockets::frame-payload-length frame))
         (plsize (cond ((<= len 125)   0)
                       ((< len 65536)  2)
                       (t              8))))
    (+ 2 plsize mlen len)))

(defun encode-frame (fin rsv1 rsv2 rsv3 opcode masking-key payload)
  (check-type fin         (or boolean bit))
  (check-type rsv1        (or boolean bit))
  (check-type rsv2        (or boolean bit))
  (check-type rsv3        (or boolean bit))
  (check-type opcode      (integer 0 15))
  (check-type masking-key (or null vector))
  (check-type payload     (or null vector))
  (flet ((asbit (x) (case x
                      ((0 nil) 0)
                      ((1 t)   1))))
    (let* ((mlen   (if masking-key 4 0))
           (len    (length payload))
           (plsize 0)
           (pllen  0))
      (cond ((<= len 125)
             (setf plsize 0
                   pllen  len))
            ((< len 65536)
             (setf plsize 2
                   pllen  126))
            (t
             (setf plsize 8
                   pllen  127)))
      (let* ((fin   (asbit fin))
             (rsv1  (asbit rsv1))
             (rsv2  (asbit rsv2))
             (rsv3  (asbit rsv3))
             (size  (+ 2 plsize mlen len))
             (frame (cl-naive-websockets::make-buffer size)))
        (setf (aref frame 0) (dpb fin (byte 1 7)
                                  (dpb rsv1 (byte 1 6)
                                       (dpb rsv2 (byte 1 5)
                                            (dpb rsv3 (byte 1 4)
                                                 opcode))))
              (aref frame 1) (dpb (if masking-key 1 0) (byte 1 7) pllen))
        (case plsize
          (2 (store-16-bit-big-endian frame 2 len))
          (8 (store-64-bit-big-endian frame 2 len)))
        (replace frame payload :start1 (+ 2 plsize mlen))
        (when masking-key
          (replace frame masking-key :start1 (+ 2 plsize) :end2 4)
          (cl-naive-websockets::mask-unmask frame masking-key :start (+ 2 plsize mlen)))
        frame))))

(defun decode-frame (frame)
  (check-type frame vector)
  (assert (<= 2 (length frame)))
  (let ((fin    (ldb (byte 1 7) (aref frame 0)))
        (rsv1   (ldb (byte 1 6) (aref frame 0)))
        (rsv2   (ldb (byte 1 5) (aref frame 0)))
        (rsv3   (ldb (byte 1 4) (aref frame 0)))
        (opcode (ldb (byte 4 0) (aref frame 0)))
        (mask   (ldb (byte 1 7) (aref frame 1)))
        (pllen  (ldb (byte 7 0) (aref frame 1)))
        (plsize 0)
        (mlen   0)
        (len    0)
        (masking-key nil)
        (payload     nil))
    (case pllen
      ((126)        (setf len    (load-16-bit-big-endian frame 2)
                          plsize 2))
      ((127)        (setf len    (load-64-bit-big-endian frame 2)
                          plsize 8))
      (otherwise    (setf len    pllen
                          plsize 0)))
    (when (plusp mask)
      (setf masking-key (cl-naive-websockets::make-buffer 4)
            mlen 4)
      (replace masking-key frame :start2 (+ 2 plsize)))
    (when (plusp len)
      (setf payload (cl-naive-websockets::make-buffer len))
      (replace payload frame :start2 (+ 2 plsize mlen))
      (when masking-key
        (cl-naive-websockets::mask-unmask payload masking-key)))
    (list fin rsv1 rsv2 rsv3 opcode masking-key payload)))



(defvar *test-big-frames* nil)

(defparameter *frame-test-data*
  (remove nil
          (let* ((smid0 (trivial-utf-8:string-to-utf-8-bytes
                         (coerce (loop :for i :below 1024
                                       :collect (code-char i))
                                 'string)))
                 (smid1 (trivial-utf-8:string-to-utf-8-bytes
                         (coerce (loop :for i :from 1024 :below 2048
                                       :collect (code-char i))
                                 'string)))
                 (smid2 (trivial-utf-8:string-to-utf-8-bytes
                         (coerce (loop :for i :from 2048 :below 3072
                                       :collect (code-char i))
                                 'string)))
                 (sbig0 (when *test-big-frames*
                          (trivial-utf-8:string-to-utf-8-bytes
                           (coerce (loop :for i :below 80000
                                         :collect (code-char i))
                                   'string)))))
            (list

             ;; small data frames

             (list 0 0 0 0 cl-naive-websockets::+text-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes "Hello"))
             (list 0 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes " World"))
             (list 1 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes "!"))

             (list 1 0 0 0 cl-naive-websockets::+text-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes "Hello World!"))


             (list 0 0 0 0 cl-naive-websockets::+binary-frame+
                   nil #(1 2 3 4 5))
             (list 0 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil #(6 7 8 9 10))
             (list 1 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil #(11 12 13 14 15))

             (list 1 0 0 0 cl-naive-websockets::+binary-frame+
                   nil #(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))

             ;; control frames

             (list 1 0 0 0 cl-naive-websockets::+close-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes
                        "Good bye, cruel world!"))

             (list 1 0 0 0 cl-naive-websockets::+ping-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes
                        "Ping"))

             (list 1 0 0 0 cl-naive-websockets::+pong-frame+
                   nil (trivial-utf-8:string-to-utf-8-bytes
                        "Pong"))


             ;; big data frames

             (list 0 0 0 0 cl-naive-websockets::+text-frame+
                   nil smid0)
             (list 0 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil smid1)
             (list 1 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil smid2)

             (when *test-big-frames*
               (list 1 0 0 0 cl-naive-websockets::+text-frame+
                     nil sbig0))


             (list 0 0 0 0 cl-naive-websockets::+binary-frame+
                   nil smid0)
             (list 0 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil smid1)
             (list 1 0 0 0 cl-naive-websockets::+continuation-frame+
                   nil smid2)

             (when *test-big-frames*
               (list 1 0 0 0 cl-naive-websockets::+binary-frame+
                     nil sbig0))


             ))))



(defun valid-frames/encode-send=read (test)
  (with-layer-pair (client server)
    (let ((original (map 'list (lambda (x)
                                 (typecase x
                                   (vector (copy-seq x))
                                   (list   (copy-seq x))
                                   (t      x)))
                         test))
          (buffer (apply (function encode-frame) test)))
      (assert (equalp original test))
      (when *debug*
        (format t "Frame: ~S~%" buffer)
        (dump buffer)
        (dump buffer :stream cl-user::*out*)
        (finish-output cl-user::*out*))
      (send-octets client buffer)
      (format t "~S = ~S~%" '*debug* *debug*)
      (handler-case (cl-naive-websockets::read-frame server)
        (:no-error (frame)
          (cl-naive-websockets::read-frame-data server frame)
          (list (if (cl-naive-websockets::frame-finp frame) 1 0)
                0 0 0
                (cl-naive-websockets::frame-opcode frame)
                (cl-naive-websockets::frame-masking-key frame)
                (cl-naive-websockets::frame-data frame)))
        (websockets-error (err)
          (format *error-output* "~&READ-FRAME signaled: ~A~%" err)
          err)))))

#-(and)
(progn

  (dump (encode-frame 0 0 0 0 cl-naive-websockets::+text-frame+
                      nil (trivial-utf-8:string-to-utf-8-bytes "Hello")) )
  00000000: 01 05 48 65 6C 6C 6F


  (with-layer-pair (s r)
    (let* ((out (encode-frame 0 0 0 0 cl-naive-websockets::+text-frame+
                              nil (trivial-utf-8:string-to-utf-8-bytes "Hello")))
           (len (length out))
           (in  (cl-naive-websockets::make-buffer len)))
      (send-octets s out)
      (dump out)
      (list (receive-octets r in) in)
      (dump in)))
  00000000: 01 05 48 65 6C 6C 6F
  00000000: 01 05 48 65 6C 6C 6F


  (with-layer-pair (s r)
    (let* ((test (list 0 0 0 0 cl-naive-websockets::+text-frame+
                       nil (trivial-utf-8:string-to-utf-8-bytes "Hello")))
           (out (apply (function encode-frame) test))
           (len (length out))
           (in  (cl-naive-websockets::make-buffer len)))
      (send-octets s out)
      (dump out)
      (print test) (terpri)
      (let ((frame (cl-naive-websockets::read-frame r)))
        (cl-naive-websockets::read-frame-data r frame)
        (list (if (cl-naive-websockets::frame-finp frame) 1 0)
              0 0 0
              (cl-naive-websockets::frame-opcode frame)
              (cl-naive-websockets::frame-masking-key frame)
              (cl-naive-websockets::frame-data frame)))))
  )


(defun valid-frames/write=receive-decode (test)
  (with-layer-pair (client server)
    (destructuring-bind  (fin rsv1 rsv2 rsv3 opcode masking-key payload) test
      (assert (zerop rsv1))
      (assert (zerop rsv2))
      (assert (zerop rsv3))
      (assert (<= 0 opcode 15))
      (let* ((frame (make-instance 'cl-naive-websockets::frame
                                   :finp (plusp fin)
                                   :opcode opcode
                                   :masking-key masking-key
                                   :data (copy-seq payload)
                                   :payload-length (length payload)))
             (expected-length  (compute-frame-length frame))
             (buffer           (cl-naive-websockets::make-buffer expected-length)))
        (cl-naive-websockets::write-frame client frame)
        (let ((received-length  (receive-octets server buffer)))
          (unless (= expected-length received-length)
            (error "Received short frame length = ~S, expected ~S"
                   received-length expected-length))
          (when *debug*
            (format t "Frame: ~S~%" buffer)
            (dump buffer)
            (dump buffer :stream cl-user::*out*)
            (finish-output cl-user::*out*))
          (decode-frame buffer))))))


(testsuite frames

  (dolist (test *frame-test-data*)
    (testcase encode-decode-valid-frames
              :equal 'equalp
              :expected test
              :actual   (decode-frame (apply (function encode-frame) test))
              :info     (format nil "(decode-frame (encode-frame ~{~S~^ ~}))" test)))

  (dolist (test *frame-test-data*)
    (dolist (masked '(nil t))
      (let ((masking-key (when masked (map-into (make-array 4 :element-type 'octet) (lambda () (random 256))))))
        (destructuring-bind  (fin rsv1 rsv2 rsv3 opcode _masking-key payload) test
          (declare (ignore _masking-key))
          (let ((test (list fin rsv1 rsv2 rsv3 opcode masking-key payload)))
            (testcase valid-frames/encode-send=read
                      :equal 'equalp
                      :expected test
                      :actual   (valid-frames/encode-send=read test)
                      :info     (format nil "valid ~@[/ masked ~*~]/ encode / send / read ~S"
                                        masking-key (list test)))
            (testcase valid-frames/write=receive-decode
                      :equal 'equalp
                      :expected test
                      :actual   (valid-frames/write=receive-decode test)
                      :info     (format nil "valid ~@[/ masked ~*~]/ write / receive / decode ~S"
                                        masking-key (list test))))))))

  ;; (let ((layer-pair (setup-null-modem-layer)))
  ;;   (unwind-protect
  ;;        (progn
  ;;          )
  ;;     (teardown-null-modem-layer layer-pair)))
  )
