(in-package :cl-naive-websockets.tests)

(testsuite null-modem-layer

  (testcase null-modem-one-octet
            :expected 42
            :actual (with-layer-pair (client server)
                      (send-octet client 42)
                      (receive-octet server)))

  (testcase null-modem-one-octet-several-times
            :expected '(1 2 3 4)
            :actual (with-layer-pair (client server)
                      (send-octet client 1)
                      (send-octet client 2)
                      (send-octet client 3)
                      (send-octet client 4)
                      (list (receive-octet server)
                            (receive-octet server)
                            (receive-octet server)
                            (receive-octet server))))

  (testcase null-modem-one-octets
            :equal 'equalp
            :expected '(4 #(1 2 3 4))
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3 4))
                      (let ((buffer (cl-naive-websockets::make-buffer 4)))
                        (list (receive-octets server buffer)
                              buffer))))

  (testcase null-modem-one-octets-several-times
            :equal 'equalp
            :expected '((3 4 9 3)
                        (#(1 2 3)
                         #(10 20 30 40)
                         #(1 2 3 4 5 6 7 8 9)
                         #(10 20 3)))
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3))
                      (send-octets client #(10 20 30 40))
                      (send-octets client #(1 2 3 4 5 6 7 8 9))
                      (send-octets client #(10 20 3))
                      (let ((bufs (list (cl-naive-websockets::make-buffer 3)
                                        (cl-naive-websockets::make-buffer 4)
                                        (cl-naive-websockets::make-buffer 9)
                                        (cl-naive-websockets::make-buffer 3))))
                        (list
                         (mapcar (lambda (buffer)
                                   (receive-octets server buffer))
                                 bufs)
                         bufs))))

  (testcase null-modem-one-octet-several-times-to-one-octets
            :equal 'equalp
            :expected '(4 #(1 2 3 4))
            :actual (with-layer-pair (client server)
                      (send-octet client 1)
                      (send-octet client 2)
                      (send-octet client 3)
                      (send-octet client 4)
                      (let ((buffer (cl-naive-websockets::make-buffer 4)))
                        (list (receive-octets server buffer)
                              buffer))))

  (testcase null-modem-one-octets-several-times-to-one-octet-several-times
            :equal 'equalp
            :expected '(1 2 3 4 5 10 20 30 40 50 1 2 3 10 20 30)
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3 4 5))
                      (send-octets client #(10 20 30 40 50))
                      (send-octets client #(1 2 3))
                      (send-octets client #(10 20 30))
                      (loop :repeat (+ 5 5 3 3)
                            :collect (receive-octet server))))

  (testcase null-modem-one-octets-several-times-to-one-octets-several-times
            :equal 'equalp
            :expected '((8 8)
                        (#(1 2 3 4 5 10 20 30)
                         #(40 50 1 2 3 10 20 30)))
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3 4 5))
                      (send-octets client #(10 20 30 40 50))
                      (send-octets client #(1 2 3))
                      (send-octets client #(10 20 30))
                      (let ((bufs (list (cl-naive-websockets::make-buffer 8)
                                        (cl-naive-websockets::make-buffer 8))))
                        (list
                         (mapcar (lambda (buffer)
                                   (receive-octets server buffer))
                                 bufs)
                         bufs))))


  (testcase null-modem-one-small-in-big
            :equal 'equalp
            :expected '((8 4)
                        (#(1 2 3 10 20 30 4 5)
                         #(6 40 50 60)))
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3))
                      (send-octets client #(10 20 30))
                      (send-octets client #(4 5 6))
                      (send-octets client #(40 50 60))
                      (let ((bufs (list (cl-naive-websockets::make-buffer 8)
                                        (cl-naive-websockets::make-buffer 4))))
                        (list
                         (mapcar (lambda (buffer)
                                   (receive-octets server buffer))
                                 bufs)
                         bufs))))

  (testcase null-modem-one-big-in-small
            :equal 'equalp
            :expected '((2 2 2 2 2
                         2 2 2 2 2
                         2 2 2 2)
                        (#(1 2) #(3 4) #(5 6) #(7 8) #(9 10)
                         #(20 30) #(40 50) #(60 70) #(80 90)
                         #(11 22) #(33 44) #(55 66) #(77 88) #(99 00)))
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3 4 5 6 7 8 9))
                      (send-octets client #(10 20 30 40 50 60 70 80 90))
                      (send-octets client #(11 22 33 44 55 66 77 88 99 00))
                      (let ((bufs (loop
                                    :repeat 14
                                    :collect (cl-naive-websockets::make-buffer 2))))
                        (list
                         (mapcar (lambda (buffer)
                                   (receive-octets server buffer))
                                 bufs)
                         bufs))))

  (testcase null-modem-one-bi-di
            :equal 'equalp
            :expected '((9 9)
                        (#(1 2 3 4 5 6 7 8 9)
                         #(10 20 30 40 50 60 70 80 90)))
            :actual (with-layer-pair (client server)
                      (send-octets client #(1 2 3 4 5 6 7 8 9))
                      (send-octets server #(10 20 30 40 50 60 70 80 90))
                      (let ((bufs (loop
                                    :repeat 2
                                    :collect (cl-naive-websockets::make-buffer 9))))
                        (list
                         (mapcar (lambda (buffer endpoint)
                                   (receive-octets endpoint buffer))
                                 bufs (list server client))
                         bufs)))))

;; TODO: test also null-modem-layer with multiple threads in parallel.
