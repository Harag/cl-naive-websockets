(in-package :cl-naive-websockets.tests)

(testsuite cl-naive-websockets/queue

  (testcase queue-empty
            :expected nil
            :actual (cl-naive-websockets::queue-head (cl-naive-websockets::make-queue)))


  (testcase queue-not-empty
            :expected nil
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (not (cl-naive-websockets::queue-head q))))

  (testcase queue-head-one
            :expected 1
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (cl-naive-websockets::queue-head q)))

  (testcase queue-dequeue-one
            :expected 1
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (cl-naive-websockets::dequeue q)))

  (testcase queue-head-several
            :expected 1
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (cl-naive-websockets::enqueue q 2)
                      (cl-naive-websockets::enqueue q 3)
                      (cl-naive-websockets::queue-head q)))

  (testcase queue-dequeue-several
            :expected '(1 2 3)
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (cl-naive-websockets::enqueue q 2)
                      (cl-naive-websockets::enqueue q 3)
                      (list (cl-naive-websockets::dequeue q)
                            (cl-naive-websockets::dequeue q)
                            (cl-naive-websockets::dequeue q))))

  (testcase queue-dequeue-empty
            :expected nil
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (cl-naive-websockets::dequeue q)
                      (cl-naive-websockets::queue-head q)))


  (testcase queue-reduce
            :expected '(10 0 30 0 (4 3 2 1))
            :actual (let ((q (cl-naive-websockets::make-queue)))
                      (cl-naive-websockets::enqueue q 1)
                      (cl-naive-websockets::enqueue q 2)
                      (cl-naive-websockets::enqueue q 3)
                      (cl-naive-websockets::enqueue q 4)
                      (list (cl-naive-websockets::queue-reduce '+ q)
                            (cl-naive-websockets::queue-reduce '+ q :initial-value -10)
                            (cl-naive-websockets::queue-reduce '+ q :key (lambda (x) (* x x)))
                            (cl-naive-websockets::queue-reduce '+ q :initial-value -30 :key (lambda (x) (* x x)))
                            (cl-naive-websockets::queue-reduce (lambda (a e) (cons e a)) q :initial-value '())))))
