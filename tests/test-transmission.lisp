(in-package :cl-naive-websockets.tests)

(defun receive-queue-lengths (endpoint)
  (list (cl-naive-websockets::queue-length
         (cl-naive-websockets::endpoint-receive-fragment-queue endpoint))))

(defun enqueue-close-message (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "Close!"))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+close-frame+
                                  :data data
                                  :finp t
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))

(defun enqueue-ping-message (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "Ping!"))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+ping-frame+
                                  :data data
                                  :finp t
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))

(defun enqueue-pong-message (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "Pong!"))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+pong-frame+
                                  :data data
                                  :finp t
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))

(defun enqueue-text-message (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "Hello World!"))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+text-frame+
                                  :data data
                                  :finp t
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))

(defun enqueue-binary-message (endpoint)
  (let* ((data     (coerce (loop for i from 10 to 60 collect i) '(vector (unsigned-byte 7))))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+binary-frame+
                                  :data data
                                  :finp t
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))


(defun enqueue-first-text-fragment (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "Hello "))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+text-frame+
                                  :data data
                                  :finp nil
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))

(defun enqueue-middle-text-fragment (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "World"))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+continuation-frame+
                                  :data data
                                  :finp nil
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))

(defun enqueue-last-text-fragment (endpoint)
  (let* ((data     (trivial-utf-8:string-to-utf-8-bytes "! "))
         (frame    (make-instance 'cl-naive-websockets::frame
                                  :opcode cl-naive-websockets::+continuation-frame+
                                  :data data
                                  :finp t
                                  :payload-length (length data)
                                  :masking-key nil)))
    (cl-naive-websockets::enqueue-frame endpoint frame)))


(testsuite transmission-endpoint/enqueue-frame

  (testcase enqueue-non-fragmented-pong-message/no-fragment
            :expected '(0)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-pong-message endpoint)
                      (receive-queue-lengths endpoint)))

  (testcase enqueue-non-fragmented-text-message/no-fragment
            :expected '(0)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-text-message endpoint)
                      (receive-queue-lengths endpoint)))

  (testcase enqueue-first-text-fragment/no-fragment
            :expected '(1)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-first-text-fragment endpoint)
                      (receive-queue-lengths endpoint)))

  (testcase enqueue-first-and-mid-text-fragment/no-fragment
            :expected '(2)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-first-text-fragment endpoint)
                      (enqueue-middle-text-fragment endpoint)
                      (receive-queue-lengths endpoint)))

  (testcase enqueue-first-to-last-text-fragment/no-fragment
            :expected '(0)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-first-text-fragment endpoint)
                      (enqueue-middle-text-fragment endpoint)
                      (enqueue-last-text-fragment endpoint)
                      (receive-queue-lengths endpoint)))

  (testcase enqueue-non-fragmented-pong-message/with-fragments
            :expected '(1)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-first-text-fragment endpoint)
                      (enqueue-pong-message endpoint)
                      (receive-queue-lengths endpoint)))

  (testcase enqueue-non-fragmented-text-message/with-fragments
            :expected '(1)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-first-text-fragment endpoint)
                      (handler-case
                          (enqueue-text-message endpoint)
                        (:no-error nil)
                        (websockets-error ()
                          (receive-queue-lengths endpoint)))))

  (testcase enqueue-first-text-fragment/with-fragments
            :expected '(1)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (enqueue-first-text-fragment endpoint)
                      (handler-case
                          (enqueue-first-text-fragment endpoint)
                        (:no-error nil)
                        (websockets-error ()
                          (receive-queue-lengths endpoint)))))

  (testcase enqueue-middle-text-fragment/no-fragment
            :expected '(0)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (handler-case
                          (enqueue-middle-text-fragment endpoint)
                        (:no-error nil)
                        (websockets-error ()
                          (receive-queue-lengths endpoint)))))

  (testcase enqueue-last-text-fragment/no-fragment
            :expected '(0)
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (handler-case
                          (enqueue-last-text-fragment endpoint)
                        (:no-error nil)
                        (websockets-error ()
                          (receive-queue-lengths endpoint)))))

  (testcase enqueue-close-message
            :expected t
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (let ((message (enqueue-close-message endpoint)))
                        (and (typep message 'websockets-close-message)
                             t))))

  (testcase enqueue-ping-message
            :expected t
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (let ((message (enqueue-ping-message endpoint)))
                        (and (typep message 'websockets-ping-message)
                             t))))

  (testcase enqueue-pong-message
            :expected t
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (let ((message (enqueue-pong-message endpoint)))
                        (and (typep message 'websockets-pong-message)
                             t))))

  (testcase enqueue-text-message
            :expected t
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (let ((message (enqueue-text-message endpoint)))
                        (and (typep message 'websockets-text-message)
                             t))))

  (testcase enqueue-binary-message
            :expected t
            :actual (let ((endpoint (make-instance 'websockets-endpoint)))
                      (let ((message (enqueue-binary-message endpoint)))
                        (and (typep message 'websockets-binary-message)
                             t)))))
