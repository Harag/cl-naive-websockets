(defpackage :cl-naive-websockets.wrapper.tests
  (:use
   :common-lisp
   :cl-naive-tests
   :cl-naive-websockets.wrapper))

(in-package :cl-naive-websockets.wrapper.tests)

(testsuite wrapper

  (testcase wrap-parameters-text-payload/both
            :expected "(:PAYLOAD \"This is a text payload.\" :STRING \"string\" :LIST \"(1 2 3)\" :INTEGER \"42\" :KEYWORD :FOO)"
            :actual (wrap-parameters-text-payload '(:string "string"
                                                    :list "(1 2 3)"
                                                    :integer "42"
                                                    :keyword :foo)
                                                  "This is a text payload."
                                                  :payload)
            :info "wrap-parameters-text-payload/both")

  (testcase wrap-parameters-text-payload/no-parameters
            :expected "(:PAYLOAD \"This is a text payload.\")"
            :actual (wrap-parameters-text-payload '()
                                                  "This is a text payload."
                                                  :payload)
            :info "wrap-parameters-text-payload/no-parameters")

  (testcase wrap-parameters-text-payload/no-payload
            :expected "(:PAYLOAD \"\" :STRING \"string\" :LIST \"(1 2 3)\" :INTEGER \"42\" :KEYWORD :FOO)"
            :actual (wrap-parameters-text-payload '(:string "string"
                                                    :list "(1 2 3)"
                                                    :integer "42"
                                                    :keyword :foo)
                                                  ""
                                                  :payload)
            :info "wrap-parameters-text-payload/both")
  
  
  (testcase unwrap-parameters-text-payload/both
            :equal 'equalp
            :expected '((:string "string" :list "(1 2 3)" :integer "42" :keyword :foo)
                        "This is a text payload.")
            :actual (multiple-value-list
                     (unwrap-parameters-text-payload "(:PAYLOAD \"This is a text payload.\" :STRING \"string\" :LIST \"(1 2 3)\" :INTEGER \"42\" :KEYWORD :FOO)"
                                                     :payload))
            :info "unwrap-parameters-text-payload/both")

  (testcase unwrap-parameters-text-payload/no-parameters
            :equal 'equalp
            :expected '(()
                        "This is a text payload.")
            :actual (multiple-value-list
                     (unwrap-parameters-text-payload "(:PAYLOAD \"This is a text payload.\")"
                                                     :payload))
            :info "unwrap-parameters-text-payload/no-parameters")

  (testcase wrap-parameters-binary-payload
            :expected "(:BINARY 4 :STRING \"string\" :LIST \"(1 2 3)\" :INTEGER \"42\" :KEYWORD :FOO)"
            :actual (wrap-parameters-binary-payload '(:string "string"
                                                      :list "(1 2 3)"
                                                      :integer "42"
                                                      :keyword :foo)
                                                    #(1 2 3 4)
                                                    :binary))
  
  (testcase unwrap-parameters-binary-payload/binary
            :equal 'equalp
            :expected '((:binary 4 :string "string" :list "(1 2 3)" :integer "42" :keyword :foo) #(1 2 3 4))
            :actual (multiple-value-list
                     (unwrap-parameters-binary-payload
                      (concatenate '(vector cl-naive-websockets:octet)
                        (babel:string-to-octets "(:BINARY 4 :STRING \"string\" :LIST \"(1 2 3)\" :INTEGER \"42\" :KEYWORD :FOO)"
                                                :encoding :utf-8)
                        #(1 2 3 4))
                      :binary :payload))
            :info "unwrap-parameters-binary-payload/binary")

  (testcase unwrap-parameters-binary-payload/text
            :equal 'equalp
            :expected '((:string "string" :list "(1 2 3)" :integer "42" :keyword :foo) #(1 2 3 4))
            :actual (multiple-value-list
                     (unwrap-parameters-binary-payload
                      (babel:string-to-octets "(:PAYLOAD #(1 2 3 4) :STRING \"string\" :LIST \"(1 2 3)\" :INTEGER \"42\" :KEYWORD :FOO)"
                                              :encoding :utf-8)
                      :binary :payload))
            :info "unwrap-parameters-binary-payload/text"))

